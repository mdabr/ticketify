import App from 'App';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';

jest.mock('components', () => ({
    Infobox: function Infobox() {
        return <div className="infobox"></div>;
    },
    Loader: function Loader() {
        return <div className="loader"></div>;
    }
}));

jest.mock('views/ViewHolder/ViewHolder', () => function ViewHolder() {
    return <div className="view-holder" />;
});

describe('<App />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        unmountComponentAtNode(container as HTMLDivElement);
        (container as HTMLDivElement).remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders without errors', () => {
        const store = createStore(combineReducers({
            user: jest.fn().mockReturnValue({})
        }));

        act(() => {
            render(<Provider store={store}><App /></Provider>, container);
        });

        const app = document.querySelector('.app-route');

        expect(app?.children[0].classList.contains('view-holder')).toEqual(true);
        expect(app?.children[1].classList.contains('infobox')).toEqual(true);
        expect(app?.children[2].classList.contains('loader')).toEqual(true);
    });
});
