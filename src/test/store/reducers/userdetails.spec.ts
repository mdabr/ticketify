import userdetailsReducer from 'store/reducers/userdetails';
import { HideUserDetailsAction, ShowUserDetailsAction } from 'types/store/actions';
import { UserDetailsActions, UserDetailsState } from 'types/store/userdetails';

describe('userdetails.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined as unknown as UserDetailsState;
        const userdetailsData: HideUserDetailsAction = {
            type: 'HIDE_USER_DETAILS'
        };

        const state = userdetailsReducer(initialState, userdetailsData);

        expect(state).toEqual({
            userDetailsVisible: false
        });
    });

    describe('SHOW_USER_DETAILS', () => {
        it('changes userDetailsVisible to true', () => {
            const initialState = {
                userDetailsVisible: false
            };
            const userdetailsData: ShowUserDetailsAction = {
                type: 'SHOW_USER_DETAILS'
            };

            const state = userdetailsReducer(initialState, userdetailsData);

            expect(state).toEqual({
                userDetailsVisible: true
            });
        });
    });

    describe('HIDE_USER_DETAILS', () => {
        it('changes userDetailsVisible to false', () => {
            const initialState = {
                userDetailsVisible: true
            };
            const userdetailsData: HideUserDetailsAction = {
                type: 'HIDE_USER_DETAILS'
            };

            const state = userdetailsReducer(initialState, userdetailsData);

            expect(state).toEqual({
                userDetailsVisible: false
            });
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState = {
                userDetailsVisible: false
            };
            const userdetailsData = {
                type: 'UNHANDLED'
            };

            const state = userdetailsReducer(initialState, userdetailsData as UserDetailsActions);

            expect(state).toEqual({
                userDetailsVisible: false
            });
        });
    });
});
