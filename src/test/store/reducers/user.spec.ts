import { UserActions, UserState } from 'types/store/user';
import userReducer from 'store/reducers/user';
import { USER_TYPES } from 'utils/constants';
import { ChangeUserAction, ResetUserAction } from 'types/store/actions';

describe('user.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined as unknown as UserState;
        const userData: ResetUserAction = {
            type: 'RESET_USER'
        };

        const state = userReducer(initialState, userData);

        expect(state).toEqual({
            id: null,
            firstName: null,
            lastName: null,
            roles: [{ role: USER_TYPES.NONE }],
            companyInfo: null,
            email: null,
            enabled: false
        });
    });

    describe('CHANGE_USER', () => {
        it('changes user', () => {
            const initialState = {
                id: null,
                firstName: null,
                lastName: null,
                roles: [{ role: USER_TYPES.NONE }],
                companyInfo: null,
                email: null,
                enabled: false
            };
            const userData: ChangeUserAction = {
                type: 'CHANGE_USER',
                payload: {
                    id: '1',
                    firstName: 'M',
                    lastName: 'D',
                    roles: [{ role: USER_TYPES.NONE }, { role: USER_TYPES.ADMINISTRATOR }],
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: 'ric@corp.com',
                    enabled: false
                }
            };

            const state = userReducer(initialState, userData);

            expect(state).toEqual(userData.payload);
        });
    });

    describe('RESET_USER', () => {
        it('resets user to none', () => {
            const initialState = undefined as unknown as UserState;
            const userData: ResetUserAction = {
                type: 'RESET_USER'
            };

            const state = userReducer(initialState, userData);

            expect(state).toEqual({
                id: null,
                firstName: null,
                lastName: null,
                roles: [{ role: USER_TYPES.NONE }],
                companyInfo: null,
                email: null,
                enabled: false
            });
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState = {
                id: null,
                firstName: null,
                lastName: null,
                roles: [{ role: USER_TYPES.NONE }],
                companyInfo: null,
                email: null,
                enabled: false
            };
            const userData = {
                type: 'UNHANDLED'
            };

            const state = userReducer(initialState, userData as UserActions);

            expect(state).toEqual(initialState);
        });
    });
});
