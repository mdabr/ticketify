import tokenReducer from 'store/reducers/token';
import { UpdateTokenAction } from 'types/store/actions';
import { TokenActions, TokenState } from 'types/store/token';

describe('token.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined as unknown as TokenState;
        const tokenData: UpdateTokenAction = {
            type: 'UPDATE_TOKEN',
            payload: {
                expires: new Date(),
                token: '12345'
            }
        };

        const state = tokenReducer(initialState, tokenData);

        expect(state).toEqual({
            expires: tokenData.payload.expires,
            token: tokenData.payload.token
        });
    });

    describe('UPDATE_TOKEN', () => {
        it('updates token', () => {
            const initialState = {
                expires: new Date(),
                token: ''
            };
            const tokenData: UpdateTokenAction = {
                type: 'UPDATE_TOKEN',
                payload: {
                    expires: new Date(),
                    token: '12345'
                }
            };

            const state = tokenReducer(initialState, tokenData);

            expect(state).toEqual({
                expires: tokenData.payload.expires,
                token: tokenData.payload.token
            });
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState = {
                expires: new Date(),
                token: ''
            };
            const tokenData = {
                type: 'UNHANDLED'
            };

            const state = tokenReducer(initialState, tokenData as TokenActions);

            expect(state).toEqual({
                ...initialState
            });
        });
    });
});
