import newticketReducer from 'store/reducers/newticket';
import { HideNewTicketAction, SetEditedTicketAction, ShowNewTicketAction } from 'types/store/actions';
import { NewTicketActions, NewTicketState } from 'types/store/newticket';

describe('newticket.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined;
        const newticketData: HideNewTicketAction = {
            type: 'HIDE_NEW_TICKET'
        };

        const state = newticketReducer(initialState as unknown as NewTicketState, newticketData);

        expect(state).toEqual({
            newTicketVisible: false,
            editedTicket: null
        });
    });

    describe('SHOW_NEW_TICKET', () => {
        it('changes newTicketVisible to true', () => {
            const initialState = {
                newTicketVisible: false,
                editedTicket: null
            };
            const newticketData: ShowNewTicketAction = {
                type: 'SHOW_NEW_TICKET'
            };

            const state = newticketReducer(initialState, newticketData);

            expect(state).toEqual({
                ...initialState,
                newTicketVisible: true
            });
        });
    });

    describe('HIDE_NEW_TICKET', () => {
        it('changes newTicketVisible to true', () => {
            const initialState = {
                newTicketVisible: true,
                editedTicket: null
            };
            const newticketData: HideNewTicketAction = {
                type: 'HIDE_NEW_TICKET'
            };

            const state = newticketReducer(initialState, newticketData);

            expect(state).toEqual({
                ...initialState,
                newTicketVisible: false
            });
        });
    });

    describe('SET_EDITED_TICKET', () => {
        it('sets editedTicket', () => {
            const initialState = {
                newTicketVisible: false,
                editedTicket: null
            };
            const newticketData: SetEditedTicketAction = {
                type: 'SET_EDITED_TICKET',
                payload: {
                    authorInfo: {
                        companyInfo: {
                            city: '',
                            country: '',
                            enabled: true,
                            id: 1,
                            localIdCode: '',
                            name: '',
                            postalCode: '',
                            premisesAddress: ''
                        },
                        email: '',
                        enabled: true,
                        firstName: '',
                        id: '',
                        lastName: '',
                        roles: []
                    },
                    categoryInfo: {
                        enabled: true,
                        id: 1,
                        name: '',
                        subcategories: [{
                            enabled: true,
                            id: 1,
                            name: ''
                        }]
                    },
                    comments: [{
                        authorInfo: {
                            companyInfo: {
                                city: '',
                                country: '',
                                enabled: true,
                                id: 1,
                                localIdCode: '',
                                name: '',
                                postalCode: '',
                                premisesAddress: ''
                            },
                            email: '',
                            enabled: true,
                            firstName: '',
                            id: '',
                            lastName: '',
                            roles: []
                        },
                        comment: ''
                    }],
                    companyInfo: {
                        id: 1
                    },
                    createdAt: new Date(2021, 4, 29, 10, 30, 0),
                    description: '',
                    id: 1,
                    lastModifiedDate: new Date(2021, 4, 29, 10, 30, 0),
                    priority: '',
                    status: '',
                    subcategoryInfo: {
                        enabled: true,
                        id: 1,
                        name: ''
                    },
                    technicianInfo: {
                        companyInfo: {
                            city: '',
                            country: '',
                            enabled: true,
                            id: 1,
                            localIdCode: '',
                            name: '',
                            postalCode: '',
                            premisesAddress: ''
                        },
                        email: '',
                        enabled: true,
                        firstName: '',
                        id: '',
                        lastName: '',
                        roles: []
                    },
                    title: ''
                }
            };

            const state = newticketReducer(initialState, newticketData);

            expect(state).toEqual({
                ...initialState,
                newTicketVisible: true,
                editedTicket: newticketData.payload
            });
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState = {
                newTicketVisible: false,
                editedTicket: null
            };
            const newticketData = {
                type: 'UNHANDLED'
            };

            const state = newticketReducer(initialState, newticketData as NewTicketActions);

            expect(state).toEqual({
                ...initialState,
                newTicketVisible: false
            });
        });
    });
});
