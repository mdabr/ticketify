import ticketdetailsReducer from 'store/reducers/ticketdetails';
import { TicketDetailsAction } from 'types/components/TicketDetails';
import { HideTicketDetailsAction, SetTicketDetailsAction } from 'types/store/actions';
import { Ticket } from 'types/store/data';
import { TicketDetailsState } from 'types/store/ticketdetails';
import { User } from 'types/store/user';

describe('ticketdetails.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined as unknown as TicketDetailsState;
        const ticketdetailsData: HideTicketDetailsAction = {
            type: 'HIDE_TICKET_DETAILS'
        };

        const state = ticketdetailsReducer(initialState, ticketdetailsData);

        expect(state).toEqual({
            ticketDetailsVisible: false,
            ticket: null,
            assignees: []
        });
    });

    describe('HIDE_TICKET_DETAILS', () => {
        it('changes ticketDetailsVisible to false', () => {
            const initialState = {
                ticketDetailsVisible: true,
                ticket: {} as Ticket,
                assignees: [] as User[]
            };
            const ticketdetailsData: HideTicketDetailsAction = {
                type: 'HIDE_TICKET_DETAILS'
            };

            const state = ticketdetailsReducer(initialState, ticketdetailsData);

            expect(state).toEqual({
                ticketDetailsVisible: false,
                ticket: null,
                assignees: []
            });
        });
    });

    describe('SET_TICKET_DETAILS', () => {
        it('changes ticketDetailsVisible to true and sets ticket', () => {
            const initialState = {
                ticketDetailsVisible: false,
                ticket: null,
                assignees: []
            };
            const ticketDetailsAction: SetTicketDetailsAction = {
                type: 'SET_TICKET_DETAILS',
                payload: {
                    ticket: {

                        authorInfo: {
                            companyInfo: {
                                city: '',
                                country: '',
                                enabled: true,
                                id: 1,
                                localIdCode: '',
                                name: '',
                                postalCode: '',
                                premisesAddress: ''
                            },
                            email: '',
                            enabled: true,
                            firstName: '',
                            id: '',
                            lastName: '',
                            roles: []
                        },
                        categoryInfo: {
                            enabled: true,
                            id: 1,
                            name: '',
                            subcategories: [{
                                enabled: true,
                                id: 1,
                                name: ''
                            }]
                        },
                        comments: [{
                            authorInfo: {
                                companyInfo: {
                                    city: '',
                                    country: '',
                                    enabled: true,
                                    id: 1,
                                    localIdCode: '',
                                    name: '',
                                    postalCode: '',
                                    premisesAddress: ''
                                },
                                email: '',
                                enabled: true,
                                firstName: '',
                                id: '',
                                lastName: '',
                                roles: []
                            },
                            comment: ''
                        }],
                        companyInfo: {
                            id: 1
                        },
                        createdAt: new Date(2021, 4, 29, 10, 30, 0),
                        description: '',
                        id: 1,
                        lastModifiedDate: new Date(2021, 4, 29, 10, 30, 0),
                        priority: '',
                        status: '',
                        subcategoryInfo: {
                            enabled: true,
                            id: 1,
                            name: ''
                        },
                        technicianInfo: {
                            companyInfo: {
                                city: '',
                                country: '',
                                enabled: true,
                                id: 1,
                                localIdCode: '',
                                name: '',
                                postalCode: '',
                                premisesAddress: ''
                            },
                            email: '',
                            enabled: true,
                            firstName: '',
                            id: '',
                            lastName: '',
                            roles: []
                        },
                        title: ''
                    },
                    assignees: []
                }
            };

            const state = ticketdetailsReducer(initialState, ticketDetailsAction);

            expect(state).toEqual({
                ticketDetailsVisible: true,
                ticket: ticketDetailsAction.payload.ticket,
                assignees: ticketDetailsAction.payload.assignees
            });
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState = {
                ticketDetailsVisible: false,
                ticket: null,
                assignees: []
            };
            const ticketdetailsData = {
                type: 'UNHANDLED'
            };

            const state = ticketdetailsReducer(initialState, ticketdetailsData as TicketDetailsAction);

            expect(state).toEqual({
                ...initialState
            });
        });
    });
});
