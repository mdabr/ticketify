import toastsReducer from 'store/reducers/toasts';
import { AddToastAction, RemoveToastAction } from 'types/store/actions';
import { Toast, ToastsActions, ToastsState } from 'types/store/toasts';

describe('toasts.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined as unknown as ToastsState;
        const toastsData: RemoveToastAction = {
            type: 'REMOVE_TOAST',
            payload: {
                id: 'abcd'
            }
        };

        const state = toastsReducer(initialState, toastsData);

        expect(state).toEqual([]);
    });

    describe('ADD_TOAST', () => {
        it('adds toast to state', () => {
            const initialState: Toast[] = [];
            const toastsData: AddToastAction = {
                type: 'ADD_TOAST',
                payload: {
                    header: 'Error!',
                    message: 'Something went wrong',
                    level: 'DANGER',
                    time: 420
                }
            };

            const state = toastsReducer(initialState, toastsData);

            expect(state).toEqual([{
                id: expect.any(String),
                header: toastsData.payload.header,
                message: toastsData.payload.message,
                level: toastsData.payload.level,
                time: toastsData.payload.time
            }]);
        });

        it('adds toast with default time if none is provided', () => {
            const initialState: Toast[] = [];
            const toastsData: AddToastAction = {
                type: 'ADD_TOAST',
                payload: {
                    header: 'Error!',
                    message: 'Something went wrong',
                    level: 'DANGER'
                }
            };

            const state = toastsReducer(initialState, toastsData);

            expect(state).toEqual([{
                id: expect.any(String),
                header: toastsData.payload.header,
                message: toastsData.payload.message,
                level: toastsData.payload.level,
                time: 5000
            }]);
        });

        it('removes duplicate toast when adding duplicate', () => {
            const initialState: Toast[] = [{
                id: '1',
                header: 'Error!',
                message: 'Something went wrong',
                level: 'DANGER',
                time: 5000
            }];
            const toastsData: AddToastAction = {
                type: 'ADD_TOAST',
                payload: {
                    header: initialState[0].header,
                    message: initialState[0].message,
                    level: initialState[0].level
                }
            };

            const state = toastsReducer(initialState, toastsData);

            expect(state).toEqual([{
                id: expect.any(String),
                header: toastsData.payload.header,
                message: toastsData.payload.message,
                level: toastsData.payload.level,
                time: 5000
            }]);
        });
    });

    describe('REMOVE_TOAST', () => {
        it('removes toast from state', () => {
            const initialState: Toast[] = [{
                id: '1337',
                header: 'Error!',
                message: 'Something went wrong',
                level: 'DANGER',
                time: 420
            }];
            const toastsData: RemoveToastAction = {
                type: 'REMOVE_TOAST',
                payload: {
                    id: '1337'
                }
            };

            const state = toastsReducer(initialState, toastsData);

            expect(state).toEqual([]);
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState: Toast[] = [];
            const toastsData = {
                type: 'UNHANDLED'
            };

            const state = toastsReducer(initialState, toastsData as ToastsActions);

            expect(state).toEqual([]);
        });
    });
});
