import loaderReducer from 'store/reducers/loader';
import { HideLoaderAction, ShowLoaderAction } from 'types/store/actions';
import { LoaderActions, LoaderState } from 'types/store/loader';

describe('loader.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined;
        const loaderData: HideLoaderAction = {
            type: 'HIDE_LOADER'
        };

        const state = loaderReducer(initialState as unknown as LoaderState, loaderData);

        expect(state).toEqual({
            loaderVisible: false
        });
    });

    describe('SHOW_LOADER', () => {
        it('sets loaderVisible to true', () => {
            const initialState = {
                loaderVisible: false
            };
            const loaderData: ShowLoaderAction = {
                type: 'SHOW_LOADER'
            };

            const state = loaderReducer(initialState, loaderData);

            expect(state).toEqual({
                loaderVisible: true
            });
        });
    });

    describe('HIDE_LOADER', () => {
        it('sets loaderVisible to false', () => {
            const initialState = {
                loaderVisible: true
            };
            const loaderData: HideLoaderAction = {
                type: 'HIDE_LOADER'
            };

            const state = loaderReducer(initialState, loaderData);

            expect(state).toEqual({
                loaderVisible: false
            });
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState = {
                loaderVisible: true
            };
            const loaderData = {
                type: 'UNHANDLED'
            };

            const state = loaderReducer(initialState, loaderData as LoaderActions);

            expect(state).toEqual({
                ...initialState
            });
        });
    });
});
