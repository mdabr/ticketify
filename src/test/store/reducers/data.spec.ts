import dataReducer from 'store/reducers/data';
import { FilterTicketsAction, SetCategoriesAction, SetTicketsAction } from 'types/store/actions';
import { Ticket, DataState, Category, Specifiers, Subcategory, DataActions } from 'types/store/data';
import { Company, User } from 'types/store/user';

describe('data.ts', () => {
    it('gets default state passed when none is provided', () => {
        const initialState = undefined as unknown as DataState;
        const data: SetTicketsAction = {
            type: 'SET_TICKETS',
            payload: []
        };
        const state = dataReducer(initialState, data);

        expect(state).toEqual({
            categories: [],
            tickets: [],
            filteredTickets: []
        });
    });

    describe('SET_TICKETS', () => {
        it('sets tickets', () => {
            const tickets: Ticket[] = [{
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            }];

            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: []
            };
            const data: SetTicketsAction = {
                type: 'SET_TICKETS',
                payload: tickets
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                tickets,
                filteredTickets: tickets
            });
        });
    });

    describe('SET_CATEGORIES', () => {
        it('sets categories', () => {
            const categories: Category[] = [{
                enabled: true,
                id: 1,
                name: '',
                subcategories: []
            }];
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: []
            };
            const data: SetCategoriesAction = {
                type: 'SET_CATEGORIES',
                payload: categories
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                categories
            });
        });
    });

    describe('FILTER_TICKETS', () => {
        it('leaves filtered tickets equal to tickets with specifiers unfilled', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: new Date(2021, 4, 29, 10, 0, 0),
                startedTo: new Date(2021, 4, 29, 11, 0, 0),
                modifiedFrom: new Date(2021, 4, 29, 10, 0, 0),
                modifiedTo: new Date(2021, 4, 29, 11, 0, 0)
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    authorInfo: {
                        companyInfo: {
                            city: '',
                            country: '',
                            enabled: true,
                            id: 1,
                            localIdCode: '',
                            name: '',
                            postalCode: '',
                            premisesAddress: ''
                        },
                        email: '',
                        enabled: true,
                        firstName: '',
                        id: '',
                        lastName: '',
                        roles: []
                    },
                    categoryInfo: {
                        enabled: true,
                        id: 1,
                        name: '',
                        subcategories: [{
                            enabled: true,
                            id: 1,
                            name: ''
                        }]
                    },
                    comments: [{
                        authorInfo: {
                            companyInfo: {
                                city: '',
                                country: '',
                                enabled: true,
                                id: 1,
                                localIdCode: '',
                                name: '',
                                postalCode: '',
                                premisesAddress: ''
                            },
                            email: '',
                            enabled: true,
                            firstName: '',
                            id: '',
                            lastName: '',
                            roles: []
                        },
                        comment: ''
                    }],
                    companyInfo: {
                        id: 1
                    },
                    createdAt: new Date(2021, 4, 29, 10, 30, 0),
                    description: '',
                    id: 1,
                    lastModifiedDate: new Date(2021, 4, 29, 10, 30, 0),
                    priority: '',
                    status: '',
                    subcategoryInfo: {
                        enabled: true,
                        id: 1,
                        name: ''
                    },
                    technicianInfo: {
                        companyInfo: {
                            city: '',
                            country: '',
                            enabled: true,
                            id: 1,
                            localIdCode: '',
                            name: '',
                            postalCode: '',
                            premisesAddress: ''
                        },
                        email: '',
                        enabled: true,
                        firstName: '',
                        id: '',
                        lastName: '',
                        roles: []
                    },
                    title: ''
                }]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: initialState.tickets
            });
        });

        it('filters tickets by authors', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [{ id: '1' } as User],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    authorInfo: {
                        id: '1'
                    }
                } as Ticket, {
                    authorInfo: {
                        id: '2'
                    }
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by categories', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [{
                    id: 1
                } as Category],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    categoryInfo: {
                        id: 1
                    }
                } as Ticket, {
                    categoryInfo: {
                        id: 2
                    }
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by created date', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: new Date(2021, 4, 29, 22, 59, 0),
                startedTo: new Date(2021, 4, 29, 23, 59, 59),
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    createdAt: new Date(2021, 4, 29, 23, 23, 23)
                } as Ticket, {
                    createdAt: new Date(1994, 19, 9, 4, 20, 0)
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by ID', () => {
            const specifiers: Specifiers = {
                id: '1',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    id: 1
                } as Ticket, {
                    id: 11
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by last modified data', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: new Date(2021, 4, 29, 22, 59, 0),
                modifiedTo: new Date(2021, 4, 29, 23, 59, 59)
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    lastModifiedDate: new Date(2021, 4, 29, 23, 23, 23)
                } as Ticket, {
                    lastModifiedDate: new Date(1994, 19, 9, 4, 20, 0)
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by priorities', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: ['LOW'],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    priority: 'LOW'
                } as Ticket, {
                    priority: 'HIGH'
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by status', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: ['UNASSIGNED'],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    status: 'UNASSIGNED'
                } as Ticket, {
                    status: 'ASSIGNED'
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by subcategories', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [{
                    id: 1
                } as Subcategory],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    subcategoryInfo: {
                        id: 1
                    }
                } as Ticket, {
                    subcategoryInfo: {
                        id: 2
                    }
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by technicians', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [{
                    id: '1'
                } as User],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    technicianInfo: {
                        id: '1'
                    }
                } as Ticket, {
                    technicianInfo: {
                        id: '2'
                    }
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by titles', () => {
            const specifiers: Specifiers = {
                id: '',
                title: 'HELLO',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    title: 'HELLO, IS IT ME YOU\'RE LOOKING FOR?'
                } as Ticket, {
                    title: 'LIONEL RICHIE?!'
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });

        it('filters tickets by companies', () => {
            const specifiers: Specifiers = {
                id: '',
                title: '',
                assignees: [],
                categories: [],
                subcategories: [],
                companies: [{
                    id: 1
                } as Company],
                creators: [],
                priorities: [],
                status: [],
                startedFrom: '',
                startedTo: '',
                modifiedFrom: '',
                modifiedTo: ''
            };
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: [{
                    companyInfo: {
                        id: 1
                    }
                } as Ticket, {
                    companyInfo: {
                        id: 2
                    }
                } as Ticket]
            };
            const data: FilterTicketsAction = {
                type: 'FILTER_TICKETS',
                payload: specifiers
            };

            const state = dataReducer(initialState, data);

            expect(state).toEqual({
                ...initialState,
                filteredTickets: [initialState.tickets[0]]
            });
        });
    });

    describe('default', () => {
        it('returns unchanged state', () => {
            const initialState: DataState = {
                categories: [],
                filteredTickets: [],
                tickets: []
            };
            const data = {
                type: 'UNHANDLED'
            };

            const state = dataReducer(initialState, data as DataActions);

            expect(state).toEqual({
                ...initialState
            });
        });
    });
});
