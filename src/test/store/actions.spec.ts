import { User } from 'types/store/user';
import * as actions from 'store/actions';
import { Category, Specifiers, Ticket, TicketInfo } from 'types/store/data';
import { Token } from 'types/store/token';

describe('actions.ts', () => {
    describe('changeUser', () => {
        it('returns changeUser action', () => {
            const user: User = {
                id: '1',
                companyInfo: {
                    id: 1,
                    city: '',
                    country: '',
                    enabled: true,
                    localIdCode: '',
                    name: '',
                    postalCode: '',
                    premisesAddress: ''
                },
                email: '',
                enabled: true,
                firstName: '',
                lastName: '',
                roles: []
            };

            const action = actions.changeUser(user);

            expect(action).toEqual({
                type: 'CHANGE_USER',
                payload: user
            });
        });
    });

    describe('resetUser', () => {
        it('returns resetUser action', () => {
            const action = actions.resetUser();

            expect(action).toEqual({
                type: 'RESET_USER'
            });
        });
    });

    describe('showSuccess', () => {
        it('returns action to show success toast', () => {
            const header = 'Success!';
            const message = 'Something succeeded';

            const action = actions.showSuccess({
                header,
                message
            });

            expect(action).toEqual({
                type: 'ADD_TOAST',
                payload: {
                    header,
                    message,
                    level: 'SUCCESS'
                }
            });
        });
    });

    describe('showDanger', () => {
        it('returns action to show danger toast', () => {
            const header = 'Danger!';
            const message = 'Something is wrong';

            const action = actions.showDanger({
                header,
                message
            });

            expect(action).toEqual({
                type: 'ADD_TOAST',
                payload: {
                    header,
                    message,
                    level: 'DANGER'
                }
            });
        });
    });

    describe('showWarning', () => {
        it('returns action to show warning toast', () => {
            const header = 'Warning!';
            const message = 'Something requires attention';

            const action = actions.showWarning({
                header,
                message
            });

            expect(action).toEqual({
                type: 'ADD_TOAST',
                payload: {
                    header,
                    message,
                    level: 'WARNING'
                }
            });
        });
    });

    describe('showInfo', () => {
        it('returns action to show info toast', () => {
            const header = 'Info!';
            const message = 'Something to note';

            const action = actions.showInfo({
                header,
                message
            });

            expect(action).toEqual({
                type: 'ADD_TOAST',
                payload: {
                    header,
                    message,
                    level: 'INFO'
                }
            });
        });
    });

    describe('removeToast', () => {
        it('returns action for removing toast', () => {
            const id = '1234';

            const action = actions.removeToast(id);

            expect(action).toEqual({
                type: 'REMOVE_TOAST',
                payload: {
                    id
                }
            });
        });
    });

    describe('showLoader', () => {
        it('returns action to show loader', () => {
            const action = actions.showLoader();

            expect(action).toEqual({
                type: 'SHOW_LOADER'
            });
        });
    });

    describe('hideLoader', () => {
        it('returns action to hide loader', () => {
            const action = actions.hideLoader();

            expect(action).toEqual({
                type: 'HIDE_LOADER'
            });
        });
    });

    describe('showUserDetails', () => {
        it('returns action to show user details', () => {
            const action = actions.showUserDetails();

            expect(action).toEqual({
                type: 'SHOW_USER_DETAILS'
            });
        });
    });

    describe('hideUserDetails', () => {
        it('returns action to hide user details', () => {
            const action = actions.hideUserDetails();

            expect(action).toEqual({
                type: 'HIDE_USER_DETAILS'
            });
        });
    });

    describe('hideTicketDetails', () => {
        it('returns action to hide ticket details', () => {
            const action = actions.hideTicketDetails();

            expect(action).toEqual({
                type: 'HIDE_TICKET_DETAILS'
            });
        });
    });

    describe('showNewTicket', () => {
        it('returns action to show new ticket', () => {
            const action = actions.showNewTicket();

            expect(action).toEqual({
                type: 'SHOW_NEW_TICKET'
            });
        });
    });

    describe('hideNewTicket', () => {
        it('returns action to hide new ticket', () => {
            const action = actions.hideNewTicket();

            expect(action).toEqual({
                type: 'HIDE_NEW_TICKET'
            });
        });
    });

    describe('setEditedTicket', () => {
        it('returns action to set edited ticket', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };

            const action = actions.setEditedTicket(ticket);

            expect(action).toEqual({
                type: 'SET_EDITED_TICKET',
                payload: ticket
            });
        });

        it('returns action to reset edited ticket', () => {
            const ticket = null;

            const action = actions.setEditedTicket(ticket);

            expect(action).toEqual({
                type: 'SET_EDITED_TICKET',
                payload: ticket
            });
        });
    });

    describe('setTickets', () => {
        it('returns action for setting tickets', () => {
            const tickets: Ticket[] = [{
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            }];

            const action = actions.setTickets(tickets);

            expect(action).toEqual({
                type: 'SET_TICKETS',
                payload: tickets
            });
        });
    });

    describe('setCategories', () => {
        it('returns action for setting categories', () => {
            const categories: Category[] = [{
                enabled: true,
                id: 1,
                name: '',
                subcategories: []
            }];

            const action = actions.setCategories(categories);

            expect(action).toEqual({
                type: 'SET_CATEGORIES',
                payload: categories
            });
        });
    });

    describe('filterTickets', () => {
        it('returns action for filtering tickets', () => {
            const specifiers: Specifiers = {
                assignees: [],
                categories: [],
                companies: [],
                creators: [],
                id: '',
                modifiedFrom: '',
                modifiedTo: '',
                priorities: [],
                startedFrom: '',
                startedTo: '',
                status: [],
                subcategories: [],
                title: ''
            };

            const action = actions.filterTickets(specifiers);

            expect(action).toEqual({
                type: 'FILTER_TICKETS',
                payload: specifiers
            });
        });
    });

    describe('updateToken', () => {
        it('returns action to update token', () => {
            const token: Token = {
                expires: new Date(),
                token: ''
            };

            const action = actions.updateToken(token);

            expect(action).toEqual({
                type: 'UPDATE_TOKEN',
                payload: {
                    expires: token.expires,
                    token: token.token
                }
            });
        });
    });

    describe('logIn', () => {
        it('returns action to log in', () => {
            const email = '';
            const password = '';

            const action = actions.logIn(email, password);

            expect(action).toEqual({
                type: 'LOG_IN',
                payload: {
                    email,
                    password
                }
            });
        });
    });

    describe('editTicket', () => {
        it('returns action to edit ticket', () => {
            const ticketInfo: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: new Date(),
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };

            const action = actions.editTicket(ticketInfo);

            expect(action).toEqual({
                type: 'EDIT_TICKET',
                payload: ticketInfo
            });
        });
    });

    describe('addNewTicket', () => {
        it('returns action to add new ticket', () => {
            const ticketInfo: TicketInfo = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                companyInfo: {
                    id: 1
                },
                createdAt: new Date(),
                description: '',
                priority: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                title: ''
            };
            const action = actions.addNewTicket(ticketInfo);

            expect(action).toEqual({
                type: 'ADD_NEW_TICKET',
                payload: ticketInfo
            });
        });
    });

    describe('registerCompany', () => {
        it('return action for registering company ', () => {
            const companyName = '';
            const contactEmail = '';

            const action = actions.registerCompany(companyName, contactEmail);

            expect(action).toEqual({
                type: 'REGISTER_COMPANY',
                payload: {
                    companyName,
                    contactEmail
                }
            });
        });
    });

    describe('resetPassword', () => {
        it('returns action for resetting password', () => {
            const email = '';

            const action = actions.resetPassword(email);

            expect(action).toEqual({
                type: 'RESET_PASSWORD',
                payload: {
                    email
                }
            });
        });
    });

    describe('refreshTickets', () => {
        it('returns action to refresh tickets', () => {
            const action = actions.refreshTickets();

            expect(action).toEqual({
                type: 'REFRESH_TICKETS'
            });
        });
    });

    describe('sendMessage', () => {
        it('returns action for sending comment to ticket', () => {
            const message = '';
            const ticketId = 1;

            const action = actions.sendMessage(message, ticketId);

            expect(action).toEqual({
                type: 'SEND_MESSAGE',
                payload: {
                    message,
                    ticketId
                }
            });
        });
    });

    describe('changeAssignee', () => {
        it('returns action to change assignee', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };
            const assignee: User = {
                companyInfo: {
                    city: '',
                    country: '',
                    enabled: true,
                    id: 1,
                    localIdCode: '',
                    name: '',
                    postalCode: '',
                    premisesAddress: ''
                },
                email: '',
                enabled: true,
                firstName: '',
                id: '',
                lastName: '',
                roles: []
            };

            const action = actions.changeAssignee(ticket, assignee);

            expect(action).toEqual({
                type: 'CHANGE_ASSIGNEE',
                payload: {
                    assignee,
                    ticket
                }
            });
        });
    });

    describe('startProgress', () => {
        it('returns action to start progress', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };

            const action = actions.startProgress(ticket);

            expect(action).toEqual({
                type: 'START_PROGRESS',
                payload: ticket
            });
        });
    });

    describe('finishTicket', () => {
        it('returns action to finish ticket', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };

            const action = actions.finishTicket(ticket);

            expect(action).toEqual({
                type: 'FINISH_TICKET',
                payload: ticket
            });
        });
    });

    describe('closeTicket', () => {
        it('returns action for closing ticket', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };

            const action = actions.closeTicket(ticket);

            expect(action).toEqual({
                type: 'CLOSE_TICKET',
                payload: ticket
            });
        });
    });

    describe('acceptSolution', () => {
        it('returns action for accepting solution', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };

            const action = actions.acceptSolution(ticket);

            expect(action).toEqual({
                type: 'ACCEPT_SOLUTION',
                payload: ticket
            });
        });
    });

    describe('rejectSolution', () => {
        it('returns action for rejecting solution', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };

            const action = actions.rejectSolution(ticket);

            expect(action).toEqual({
                type: 'REJECT_SOLUTION',
                payload: ticket
            });
        });
    });

    describe('setTicketDetails', () => {
        it('returns action for setting ticket details', () => {
            const ticket: Ticket = {
                authorInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                categoryInfo: {
                    enabled: true,
                    id: 1,
                    name: '',
                    subcategories: []
                },
                comments: [],
                companyInfo: {
                    id: 1
                },
                createdAt: '',
                description: '',
                id: 1,
                lastModifiedDate: '',
                priority: '',
                status: '',
                subcategoryInfo: {
                    enabled: true,
                    id: 1,
                    name: ''
                },
                technicianInfo: {
                    companyInfo: {
                        city: '',
                        country: '',
                        enabled: true,
                        id: 1,
                        localIdCode: '',
                        name: '',
                        postalCode: '',
                        premisesAddress: ''
                    },
                    email: '',
                    enabled: true,
                    firstName: '',
                    id: '',
                    lastName: '',
                    roles: []
                },
                title: ''
            };
            const assignees = [] as User[];

            const action = actions.setTicketDetails(ticket, assignees);

            expect(action).toEqual({
                type: 'SET_TICKET_DETAILS',
                payload: {
                    ticket,
                    assignees
                }
            });
        });

        it('returns action for resetting ticket details', () => {
            const ticket = null;
            const assignees = [] as User[];

            const action = actions.setTicketDetails(ticket, assignees);

            expect(action).toEqual({
                type: 'SET_TICKET_DETAILS',
                payload: {
                    ticket,
                    assignees
                }
            });
        });
    });

    describe('showTicketDetails', () => {
        it('returns action for showing ticket details ', () => {
            const ticketId = 1;

            const action = actions.showTicketDetails(ticketId);

            expect(action).toEqual({
                type: 'FETCH_TICKET_DETAILS',
                payload: {
                    ticketId
                }
            });
        });
    });
});
