/* eslint-disable id-length */
import { of } from 'ramda';
import { ActionsObservable, StateObservable } from 'redux-observable';
import { Subject, throwError } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { logInEpic, registerCompanyEpic, resetPasswordEpic } from 'store/epics/prelogin';
import { AddToastAction, ChangeUserAction, HideLoaderAction, LogInAction, RegisterCompanyAction, ResetPasswordAction, SetCategoriesAction, SetTicketsAction, ShowLoaderAction, UpdateTokenAction } from 'types/store/actions';
import { TOAST_LEVELS } from 'utils/constants';

describe('prelogin', () => {
    describe('logInEpic', () => {
        it('handles LOG_IN action properly', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'LOG_IN', payload: { email: 'test@gmail.com', password: 'test123' } } as LogInAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        submitLogin: () => of({ json: () => of({ expires: new Date(2021, 5, 10), token: 'test' }) }),
                        fetchMyself: () => of({ json: () => of({ id: '1', firstName: 'Mateusz', lastName: 'Dąbrowski' }) }),
                        fetchTickets: () => of({ json: () => of([{ id: 2 }]) }),
                        fetchCategories: () => of({ json: () => of([{ id: 3 }]) })
                    }
                };

                const output$ = logInEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(afbcde)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    f: { type: 'UPDATE_TOKEN', payload: { expires: new Date(2021, 5, 10), token: 'test' } } as UpdateTokenAction,
                    b: { type: 'SET_TICKETS', payload: [{ id: 2 }] } as SetTicketsAction,
                    c: { type: 'SET_CATEGORIES', payload: [{ id: 3 }] } as SetCategoriesAction,
                    d: { type: 'CHANGE_USER', payload: { id: '1', firstName: 'Mateusz', lastName: 'Dąbrowski' } } as ChangeUserAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction
                });
            });
        });

        it('handles errors during LOG_IN action properly', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'LOG_IN', payload: { email: 'test@gmail.com', password: 'test123' } } as LogInAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        submitLogin: () => of({ json: () => {
                            throw new Error('Login is no no');
                        } }),
                        fetchMyself: () => of({ json: () => of({ id: '1', firstName: 'Mateusz', lastName: 'Dąbrowski' }) }),
                        fetchTickets: () => of({ json: () => of([{ id: 2 }]) }),
                        fetchCategories: () => of({ json: () => of([{ id: 3 }]) })
                    }
                };

                const output$ = logInEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(afe)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction,
                    f: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to log in', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('registerCompanyEpic', () => {
        it('shows success for REGISTER_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'REGISTER_COMPANY', payload: { companyName: 'RicCorp', contactEmail: 'ric@corp.com' } } as RegisterCompanyAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        submitRegister: () => of({ status: 200 })
                    }
                };

                const output$ = registerCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(age)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction,
                    g: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Company registration submitted!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error for REGISTER_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'REGISTER_COMPANY', payload: { companyName: 'RicCorp', contactEmail: 'ric@corp.com' } } as RegisterCompanyAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        submitRegister: () => of({ status: 400 })
                    }
                };

                const output$ = registerCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(age)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction,
                    g: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Company registration failed!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });

        it('handles error for REGISTER_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'REGISTER_COMPANY', payload: { companyName: 'RicCorp', contactEmail: 'ric@corp.com' } } as RegisterCompanyAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        submitRegister: () => throwError(new Error('Expected error'))
                    }
                };

                const output$ = registerCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(age)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction,
                    g: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Expected error', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('resetPasswordEpic', () => {
        it('shows success for RESET_PASSWORD action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'RESET_PASSWORD', payload: { email: 'ric@corp.com' } } as ResetPasswordAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        resetPassword: () => of({ status: 200 })
                    }
                };

                const output$ = resetPasswordEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(age)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction,
                    g: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Password reset email was sent if account exists!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error for RESET_PASSWORD action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'RESET_PASSWORD', payload: { email: 'ric@corp.com' } } as ResetPasswordAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        resetPassword: () => of({ status: 400 })
                    }
                };

                const output$ = resetPasswordEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(age)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction,
                    g: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to send password reset request!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });

        it('handles error for RESET_PASSWORD action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'RESET_PASSWORD', payload: { email: 'ric@corp.com' } } as ResetPasswordAction
                }));
                const state$ = new StateObservable(new Subject(), null);
                const dependencies = {
                    FormService: {
                        resetPassword: () => throwError(new Error('Expected error'))
                    }
                };

                const output$ = resetPasswordEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(age)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    e: { type: 'HIDE_LOADER' } as HideLoaderAction,
                    g: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Expected error', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });
});
