/* eslint-disable id-length */
import { EditTicketAction, RefreshTicketsAction, AddNewTicketAction, AddToastAction, HideNewTicketAction, SetEditedTicketAction } from 'types/store/actions';
import { ActionsObservable, StateObservable } from 'redux-observable';
import { of, Subject } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { StoreState } from 'store';
import { Dependencies } from 'types/store/common';
import { TOAST_LEVELS } from 'utils/constants';
import { addNewTicketEpic, editTicketEpic } from 'store/epics/newticket';

describe('newticket', () => {
    describe('addNewTicketEpic', () => {
        it('creates new ticket and refreshes tickets list on ADD_NEW_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_NEW_TICKET', payload: { title: 'New title', priority: 'LOW', categoryInfo: {}, subcategoryInfo: {}, description: 'Oh, I forgot...', authorInfo: {}, companyInfo: {}, createdAt: new Date() } } as AddNewTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addNewTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = addNewTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(abc)', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Ticket successfully created!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction,
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'HIDE_NEW_TICKET' } as HideNewTicketAction
                });
            });
        });

        it('shows error when ticket is incomplete on ADD_NEW_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_NEW_TICKET', payload: { title: 'New title', priority: 'LOW', categoryInfo: {}, subcategoryInfo: {} } } as AddNewTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {} as unknown as Dependencies;

                const output$ = addNewTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Ticket is incomplete!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });

        it('shows error when new ticket could not be created on ADD_NEW_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_NEW_TICKET', payload: { title: 'New title', priority: 'LOW', categoryInfo: {}, subcategoryInfo: {}, description: 'Oh, I forgot...', authorInfo: {}, companyInfo: {}, createdAt: new Date() } } as AddNewTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addNewTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = addNewTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to create ticket!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('editTicketEpic', () => {
        it('edits ticket and refreshes tickets list on EDIT_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_TICKET', payload: { title: 'New title', priority: 'LOW', categoryInfo: {}, subcategoryInfo: {}, authorInfo: {}, companyInfo: {}, createdAt: new Date(), description: 'Oh, I forgot...' } } as EditTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = editTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(abc)', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Ticket successfully edited!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction,
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'SET_EDITED_TICKET', payload: null } as SetEditedTicketAction
                });
            });
        });

        it('shows error when ticket is incomplete on EDIT_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_TICKET', payload: { title: 'New title', priority: 'LOW', categoryInfo: {}, subcategoryInfo: {} } } as EditTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {} as unknown as Dependencies;

                const output$ = editTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Ticket is incomplete!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });

        it('shows error when ticket could not be edited on EDIT_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_TICKET', payload: { title: 'New title', priority: 'LOW', categoryInfo: {}, subcategoryInfo: {}, authorInfo: {}, companyInfo: {}, createdAt: new Date(), description: 'Oh, I forgot...' } } as EditTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = editTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to edit ticket!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });
});
