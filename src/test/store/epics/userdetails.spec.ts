/* eslint-disable id-length */
import { ActionsObservable, StateObservable } from 'redux-observable';
import { of, Subject } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import {
    addCompanyEpic,
    addUserEpic,
    fetchCategoriesEpic,
    addSubcategoryEpic,
    addCategoryEpic,
    deleteCategoryEpic,
    editSubcategoryEpic,
    editCategoryEpic,
    editCompanyEpic,
    editUserEpic
} from 'store/epics/userdetails';
import { StoreState } from 'store';
import { AddCategoryAction, AddCompanyAction, AddSubcategoryAction, AddToastAction, AddUserAction, DeleteCategoryAction, EditCategoryAction, EditCompanyAction, EditSubcategoryAction, EditUserAction, FetchCategoriesAction, SetCategoriesAction } from 'types/store/actions';
import { Dependencies } from 'types/store/common';
import { TOAST_LEVELS } from 'utils/constants';

describe('userdetails', () => {
    describe('addCompanyEpic', () => {
        it('adds a new company on ADD_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_COMPANY', payload: { city: 'Warsaw', country: 'Poland', localIdCode: '123456789', premisesAddress: 'Gwiaździsta 21', postalCode: '01-651', name: 'RicCorp' } } as AddCompanyAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addCompany: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = addCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Company successfully added!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when new company information are incomplete on ADD_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_COMPANY' } as AddCompanyAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {} as StoreState);
                const dependencies = {} as unknown as Dependencies;

                const output$ = addCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Company information are incomplete!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });

        it('shows error when new company could not be added on ADD_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_COMPANY', payload: { city: 'Warsaw', country: 'Poland', localIdCode: '123456789', premisesAddress: 'Gwiaździsta 21', postalCode: '01-651', name: 'RicCorp' } } as AddCompanyAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addCompany: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = addCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to add a new company!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('addUserEpic', () => {
        it('adds a new user on ADD_USER action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_USER', payload: { company: {}, email: 'ric@corp.com', firstName: 'Mateusz', lastName: 'Dabrowski', password: 'test123' } } as AddUserAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addUser: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = addUserEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'User successfully added!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when new user information are incomplete on ADD_USER action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_USER' } as AddUserAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {} as StoreState);
                const dependencies = {} as unknown as Dependencies;

                const output$ = addUserEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'User information are incomplete!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });

        it('shows error when new user could not be added on ADD_USER action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_USER', payload: { company: {}, email: 'ric@corp.com', firstName: 'Mateusz', lastName: 'Dabrowski', password: 'test123' } } as AddUserAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addUser: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = addUserEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to add a new user!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('fetchCategoriesEpic', () => {
        it('fetches categories for FETCH_CATEGORIES action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'FETCH_CATEGORIES', payload: { companyId: '1' } } as FetchCategoriesAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        fetchCategories: () => of({ status: 200, json: () => of([{ id: 1 }]) })
                    }
                } as unknown as Dependencies;

                const output$ = fetchCategoriesEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'SET_CATEGORIES', payload: [{ id: 1 }] } as SetCategoriesAction
                });
            });
        });

        it('shows error when categories could not be fetched on FETCH_CATEGORIES action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'FETCH_CATEGORIES', payload: { companyId: '1' } } as FetchCategoriesAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        fetchCategories: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = fetchCategoriesEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to fetch categories!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('addSubcategoryEpic', () => {
        it('adds subcategory for ADD_SUBCATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_SUBCATEGORY', payload: { categoryId: '1', name: 'subcategory' } } as AddSubcategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addSubcategory: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = addSubcategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Subcategory successfully created!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when subcategory could not be added on ADD_SUBCATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_SUBCATEGORY', payload: { categoryId: '1', name: 'subcategory' } } as AddSubcategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addSubcategory: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = addSubcategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to add subcategory!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('addCategoryEpic', () => {
        it('adds category for ADD_CATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_CATEGORY', payload: { name: 'category', companyId: '1' } } as AddCategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addCategory: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = addCategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Category successfully created!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when categories could not be added on ADD_CATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ADD_CATEGORY', payload: { name: 'category', companyId: '1' } } as AddCategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        addCategory: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = addCategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to add category!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('deleteCategoryEpic', () => {
        it('deletes category for DELETE_CATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'DELETE_CATEGORY', payload: { categoryId: '1' } } as DeleteCategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        deleteCategory: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = deleteCategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Category successfully deleted!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when categories could not be deleted on DELETE_CATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'DELETE_CATEGORY', payload: { categoryId: '1' } } as DeleteCategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        deleteCategory: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = deleteCategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to delete category!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('editSubcategoryEpic', () => {
        it('edits subcategory for EDIT_SUBCATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_SUBCATEGORY', payload: { subcategory: { enabled: true, id: 1, name: 'new name' }, categoryId: 1 } } as EditSubcategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editSubcategory: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = editSubcategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Subcategory successfully edited!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when subcategory could not be edited on EDIT_SUBCATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_SUBCATEGORY', payload: { subcategory: { enabled: true, id: 1, name: 'new name' }, categoryId: 1 } } as EditSubcategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editSubcategory: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = editSubcategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to edit subcategory!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('editCategoryEpic', () => {
        it('edits category for EDIT_CATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_CATEGORY', payload: { enabled: true, id: '1', name: 'new name' } } as EditCategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editCategory: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = editCategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Category successfully edited!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when category could not be edited on EDIT_CATEGORY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_CATEGORY', payload: { enabled: true, id: '1', name: 'new name' } } as EditCategoryAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editCategory: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = editCategoryEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to edit category!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('editCompanyEpic', () => {
        it('edits company for EDIT_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_COMPANY', payload: { city: 'Warsaw', country: 'Poland', enabled: true, localIdCode: '123456789', name: 'RicCorp', postalCode: '00-000', premisesAddress: 'Some Polish Street 2137' } } as EditCompanyAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editCompany: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = editCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'Company successfully edited!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when company could not be edited on EDIT_COMPANY action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_COMPANY', payload: { city: 'Warsaw', country: 'Poland', enabled: true, localIdCode: '123456789', name: 'RicCorp', postalCode: '00-000', premisesAddress: 'Some Polish Street 2137' } } as EditCompanyAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editCompany: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = editCompanyEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to edit company!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('editUserEpic', () => {
        it('edits user for EDIT_USER action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_USER', payload: { id: '1', firstName: 'Mateusz', lastName: 'Dąbrowski', roles: [{ role: 'ADMINISTRATOR' }], companyInfo: {}, email: 'ric@corp.com', enabled: true } } as EditUserAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editUser: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = editUserEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Success!', message: 'User successfully edited!', level: TOAST_LEVELS.SUCCESS } } as AddToastAction
                });
            });
        });

        it('shows error when user could not be edited on EDIT_USER action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'EDIT_USER', payload: { id: '1', firstName: 'Mateusz', lastName: 'Dąbrowski', roles: [{ role: 'ADMINISTRATOR' }], companyInfo: {}, email: 'ric@corp.com', enabled: true } } as EditUserAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editUser: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = editUserEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-a', {
                    a: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to edit user!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });
});
