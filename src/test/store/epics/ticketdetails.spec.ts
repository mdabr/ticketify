/* eslint-disable id-length */
import { ActionsObservable, StateObservable } from 'redux-observable';
import { of, Subject, throwError } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { AddToastAction, FetchTicketDetailsAction, RefreshTicketsAction, SendMessageAction, SetTicketsAction, ChangeAssigneeAction, StartProgressAction, FinishTicketAction, CloseTicketAction, AcceptSolutionAction, RejectSolutionAction, SetTicketDetailsAction, ShowLoaderAction, HideLoaderAction } from 'types/store/actions';
import {
    refreshTicketsEpic,
    sendMessageEpic,
    changeAssigneeEpic,
    startProgressEpic,
    finishTicketEpic,
    closeTicketEpic,
    acceptSolutionEpic,
    rejectSolutionEpic,
    fetchTicketDetailsEpic
} from 'store/epics/ticketdetails';
import { TOAST_LEVELS } from 'utils/constants';
import { StoreState } from 'store';
import { Dependencies } from 'types/store/common';
import { User } from 'types/store/user';

describe('ticketdetails', () => {
    describe('refreshTicketsEpic', () => {
        it('refreshes tickets list on REFRESH_TICKETS action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        fetchTickets: () => of({ json: () => of([{ id: 2 }]) })
                    }
                } as unknown as Dependencies;

                const output$ = refreshTicketsEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'SET_TICKETS', payload: [{ id: 2 }] } as SetTicketsAction
                });
            });
        });

        it('shows error when tickets could not be refreshed on REFRESH_TICKETS action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        fetchTickets: () => throwError(new Error('Failed to fetch tickets!'))
                    }
                } as unknown as Dependencies;

                const output$ = refreshTicketsEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to fetch tickets!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('sendMessageEpic', () => {
        it('add comment to ticket on SEND_MESSAGE action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'SEND_MESSAGE', payload: { message: 'Hello', ticketId: 1 } } as SendMessageAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        sendComment: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = sendMessageEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: undefined } } as FetchTicketDetailsAction
                });
            });
        });

        it('shows error when comment could not be added on SEND_MESSAGE action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'SEND_MESSAGE', payload: { message: 'Hello', ticketId: 1 } } as SendMessageAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        sendComment: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = sendMessageEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to send message!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('changeAssigneeEpic', () => {
        it('changes technician in ticket on CHANGE_ASSIGNEE action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'CHANGE_ASSIGNEE', payload: { ticket: { id: 1 }, assignee: { id: '1' } } } as ChangeAssigneeAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = changeAssigneeEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(bc)', {
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: undefined } } as FetchTicketDetailsAction
                });
            });
        });

        it('shows error when technician could not be changed on CHANGE_ASSIGNEE action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'CHANGE_ASSIGNEE', payload: { ticket: { id: 1 }, assignee: { id: '1' } } } as ChangeAssigneeAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = changeAssigneeEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to assign technician!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('startProgressEpic', () => {
        it('starts progress of ticket on START_PROGRESS action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'START_PROGRESS', payload: { id: 1 } } as StartProgressAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = startProgressEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(bc)', {
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: undefined } } as FetchTicketDetailsAction
                });
            });
        });

        it('shows error when progress could not be started on START_PROGRESS action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'START_PROGRESS', payload: { id: 1 } } as StartProgressAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = startProgressEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to start progress!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('finishTicketEpic', () => {
        it('finishes progress of ticket on FINISH_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'FINISH_TICKET', payload: { id: 1 } } as FinishTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = finishTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(bc)', {
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: undefined } } as FetchTicketDetailsAction
                });
            });
        });

        it('shows error when progress could not be finished on FINISH_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'FINISH_TICKET', payload: { id: 1 } } as FinishTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = finishTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to finish ticket!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('closeTicketEpic', () => {
        it('closes ticket on CLOSE_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'CLOSE_TICKET', payload: { id: 1 } } as CloseTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = closeTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(bc)', {
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: undefined } } as FetchTicketDetailsAction
                });
            });
        });

        it('shows error when ticket could not be closed on CLOSE_TICKET action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'CLOSE_TICKET', payload: { id: 1 } } as CloseTicketAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = closeTicketEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to close ticket!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('acceptSolutionEpic', () => {
        it('accepts solution for ticket on ACCEPT_SOLUTION action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ACCEPT_SOLUTION', payload: { id: 1 } } as AcceptSolutionAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = acceptSolutionEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(bc)', {
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: undefined } } as FetchTicketDetailsAction
                });
            });
        });

        it('shows error when solution could not be accepted on ACCEPT_SOLUTION action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'ACCEPT_SOLUTION', payload: { id: 1 } } as AcceptSolutionAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = acceptSolutionEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to accept solution!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('rejectSolutionEpic', () => {
        it('rejects solution for ticket on REJECT_SOLUTION action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'REJECT_SOLUTION', payload: { id: 1 } } as RejectSolutionAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 200 })
                    }
                } as unknown as Dependencies;

                const output$ = rejectSolutionEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(bc)', {
                    b: { type: 'REFRESH_TICKETS' } as RefreshTicketsAction,
                    c: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: undefined } } as FetchTicketDetailsAction
                });
            });
        });

        it('shows error when solution could not be rejected on REJECT_SOLUTION action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'REJECT_SOLUTION', payload: { id: 1 } } as RejectSolutionAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        editTicket: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = rejectSolutionEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to reject solution!', level: TOAST_LEVELS.DANGER } } as AddToastAction
                });
            });
        });
    });

    describe('fetchTicketDetailsEpic', () => {
        it('fetches details for ticket on FETCH_TICKET_DETAILS action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: true } } as FetchTicketDetailsAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        fetchTicketDetails: () => of({ status: 200, json: () => of({ id: 1 }) }),
                        fetchUserWithRole: () => of({ status: 200, json: () => of([]) })
                    }
                } as unknown as Dependencies;

                const output$ = fetchTicketDetailsEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(abc)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    b: { type: 'SET_TICKET_DETAILS', payload: { ticket: { id: 1 }, assignees: [] as User[] } } as SetTicketDetailsAction,
                    c: { type: 'HIDE_LOADER' } as HideLoaderAction
                });
            });
        });

        it('shows error when details could not be fetched on FETCH_TICKET_DETAILS action', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1, withLoader: true } } as FetchTicketDetailsAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        fetchTicketDetails: () => of({ status: 400 }),
                        fetchUserWithRole: () => of({ status: 400 })
                    }
                } as unknown as Dependencies;

                const output$ = fetchTicketDetailsEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-(abc)', {
                    a: { type: 'SHOW_LOADER' } as ShowLoaderAction,
                    b: { type: 'ADD_TOAST', payload: { header: 'Error!', message: 'Failed to fetch ticket details', level: TOAST_LEVELS.DANGER } } as AddToastAction,
                    c: { type: 'HIDE_LOADER' } as HideLoaderAction
                });
            });
        });

        it('shows loader on fetch ticket details start and hides it at the end', () => {
            const testScheduler = new TestScheduler((actual, expected) => {
                expect(actual).toStrictEqual(expected);
            });

            testScheduler.run(({ hot, expectObservable }) => {
                const action$ = new ActionsObservable(hot('-a', {
                    a: { type: 'FETCH_TICKET_DETAILS', payload: { ticketId: 1 } } as FetchTicketDetailsAction
                }));
                const state$ = new StateObservable(new Subject<StoreState>(), {
                    token: {
                        token: ''
                    }
                } as StoreState);
                const dependencies = {
                    FormService: {
                        fetchTicketDetails: () => of({ status: 200, json: () => of({ id: 1 }) }),
                        fetchUserWithRole: () => of({ status: 200, json: () => of([]) })
                    }
                } as unknown as Dependencies;

                const output$ = fetchTicketDetailsEpic(action$, state$, dependencies);

                expectObservable(output$).toBe('-b', {
                    b: { type: 'SET_TICKET_DETAILS', payload: { ticket: { id: 1 }, assignees: [] as User[] } } as SetTicketDetailsAction
                });
            });
        });
    });
});
