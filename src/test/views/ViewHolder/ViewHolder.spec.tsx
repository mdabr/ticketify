import { render } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { MemoryRouter } from 'react-router';
import { UserState } from 'types/store/user';

const { ViewHolder } = jest.requireActual('views');

jest.mock('views', () => ({
    Prelogin: function Prelogin() {
        return <div className="prelogin"></div>;
    },
    MainPage: function MainPage() {
        return <div className="main-page"></div>;
    }
}));

describe('<ViewHolder />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders prelogin for root path', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/']}>
                <ViewHolder user={{ id: null } as UserState}></ViewHolder>
            </MemoryRouter>, container);
        });

        const renderedRoute = container?.children[0];

        expect(container?.children.length).toEqual(1);
        expect(renderedRoute?.classList.contains('prelogin')).toBe(true);
    });

    it('renders prelogin for prelogin path with no user ID', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/prelogin']}>
                <ViewHolder user={{ id: null } as UserState}></ViewHolder>
            </MemoryRouter>, container);
        });

        const renderedRoute = container?.children[0];

        expect(container?.children.length).toEqual(1);
        expect(renderedRoute?.classList.contains('prelogin')).toBe(true);
    });

    it('renders main page for prelogin path with user ID', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/prelogin']}>
                <ViewHolder user={{ id: '123' } as UserState}></ViewHolder>
            </MemoryRouter>, container);
        });

        const renderedRoute = container?.children[0];

        expect(container?.children.length).toEqual(1);
        expect(renderedRoute?.classList.contains('main-page')).toBe(true);
    });

    it('renders prelogin for main page path with no user ID', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/main-page']}>
                <ViewHolder user={{ id: null } as UserState}></ViewHolder>
            </MemoryRouter>, container);
        });

        const renderedRoute = container?.children[0];

        expect(container?.children.length).toEqual(1);
        expect(renderedRoute?.classList.contains('prelogin')).toBe(true);
    });

    it('renders main=page for main page path with user ID', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/main-page']}>
                <ViewHolder user={{ id: '123' } as UserState}></ViewHolder>
            </MemoryRouter>, container);
        });

        const renderedRoute = container?.children[0];

        expect(container?.children.length).toEqual(1);
        expect(renderedRoute?.classList.contains('main-page')).toBe(true);
    });
});
