import { render } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';
import { MainPage } from 'views';


jest.mock('components', () => ({
    Topbar: function Topbar() {
        return <div className="topbar"></div>;
    },
    Search: function Search() {
        return <div className="search"></div>;
    },
    TicketList: function TicketList() {
        return <div className="ticket-list"></div>;
    },
    UserDetails: function UserDetails() {
        return <div className="user-details"></div>;
    },
    TicketDetails: function TicketDetails() {
        return <div className="ticket-details"></div>;
    },
    AddNewTicket: function AddNewTicket() {
        return <div className="add-new-ticket"></div>;
    },
    NewTicket: function NewTicket() {
        return <div className="new-ticket"></div>;
    }
}));

describe('<MainPage />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders main page without popups open', () => {
        const store = createStore(combineReducers({
            userdetails: jest.fn().mockReturnValue({ userDetailsVisible: false }),
            ticketdetails: jest.fn().mockReturnValue({ ticketDetailsVisible: false }),
            newticket: jest.fn().mockReturnValue({ newTicketVisible: false })
        }));

        act(() => {
            render(<Provider store={store}>
                <MainPage />
            </Provider>, container);
        });

        const mainPageContainer = document.querySelector('.main-page-container');

        expect(mainPageContainer?.children[0].classList.contains('topbar')).toBe(true);
        expect(mainPageContainer?.children[1].classList.contains('search')).toBe(true);
        expect(mainPageContainer?.children[2].classList.contains('ticket-list')).toBe(true);
        expect(mainPageContainer?.children[3].classList.contains('add-new-ticket')).toBe(true);
        expect(mainPageContainer?.children[4]).toBeUndefined();
        expect(mainPageContainer?.children[5]).toBeUndefined();
        expect(mainPageContainer?.children[6]).toBeUndefined();
    });

    it('renders main page with user details open', () => {
        const store = createStore(combineReducers({
            userdetails: jest.fn().mockReturnValue({ userDetailsVisible: true }),
            ticketdetails: jest.fn().mockReturnValue({ ticketDetailsVisible: false }),
            newticket: jest.fn().mockReturnValue({ newTicketVisible: false })
        }));

        act(() => {
            render(<Provider store={store}>
                <MainPage />
            </Provider>, container);
        });

        const mainPageContainer = document.querySelector('.main-page-container');

        expect(mainPageContainer?.children[0].classList.contains('topbar')).toBe(true);
        expect(mainPageContainer?.children[1].classList.contains('search')).toBe(true);
        expect(mainPageContainer?.children[2].classList.contains('ticket-list')).toBe(true);
        expect(mainPageContainer?.children[3].classList.contains('add-new-ticket')).toBe(true);
        expect(mainPageContainer?.children[4].classList.contains('user-details')).toBe(true);
        expect(mainPageContainer?.children[5]).toBeUndefined();
        expect(mainPageContainer?.children[6]).toBeUndefined();
    });

    it('renders main page with ticket details open', () => {
        const store = createStore(combineReducers({
            userdetails: jest.fn().mockReturnValue({ userDetailsVisible: false }),
            ticketdetails: jest.fn().mockReturnValue({ ticketDetailsVisible: true }),
            newticket: jest.fn().mockReturnValue({ newTicketVisible: false })
        }));

        act(() => {
            render(<Provider store={store}>
                <MainPage />
            </Provider>, container);
        });

        const mainPageContainer = document.querySelector('.main-page-container');

        expect(mainPageContainer?.children[0].classList.contains('topbar')).toBe(true);
        expect(mainPageContainer?.children[1].classList.contains('search')).toBe(true);
        expect(mainPageContainer?.children[2].classList.contains('ticket-list')).toBe(true);
        expect(mainPageContainer?.children[3].classList.contains('add-new-ticket')).toBe(true);
        expect(mainPageContainer?.children[4].classList.contains('ticket-details')).toBe(true);
        expect(mainPageContainer?.children[5]).toBeUndefined();
        expect(mainPageContainer?.children[6]).toBeUndefined();
    });

    it('renders main page with new ticket open', () => {
        const store = createStore(combineReducers({
            userdetails: jest.fn().mockReturnValue({ userDetailsVisible: false }),
            ticketdetails: jest.fn().mockReturnValue({ ticketDetailsVisible: false }),
            newticket: jest.fn().mockReturnValue({ newTicketVisible: true })
        }));

        act(() => {
            render(<Provider store={store}>
                <MainPage />
            </Provider>, container);
        });

        const mainPageContainer = document.querySelector('.main-page-container');

        expect(mainPageContainer?.children[0].classList.contains('topbar')).toBe(true);
        expect(mainPageContainer?.children[1].classList.contains('search')).toBe(true);
        expect(mainPageContainer?.children[2].classList.contains('ticket-list')).toBe(true);
        expect(mainPageContainer?.children[3].classList.contains('add-new-ticket')).toBe(true);
        expect(mainPageContainer?.children[4].classList.contains('new-ticket')).toBe(true);
        expect(mainPageContainer?.children[5]).toBeUndefined();
        expect(mainPageContainer?.children[6]).toBeUndefined();
    });
});
