import { render } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { MemoryRouter } from 'react-router';
import { Prelogin } from 'views';

jest.mock('components', () => ({
    Login: function Login() {
        return <div className="login"></div>;
    },
    Register: function Register() {
        return <div className="register"></div>;
    },
    SendReset: function SendReset() {
        return <div className="send-reset"></div>;
    }
}));

describe('<Prelogin />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders app title with no routes matching', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/']}>
                <Prelogin></Prelogin>
            </MemoryRouter>, container);
        });

        const preloginContainer = document.querySelector('.prelogin');

        expect(preloginContainer?.children.length).toBe(2);
        expect(preloginContainer?.children[0].textContent).toEqual('Ticketify');
        expect(preloginContainer?.children[1].innerHTML).toEqual('');
    });

    it('renders prelogin route', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/prelogin']}>
                <Prelogin></Prelogin>
            </MemoryRouter>, container);
        });

        const preloginContainer = document.querySelector('.prelogin');
        const loginRegisterContainer = preloginContainer?.children[1];

        expect(preloginContainer?.children.length).toBe(2);
        expect(preloginContainer?.children[0].textContent).toEqual('Ticketify');
        expect(loginRegisterContainer?.children[0].classList.contains('login')).toBe(true);
        expect(loginRegisterContainer?.children[1].classList.contains('register')).toBe(true);
    });

    it('renders forgot password route', () => {
        act(() => {
            render(<MemoryRouter initialEntries={['/prelogin/forgot']}>
                <Prelogin></Prelogin>
            </MemoryRouter>, container);
        });

        const preloginContainer = document.querySelector('.prelogin');
        const loginRegisterContainer = preloginContainer?.children[1];

        expect(preloginContainer?.children.length).toBe(2);
        expect(preloginContainer?.children[0].textContent).toEqual('Ticketify');
        expect(loginRegisterContainer?.children[0].classList.contains('send-reset')).toBe(true);
    });
});
