import { fireEvent, render } from '@testing-library/react';
import { Search } from 'components';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';
import { Ticket } from 'types/store/data';
import userEvent from '@testing-library/user-event';

jest.mock('utils/helpers', () => ({
    capitalizeFirst: jest.fn().mockImplementation(value => value),
    zeroHour: jest.fn().mockReturnValue(new Date(2021, 5, 3, 0, 0, 0)),
    fullHour: jest.fn().mockReturnValue(new Date(2021, 5, 3, 23, 59, 59)),
    isIdentifiableUnique: jest.fn().mockReturnValue(true)
}));

jest.mock('@material-ui/core/MenuItem', () => function MenuItem({ ...props }) {
    return <option value={`${props.value}`} className="menu-item" {...props}></option>;
});

jest.mock('components/MultiSelect/ConfigurableMultiSelect', () => function ConfigurableMultiSelect() {
    return function MultiSelect({ options, ...props }: any) {
        return <select {...props} onChange={event => props.onChange([event.target.value])} className="multiselect" multiple>{options.map((opt: any, index: any) => <option key={index} value={opt}>{opt}</option>)}</select>;
    };
});

jest.mock('utils/uiComponents', () => ({
    ...jest.requireActual('utils/uiComponents'),
    TextField: function TextField({ ...props }) {
        return <input onChange={props.onChange} value={props.value} className="textfield" {...props} />;
    },
    InputLabel: function InputLabel({ children, ...props }: any) {
        return <div className="inputlabel" {...props}>{children}</div>;
    },
    ListItemText: function ListItemText({ children, ...props }: any) {
        return <div className="listitemtext" {...props}>{children}</div>;
    },
    Button: function Button({ children, ...props }: any) {
        return <div className="button" {...props}>{children}</div>;
    },
    Select: function Select({ renderValue, inputProps, ...props }: any) {
        const options = props.value.map((val: any) => val.id);

        if (renderValue([{}]) && inputProps) {
            return <select {...props} onChange={event => props.onChange(event)} multiple>{options.map((opt: any, index: any) => <option key={index} value={`${opt}`}>{`${opt}`}</option>)}</select>;
        }
        return <select {...props} onChange={event => props.onChange(event)} multiple>{options.map((opt: any, index: any) => <option key={index} value={`${opt}`}>{`${opt}`}</option>)}</select>;
    },
    Accordion: function Accordion({ children, ...props }: any) {
        return <div className="accordion" {...props}>{children}</div>;
    },
    AccordionSummary: function AccordionSummary({ children, ...props }: any) {
        return <div className="accordionsummary" {...props}>{children}</div>;
    },
    AccordionDetails: function AccordionDetails({ children, ...props }: any) {
        return <div className="accordiondetails" {...props}>{children}</div>;
    },
    Checkbox: function Checkbox({ children, ...props }: any) {
        return <div className="checkbox" {...props}>{children}</div>;
    },
    FilterIcon: function FilterIcon({ children, ...props }: any) {
        return <div className="filtericon" {...props}>{children}</div>;
    },
    KeyboardDatePicker: function KeyboardDatePicker({ children, ...props }: any) {
        return <div className="keyboarddatepicker" {...props}>{children}</div>;
    }
}));

describe('<Search />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders search menu', () => {
        const dataReducer = jest.fn().mockReturnValue({
            categories: [{
                id: 1,
                name: 'abc',
                enabled: true,
                subcategories: [{
                    id: 2,
                    name: 'def',
                    enabled: true
                }]
            }],
            tickets: []
        });
        const store = createStore(combineReducers({
            data: dataReducer
        }));

        render(<Provider store={store}>
            <Search />
        </Provider>, { container: container as HTMLDivElement });

        const searchContainer = document.querySelector('.search-container');

        expect(searchContainer?.children.length).toBe(1);
        expect(searchContainer?.children[0].children[0].textContent).toBe('Filter');
    });

    it('dispatches filter tickets action on submit', () => {
        const dataReducer = jest.fn().mockReturnValue({
            data: {
                tickets: [{
                    createdAt: new Date(2021, 5, 3, 23, 59, 59),
                    lastModifiedDate: new Date(2021, 5, 3, 23, 59, 59),
                    authorInfo: {
                        id: '1',
                        firstName: 'gfd',
                        lastName: 'fgh',
                        companyInfo: {
                            id: 1,
                            name: 'asd'
                        }
                    },
                    companyInfo: {
                        id: 1
                    },
                    categoryInfo: {
                        enabled: true,
                        id: 1,
                        name: 'abc',
                        subcategories: [{
                            id: 2,
                            name: 'def',
                            enabled: true
                        }]
                    }
                } as Ticket],
                categories: [{
                    enabled: true,
                    id: 1,
                    name: 'abc',
                    subcategories: [{
                        id: 2,
                        name: 'def',
                        enabled: true
                    }]
                }]
            }
        });
        const store = createStore(dataReducer);

        render(<Provider store={store}>
            <Search />
        </Provider>, { container: container as HTMLDivElement });

        const searchContainer = document.querySelector('.search-container');

        fireEvent.change(searchContainer?.children[0].children[1].children[0] as Element, { target: { value: '123' } });
        fireEvent.change(searchContainer?.children[0].children[1].children[1] as Element, { target: { value: 'Some title' } });
        userEvent.selectOptions(searchContainer?.children[0].children[1].children[3] as Element, ['LOW']);
        userEvent.selectOptions(searchContainer?.children[0].children[1].children[5] as Element, ['UNASSIGNED']);
        fireEvent.click(searchContainer?.children[0].children[2] as Element);

        expect(dataReducer).toBeCalledWith(expect.anything(), {
            type: 'FILTER_TICKETS',
            payload: {
                status: ['UNASSIGNED'],
                subcategories: [],
                assignees: [],
                categories: [],
                companies: [],
                creators: [],
                id: '123',
                modifiedFrom: expect.anything(),
                modifiedTo: expect.anything(),
                priorities: ['LOW'],
                startedFrom: expect.anything(),
                startedTo: expect.anything(),
                title: 'Some title'
            }
        });
    });
});
