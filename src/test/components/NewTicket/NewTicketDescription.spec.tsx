import { fireEvent, render } from '@testing-library/react';
import NewTicketDescription from 'components/NewTicket/NewTicketDescription';

jest.mock('utils/uiComponents', () => ({
    DescTextField: function DescTextField({ rowsMax, multiline, ...props }: any) {
        if (!rowsMax || !multiline) {
            return <input {...props} />;
        }

        return <input {...props} />;
    }
}));

describe('<NewTicketDescription />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders description text field', () => {
        render(<NewTicketDescription description="" setDescription={jest.fn()} />, { container: container as HTMLDivElement });

        const newTicketDescription = document.querySelector('.new-ticket-description');

        expect(newTicketDescription?.children[0]).toBeDefined();
    });

    it('updates description on change', () => {
        const setDesription = jest.fn();

        render(<NewTicketDescription description="" setDescription={setDesription} />, { container: container as HTMLDivElement });

        const newTicketDescription = document.querySelector('.new-ticket-description');

        fireEvent.change(newTicketDescription?.querySelector('input') as Element, { target: { value: 'Some description' } });

        expect(setDesription).toBeCalledWith('Some description');
    });

    it('gets trimmed if it exceeds length', () => {
        const setDesription = jest.fn();

        render(<NewTicketDescription description="" setDescription={setDesription} />, { container: container as HTMLDivElement });

        const newTicketDescription = document.querySelector('.new-ticket-description');

        fireEvent.change(newTicketDescription?.querySelector('input') as Element, { target: { value: 'Some descriptionSome descriptionSome descriptionSo1me descriptionSome descriptionSome descriptionSome2 descriptionSome descriptionSome descriptionSome d3escriptionSome descriptionSome descriptionSome des4criptionSome descriptionSome descriptionSome descr5iptionSome descriptionSome descriptionSome descrip6tionSome descriptionSome descriptionSome descripti7onSome description' } });

        expect(setDesription).toBeCalledWith('Some descriptionSome descriptionSome descriptionSo1me descriptionSome descriptionSome descriptionSome2 descriptionSome descriptionSome descriptionSome d3escriptionSome descriptionSome descriptionSome ');
    });
});
