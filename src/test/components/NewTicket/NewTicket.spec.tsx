import { fireEvent, render } from '@testing-library/react';
import { NewTicket } from 'components';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';

jest.mock('utils/uiComponents', () => ({
    List: function List({ children }: any) {
        return <div className="list">{children}</div>;
    },
    ListItem: function ListItem({ selected, ...props }: any) {
        return <div {...props} className="list-item">{selected ? 'TRUE' : 'FALSE'}</div>;
    }
}));

jest.mock('components/NewTicket/NewTicketMain', () => function NewTicketMain({ onSubmit }: any) {
    return <div onClick={onSubmit} className="new-ticket-main"></div>;
});
jest.mock('components/NewTicket/NewTicketDescription', () => function NewTicketDescription() {
    return <div className="new-ticket-description"></div>;
});
jest.mock('utils/ticket');
jest.mock('utils/helpers');

describe('<NewTicket />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders new ticket popup with main view opened', () => {
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({ categories: [] }),
            user: jest.fn().mockReturnValue({}),
            token: jest.fn().mockReturnValue({}),
            newticket: jest.fn().mockReturnValue({})
        }));

        render(<Provider store={store}><NewTicket /></Provider>, { container: container as HTMLDivElement });

        expect(container?.children.length).toBe(2);
        expect(container?.children[0].classList.contains('new-ticket-backdrop')).toBe(true);
        expect(container?.children[1].classList.contains('new-ticket-container')).toBe(true);
        expect(container?.children[1].children[0].children.length).toBe(2);
        expect(container?.children[1].children[0].children[0].textContent).toBe('TRUE');
        expect(container?.children[1].children[1].classList.contains('new-ticket-main')).toBe(true);
        expect(container?.children[1].children[1].classList.contains('new-ticket-description')).toBe(false);
        expect(container?.children[1].children[2].classList.contains('close-button')).toBe(true);
    });

    it('renders prefilled new ticket popup with main view opened', () => {
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({ categories: [{}] }),
            user: jest.fn().mockReturnValue({}),
            token: jest.fn().mockReturnValue({}),
            newticket: jest.fn().mockReturnValue({ editedTicket: {} })
        }));

        render(<Provider store={store}><NewTicket /></Provider>, { container: container as HTMLDivElement });

        expect(container?.children.length).toBe(2);
        expect(container?.children[0].classList.contains('new-ticket-backdrop')).toBe(true);
        expect(container?.children[1].classList.contains('new-ticket-container')).toBe(true);
        expect(container?.children[1].children[0].children.length).toBe(2);
        expect(container?.children[1].children[0].children[0].textContent).toBe('TRUE');
        expect(container?.children[1].children[1].classList.contains('new-ticket-main')).toBe(true);
        expect(container?.children[1].children[1].classList.contains('new-ticket-description')).toBe(false);
        expect(container?.children[1].children[2].classList.contains('close-button')).toBe(true);
    });

    it('renders new ticket popup with description view opened', () => {
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({ categories: [{}] }),
            user: jest.fn().mockReturnValue({}),
            token: jest.fn().mockReturnValue({}),
            newticket: jest.fn().mockReturnValue({ editedTicket: {} })
        }));

        render(<Provider store={store}><NewTicket /></Provider>, { container: container as HTMLDivElement });

        fireEvent.click(document.querySelectorAll('.list-item')[1]);

        expect(container?.children.length).toBe(2);
        expect(container?.children[0].classList.contains('new-ticket-backdrop')).toBe(true);
        expect(container?.children[1].classList.contains('new-ticket-container')).toBe(true);
        expect(container?.children[1].children[0].children.length).toBe(2);
        expect(container?.children[1].children[0].children[1].textContent).toBe('TRUE');
        expect(container?.children[1].children[1].classList.contains('new-ticket-main')).toBe(false);
        expect(container?.children[1].children[1].classList.contains('new-ticket-description')).toBe(true);
        expect(container?.children[1].children[2].classList.contains('close-button')).toBe(true);
    });

    it('dispatches hiding new ticket on close clicked', () => {
        const reducer = jest.fn().mockReturnValue({ data: { categories: [{}] }, token: {}, user: {}, newticket: { editedTicket: {} } });
        const store = createStore(reducer);

        render(<Provider store={store}><NewTicket /></Provider>, { container: container as HTMLDivElement });

        fireEvent.click(document.querySelector('.close-button') as Element);

        expect(reducer).toBeCalledWith(expect.anything(), { type: 'HIDE_NEW_TICKET' });
    });

    it('dispatches editing ticket on save clicked', () => {
        const reducer = jest.fn().mockReturnValue({ data: { categories: [{}] }, token: { token: '1234' }, user: {}, newticket: { editedTicket: {} } });
        const store = createStore(reducer);

        render(<Provider store={store}><NewTicket /></Provider>, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');

        fireEvent.click(newTicketMain as Element);

        expect(reducer).toBeCalledWith(expect.anything(), {
            type: 'EDIT_TICKET',
            payload: expect.any(Object)
        });
    });

    it('dispatches adding new ticket on save clicked', () => {
        const reducer = jest.fn().mockReturnValue({ data: { categories: [{}] }, token: { token: '1234' }, user: {}, newticket: { editedTicket: null } });
        const store = createStore(reducer);

        render(<Provider store={store}><NewTicket /></Provider>, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');

        fireEvent.click(newTicketMain as Element);

        expect(reducer).toBeCalledWith(expect.anything(), {
            type: 'ADD_NEW_TICKET',
            payload: expect.any(Object)
        });
    });
});
