import { fireEvent, render } from '@testing-library/react';
import NewTicketMain from 'components/NewTicket/NewTicketMain';
import { Category } from 'types/store/data';

jest.mock('@material-ui/core/MenuItem', () => function MenuItem({ value, children, ...props }: any) {
    return <option value={value} {...props}>{children}</option>;
});
jest.mock('utils/helpers');
jest.mock('utils/uiComponents', () => ({
    TextField: function TextField({ ...props }) {
        return <input {...props} className="text-field"></input>;
    },
    InputLabel: function InputLabel() {
        return <div className="input-label"></div>;
    },
    ListItemText: function ListItemText({ ...props }) {
        return <div {...props} className="list-item-text"></div>;
    },
    Button: function Button({ children }: any) {
        return <div className="button">{children}</div>;
    },
    Select: function Select({ value, onChange, children, renderValue }: any) {
        return <select className="select" value={value} onChange={onChange}>
            {children.map((child: JSX.Element, index: number) => <child.type {...child.props} key={index}>{renderValue(child.props.value)}</child.type>)}
        </select>;
    }
}));

describe('<NewTicketMain />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('render new ticket main part', () => {
        const onSubmit = jest.fn();
        const stateHandlers = {
            title: '',
            setTitle: jest.fn(),
            priority: '',
            setPriority: jest.fn(),
            category: {
                id: 1,
                name: 'abcd',
                enabled: true,
                subcategories: [{
                    id: 1,
                    name: 'asdf',
                    enabled: true
                }]
            },
            setCategory: jest.fn(),
            subcategory: '',
            setSubcategory: jest.fn()
        };
        const categories = [{
            id: 1,
            name: 'abcd',
            enabled: true,
            subcategories: [{
                id: 1,
                name: 'asdf',
                enabled: true
            }]
        }];

        render(<NewTicketMain stateHandlers={stateHandlers} categories={categories} onSubmit={onSubmit} editing={false} />, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');

        expect(newTicketMain?.children.length).toBe(2);
        expect(newTicketMain?.children[0].classList.contains('inputs')).toBe(true);
        expect(newTicketMain?.children[1].textContent).toBe('Submit');
    });

    it('updates state on title change', () => {
        const onSubmit = jest.fn();
        const stateHandlers = {
            title: '',
            setTitle: jest.fn(),
            priority: '',
            setPriority: jest.fn(),
            category: '' as unknown as Category,
            setCategory: jest.fn(),
            subcategory: '',
            setSubcategory: jest.fn()
        };

        render(<NewTicketMain stateHandlers={stateHandlers} categories={[]} onSubmit={onSubmit} editing={false} />, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');
        const titleTextFieldInput = newTicketMain?.children[0].children[0].children[0];

        fireEvent.change(titleTextFieldInput as Element, { target: { value: 'test' } });

        expect(stateHandlers.setTitle).toBeCalledWith('test');
    });

    it('updates state on priority change', () => {
        const onSubmit = jest.fn();
        const stateHandlers = {
            title: '',
            setTitle: jest.fn(),
            priority: '',
            setPriority: jest.fn(),
            category: {
                id: 1,
                name: 'abcd',
                enabled: true,
                subcategories: [{
                    id: 1,
                    name: 'asdf',
                    enabled: true
                }]
            },
            setCategory: jest.fn(),
            subcategory: '',
            setSubcategory: jest.fn()
        };
        const categories = [{
            id: 1,
            name: 'abcd',
            enabled: true,
            subcategories: [{
                id: 1,
                name: 'asdf',
                enabled: true
            }]
        }];

        render(<NewTicketMain stateHandlers={stateHandlers} categories={categories} onSubmit={onSubmit} editing={false} />, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');
        const prioritySelect = newTicketMain?.children[0].children[0].children[1].querySelector('select');

        fireEvent.change(prioritySelect as Element, { target: { value: 'LOW' } });

        expect(stateHandlers.setPriority).toBeCalledWith('LOW');
    });

    it('updates state on category change', () => {
        const onSubmit = jest.fn();
        const stateHandlers = {
            title: '',
            setTitle: jest.fn(),
            priority: '',
            setPriority: jest.fn(),
            category: {
                id: 1,
                name: 'abcd',
                enabled: true,
                subcategories: [{
                    id: 1,
                    name: 'asdf',
                    enabled: true
                }]
            },
            setCategory: jest.fn(),
            subcategory: '',
            setSubcategory: jest.fn()
        };
        const categories = [{
            id: 1,
            name: 'abcd',
            enabled: true,
            subcategories: [{
                id: 1,
                name: 'asdf',
                enabled: true
            }]
        }];

        render(<NewTicketMain stateHandlers={stateHandlers} categories={categories} onSubmit={onSubmit} editing={false} />, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');
        const categorySelect = newTicketMain?.children[0].children[1].children[0].querySelector('select');
        const option = categorySelect?.children[0];

        fireEvent.change(categorySelect as Element, { target: { value: 1 } });
        fireEvent.click(option as Element);

        expect(stateHandlers.setCategory).toBeCalledWith({
            id: 1,
            name: 'abcd',
            enabled: true,
            subcategories: [{
                id: 1,
                name: 'asdf',
                enabled: true
            }]
        });
        expect(stateHandlers.setSubcategory).toBeCalledWith({
            id: 1,
            name: 'asdf',
            enabled: true
        });
    });

    it('updates state on subcategory change', () => {
        const onSubmit = jest.fn();
        const stateHandlers = {
            title: '',
            setTitle: jest.fn(),
            priority: '',
            setPriority: jest.fn(),
            category: {
                id: 1,
                name: 'abcd',
                enabled: true,
                subcategories: [{
                    id: 1,
                    name: 'asdf',
                    enabled: true
                }]
            },
            setCategory: jest.fn(),
            subcategory: '',
            setSubcategory: jest.fn()
        };
        const categories = [{
            id: 1,
            name: 'abcd',
            enabled: true,
            subcategories: [{
                id: 1,
                name: 'asdf',
                enabled: true
            }]
        }];

        render(<NewTicketMain stateHandlers={stateHandlers} categories={categories} onSubmit={onSubmit} editing={false} />, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');
        const subcategorySelect = newTicketMain?.children[0].children[1].children[1].querySelector('select');

        fireEvent.change(subcategorySelect as Element, { target: { value: 1 } });

        expect(stateHandlers.setSubcategory).toBeCalledWith({
            id: 1,
            name: 'asdf',
            enabled: true
        });
    });

    it('text on button changes to save when editing ticket', () => {
        const onSubmit = jest.fn();
        const stateHandlers = {
            title: '',
            setTitle: jest.fn(),
            priority: '',
            setPriority: jest.fn(),
            category: {
                id: 1,
                name: 'abcd',
                enabled: true,
                subcategories: [{
                    id: 1,
                    name: 'asdf',
                    enabled: true
                }]
            },
            setCategory: jest.fn(),
            subcategory: '',
            setSubcategory: jest.fn()
        };
        const categories = [{
            id: 1,
            name: 'abcd',
            enabled: true,
            subcategories: [{
                id: 1,
                name: 'asdf',
                enabled: true
            }]
        }];

        render(<NewTicketMain stateHandlers={stateHandlers} categories={categories} onSubmit={onSubmit} editing={true} />, { container: container as HTMLDivElement });

        const newTicketMain = document.querySelector('.new-ticket-main');

        expect(newTicketMain?.children[1].textContent).toBe('Save');
    });
});
