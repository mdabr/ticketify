import { Login } from 'components';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router';
import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { fireEvent } from '@testing-library/react';
import { logIn } from 'store/actions';

jest.mock('store/actions', () => ({
    logIn: jest.fn()
}));

describe('<Login />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders a log in form', () => {
        const sagaMiddleware = createSagaMiddleware();
        const store = createStore(
            jest.fn(),
            applyMiddleware(sagaMiddleware)
        );
        const rootSagaMock = jest.fn(function *rootSaga() {
            yield 1;
        });
        sagaMiddleware.run(rootSagaMock);

        render(<Provider store={store}>
            <MemoryRouter>
                <Login></Login>
            </MemoryRouter>
        </Provider>, container);

        const loginContainer = document.querySelector('.login-container');
        const form = document.querySelector('form');

        expect(loginContainer?.children[0].textContent).toBe('Log in');
        expect(form?.children.length).toBe(4);
        expect(form?.children[0].textContent).toBe('Login');
        expect(form?.children[1].textContent).toBe('Password');
        expect((form?.children[2] as HTMLInputElement).value).toBe('Log in');
        expect(form?.children[3].textContent).toBe('Forgot password? Click here to reset');
    });

    it('dispatches log in action on submit clicked', () => {
        (logIn as jest.Mock).mockImplementationOnce((login, password) => ({
            type: 'LOG_IN',
            login,
            password
        }));

        const sagaMiddleware = createSagaMiddleware();
        const store = createStore(
            jest.fn(),
            applyMiddleware(sagaMiddleware)
        );
        const rootSagaMock = jest.fn(function *rootSaga() {
            yield 1;
        });
        sagaMiddleware.run(rootSagaMock);

        render(<Provider store={store}>
            <MemoryRouter>
                <Login></Login>
            </MemoryRouter>
        </Provider>, container);

        const loginInput = document.querySelector('#LOGIN') as HTMLInputElement;
        const passwordInput = document.querySelector('#PASSWORD') as HTMLInputElement;
        const submitButton = document.querySelector('input[type=submit]') as HTMLInputElement;

        const login = 'test@gmail.com';
        const password = 'test123';

        fireEvent.change(loginInput, { target: { value: login } });
        fireEvent.change(passwordInput, { target: { value: password } });
        fireEvent.click(submitButton);

        expect(logIn).toBeCalledWith(login, password);
    });
});
