import { fireEvent, render } from '@testing-library/react';
import MultiSelect from 'components/MultiSelect/MultiSelect';

jest.mock('utils/uiComponents', () => ({
    Checkbox: function Checkbox({ checked }: any) {
        return <div className="check-box">{checked ? 'YES' : 'NO'}</div>;
    },
    ListItemText: function ListItemText() {
        return <div className="list-item-text"></div>;
    },
    Select: function Select({ value, onChange, children, renderValue }: any) {
        return <select multiple className="select" value={value} onChange={onChange}>
            {children.map((child: JSX.Element, index: number) => <child.type {...child.props} key={index}>{renderValue([child.props.value])} {child.props.children[0].props.checked ? 'TRUE' : 'FALSE'}</child.type>)}
        </select>;
    }
}));

jest.mock('@material-ui/core', () => ({
    MenuItem: function MenuItem({ value, children }: any) {
        return <option value={value}>{children}</option>;
    }
}));

describe('<MultiSelect />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders preconfigured multi select', () => {
        const onChange = jest.fn();

        render(<MultiSelect options={['option 1', 'option 2']} onChange={onChange} />, { baseElement: container as HTMLDivElement });

        const multiSelect = document.querySelector('.select');

        expect(multiSelect?.children.length).toBe(2);
    });

    it('checks elements on change', () => {
        render(<MultiSelect options={['option 1', 'option 2']}/>, { baseElement: container as HTMLDivElement });

        const multiSelect = document.querySelector('.select') as Element;

        fireEvent.change(multiSelect, { target: { value: ['option 1'] } });

        expect(multiSelect.children[0].textContent).toBe('Option 1 TRUE');
    });

    it('uses passed onChange to return elements', () => {
        const onChange = jest.fn();

        render(<MultiSelect options={['option 1', 'option 2']} onChange={onChange} />, { baseElement: container as HTMLDivElement });

        const multiSelect = document.querySelector('.select') as Element;

        fireEvent.change(multiSelect, { target: { value: ['option 1'] } });

        expect(onChange).toBeCalledWith('option 1');
    });
});
