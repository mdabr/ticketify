import { render } from '@testing-library/react';
import ConfigurableMultiSelect from 'components/MultiSelect/ConfigurableMultiSelect';

describe('<ConfigurableMultiSelect />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders a preconfigured multi select', () => {
        const ConfiguredMultiSelect = ConfigurableMultiSelect({ name: 'Configured' });

        render(<ConfiguredMultiSelect options={[]} />, { baseElement: container as HTMLDivElement });

        const multiSelect = document.querySelector('.MuiSelect-root');
        const input = document.querySelector('input');

        expect(input?.name).toBe('Configured');
        expect(multiSelect?.id).toBe('mui-component-select-Configured');
    });

    it('renders unconfigured multi select', () => {
        const ConfiguredMultiSelect = ConfigurableMultiSelect({});

        render(<ConfiguredMultiSelect options={[]} />, { baseElement: container as HTMLDivElement });

        const multiSelect = document.querySelector('.MuiSelect-root');
        const input = document.querySelector('input');

        expect(input?.name).not.toBe('Configured');
        expect(multiSelect?.id).not.toBe('mui-component-select-Configured');
    });
});
