import { fireEvent, getByRole, getByText, render } from '@testing-library/react';
import { AssignTechnician } from 'components/TicketDetails/actions';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Ticket } from 'types/store/data';
import { User } from 'types/store/user';
import { getRole } from 'utils/helpers';

jest.mock('utils/helpers', () => ({
    getRole: jest.fn()
}));

describe('<AssignTechnician />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders "assign technician" select for administrator', () => {
        (getRole as jest.Mock).mockReturnValueOnce('ADMINISTRATOR');
        const userReducer = {
            roles: [{ role: 'ADMINISTRATOR' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {} as Ticket;
        const assignees = [{
            id: '1',
            firstName: 'Mateusz',
            lastName: 'Dąbrowski'
        } as User];

        render(<Provider store={store}><AssignTechnician assignees={assignees} ticket={ticket}/></Provider>);

        const selectLabel = document.querySelector('label');
        const select = document.querySelector('.MuiSelect-select');

        expect(selectLabel).toBeDefined();
        expect(selectLabel?.textContent).toBe('Assign technician: ');
        expect(select).toBeDefined();
    });

    it('does not render "assign technician" select for non-administrator', () => {
        (getRole as jest.Mock).mockReturnValueOnce('EMPLOYEE');
        const userReducer = jest.fn().mockReturnValue({
            roles: [{ role: 'EMPLOYEE' }]
        } as User);
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {} as Ticket;
        const assignees = [] as User[];

        render(<Provider store={store}><AssignTechnician assignees={assignees} ticket={ticket}/></Provider>);

        const selectLabel = document.querySelector('label');
        const select = document.querySelector('.MuiSelect-select');

        expect(selectLabel).toBeNull();
        expect(select).toBeNull();
    });

    it('emits CHANGE_ASSIGNEE on select change for administrator', () => {
        (getRole as jest.Mock).mockReturnValueOnce('ADMINISTRATOR');
        const userReducer = jest.fn().mockReturnValue({
            roles: [{ role: 'ADMINISTRATOR' }]
        } as User);
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            id: 13
        } as Ticket;
        const assignees = [{
            id: '37',
            firstName: 'Mateusz',
            lastName: 'Dąbrowski'
        }] as User[];

        render(<Provider store={store}><AssignTechnician assignees={assignees} ticket={ticket}/></Provider>, { container: container as HTMLDivElement });

        fireEvent.mouseDown(getByRole(container as HTMLElement, 'button'));
        fireEvent.click(getByText(document.body as HTMLElement, 'Mateusz Dąbrowski'));

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'CHANGE_ASSIGNEE',
            payload: {
                ticket: {
                    id: 13
                },
                assignee: {
                    id: '37',
                    firstName: 'Mateusz',
                    lastName: 'Dąbrowski'
                }
            }
        });
    });
});
