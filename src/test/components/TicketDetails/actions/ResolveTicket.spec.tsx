import { fireEvent, render } from '@testing-library/react';
import { ResolveTicket } from 'components/TicketDetails/actions';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Ticket } from 'types/store/data';
import { User } from 'types/store/user';

describe('<ResolveTicket />', () => {
    it('renders buttons for resolving ticket for technician assigned to the ticket', () => {
        const userReducer = {
            id: '1',
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            technicianInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><ResolveTicket ticket={ticket} /></Provider>);

        const container = document.querySelector('div');

        const doneButton = container?.children[0] as HTMLButtonElement;
        const closeButton = container?.children[1] as HTMLButtonElement;

        expect(doneButton).toBeDefined();
        expect(doneButton.nodeName).toBe('BUTTON');
        expect(closeButton).toBeDefined();
        expect(closeButton.nodeName).toBe('BUTTON');
    });

    it('renders nothing for technician not assigned to the ticket', () => {
        const userReducer = {
            id: '2',
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            technicianInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><ResolveTicket ticket={ticket} /></Provider>);

        const container = document.querySelector('div');

        const doneButton = container?.children[0] as HTMLButtonElement;
        const closeButton = container?.children[1] as HTMLButtonElement;

        expect(doneButton).not.toBeDefined();
        expect(closeButton).not.toBeDefined();
    });

    it('dispatches FINISH_TICKET action on done button clicked', () => {
        const userReducer = {
            id: '1',
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            technicianInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><ResolveTicket ticket={ticket} /></Provider>);

        const container = document.querySelector('div');
        const doneButton = container?.children[0] as HTMLButtonElement;

        fireEvent.click(doneButton);

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'FINISH_TICKET',
            payload: {
                technicianInfo: {
                    id: '1'
                }
            }
        });
    });

    it('dispatches CLOSE_TICKET action on done button clicked', () => {
        const userReducer = {
            id: '1',
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            technicianInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><ResolveTicket ticket={ticket} /></Provider>);

        const container = document.querySelector('div');
        const closeButton = container?.children[1] as HTMLButtonElement;

        fireEvent.click(closeButton);

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'CLOSE_TICKET',
            payload: {
                technicianInfo: {
                    id: '1'
                }
            }
        });
    });
});
