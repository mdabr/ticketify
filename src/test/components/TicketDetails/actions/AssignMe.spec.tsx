import { fireEvent, render } from '@testing-library/react';
import { AssignMe } from 'components/TicketDetails/actions';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Ticket } from 'types/store/data';
import { User } from 'types/store/user';
import { getRole } from 'utils/helpers';

jest.mock('utils/helpers', () => ({
    getRole: jest.fn()
}));

describe('<AssignMe />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders "assign me" button for technician', () => {
        (getRole as jest.Mock).mockReturnValueOnce('TECHNICIAN');
        const userReducer = jest.fn().mockReturnValue({
            roles: [{ role: 'TECHNICIAN' }]
        } as User);
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {} as Ticket;

        render(<Provider store={store}><AssignMe ticket={ticket}/></Provider>);

        const assignMeButton = document.querySelector('button');

        expect(assignMeButton).toBeDefined();
        expect(assignMeButton?.textContent).toBe('Assign me');
    });

    it('does not render "assign me" button for non-technician', () => {
        (getRole as jest.Mock).mockReturnValueOnce('EMPLOYEE');
        const userReducer = jest.fn().mockReturnValue({
            roles: [{ role: 'EMPLOYEE' }]
        } as User);
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {} as Ticket;

        render(<Provider store={store}><AssignMe ticket={ticket}/></Provider>);

        const assignMeButton = document.querySelector('button');

        expect(assignMeButton).toBeNull();
    });

    it('emits CHANGE_ASSIGNEE for button clicked by technician', () => {
        (getRole as jest.Mock).mockReturnValueOnce('TECHNICIAN');
        const userReducer = {
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {} as Ticket;

        render(<Provider store={store}><AssignMe ticket={ticket}/></Provider>);

        const assignMeButton = document.querySelector('button');

        fireEvent.click(assignMeButton as HTMLButtonElement);

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'CHANGE_ASSIGNEE',
            payload: {
                assignee: {
                    roles: [{ role: 'TECHNICIAN' }]
                },
                ticket: {}
            }
        });
    });
});
