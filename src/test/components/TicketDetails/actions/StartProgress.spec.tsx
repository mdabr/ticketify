import { fireEvent, render } from '@testing-library/react';
import { StartProgress } from 'components/TicketDetails/actions';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Ticket } from 'types/store/data';
import { User } from 'types/store/user';

describe('<StartProgress />', () => {
    it('renders progress for starting progress for technician assigned to the ticket', () => {
        const userReducer = {
            id: '1',
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            technicianInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><StartProgress ticket={ticket} /></Provider>);

        const container = document.querySelector('div');

        expect(container?.children[0]).toBeDefined();
        expect(container?.children[0].nodeName).toBe('BUTTON');
    });

    it('renders nothing for technician not assigned to the ticket', () => {
        const userReducer = {
            id: '1',
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            technicianInfo: {
                id: '2'
            }
        } as Ticket;

        render(<Provider store={store}><StartProgress ticket={ticket} /></Provider>);

        const container = document.querySelector('div');

        expect(container?.children[0]).not.toBeDefined();
    });

    it('dispatches START_PROGRESS action on start progress button clicked', () => {
        const userReducer = {
            id: '1',
            roles: [{ role: 'TECHNICIAN' }]
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            technicianInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><StartProgress ticket={ticket} /></Provider>);

        const container = document.querySelector('div');
        const startProgressButton = container?.children[0] as HTMLButtonElement;

        fireEvent.click(startProgressButton);

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'START_PROGRESS',
            payload: {
                technicianInfo: {
                    id: '1'
                }
            }
        });
    });
});
