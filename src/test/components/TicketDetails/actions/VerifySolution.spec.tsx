import { fireEvent, render } from '@testing-library/react';
import { VerifySolution } from 'components/TicketDetails/actions';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Ticket } from 'types/store/data';
import { User } from 'types/store/user';

describe('<VerifySolution />', () => {
    it('renders buttons for verifying solution for the ticket author', () => {
        const userReducer = {
            id: '1'
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            authorInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><VerifySolution ticket={ticket} /></Provider>);

        const container = document.querySelector('div');

        expect(container?.children[0]).toBeDefined();
        expect(container?.children[0].textContent).toBe('Accept');
        expect(container?.children[1]).toBeDefined();
        expect(container?.children[1].textContent).toBe('Reject');
    });

    it('renders nothing for user that is not ticket author', () => {
        const userReducer = {
            id: '1'
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            authorInfo: {
                id: '2'
            }
        } as Ticket;

        render(<Provider store={store}><VerifySolution ticket={ticket} /></Provider>);

        const container = document.querySelector('div');

        expect(container?.children[0]).not.toBeDefined();
    });

    it('dispatches ACCEPT_SOLUTION action on accept button clicked', () => {
        const userReducer = {
            id: '1'
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            authorInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><VerifySolution ticket={ticket} /></Provider>);

        const container = document.querySelector('div');
        const acceptButton = container?.children[0] as HTMLButtonElement;

        fireEvent.click(acceptButton);

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'ACCEPT_SOLUTION',
            payload: {
                authorInfo: {
                    id: '1'
                }
            }
        });
    });

    it('dispatches REJECT_SOLUTION action on reject button clicked', () => {
        const userReducer = {
            id: '1'
        } as User;
        const reducer = jest.fn().mockReturnValue({
            user: userReducer
        });
        const store = createStore(reducer);
        const ticket = {
            authorInfo: {
                id: '1'
            }
        } as Ticket;

        render(<Provider store={store}><VerifySolution ticket={ticket} /></Provider>);

        const container = document.querySelector('div');
        const rejectButton = container?.children[1] as HTMLButtonElement;

        fireEvent.click(rejectButton);

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'REJECT_SOLUTION',
            payload: {
                authorInfo: {
                    id: '1'
                }
            }
        });
    });
});
