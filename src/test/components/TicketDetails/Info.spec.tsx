import { render } from '@testing-library/react';
import Info from 'components/TicketDetails/Info';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';
import { Category, Ticket } from 'types/store/data';
import { User } from 'types/store/user';

jest.mock('components/TicketDetails/actions', () => ({
    AssignTechnician: function AssignTechnician() {
        return <div className="assign-technician"></div>;
    },
    ResolveTicket: function ResolveTicket() {
        return <div className="resolve-ticket"></div>;
    },
    VerifySolution: function VerifySolution() {
        return <div className="verify-solution"></div>;
    },
    AssignMe: function AssignMe() {
        return <div className="assign-me"></div>;
    },
    StartProgress: function StartProgress() {
        return <div className="start-progress"></div>;
    }
}));

describe('<Info />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders general ticket information', () => {
        const ticket = {
            authorInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            categoryInfo: {
                id: 1,
                name: 'Some category',
                subcategories: [{
                    id: 1,
                    name: 'Some subcategory'
                }]
            },
            createdAt: new Date(2021, 5, 9, 1, 44),
            id: 1,
            lastModifiedDate: new Date(2021, 5, 9, 1, 44),
            priority: 'LOW',
            status: 'UNASSIGNED',
            subcategoryInfo: {
                id: 1,
                name: 'Some subcategory'
            },
            technicianInfo: null,
            title: 'Some ticket title'
        } as Ticket;
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({
                categories: [{
                    id: 1,
                    name: 'Some category',
                    subcategories: [{
                        id: 1,
                        name: 'Some subcategory'
                    }]
                }] as Category[]
            }),
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'NONE' }]
            } as User)
        }));

        render(<Provider store={store}><Info assignees={[]} ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const ticketInfoContainer = document.querySelector('.ticket-info');

        expect(ticketInfoContainer?.children[0].classList.contains('ticket-info-details')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[0].classList.contains('ticket-id')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[0].textContent).toBe('ID: 1');
        expect(ticketInfoContainer?.children[0].children[1].classList.contains('ticket-priority')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[1].textContent).toBe('Priority: Low');
        expect(ticketInfoContainer?.children[0].children[2].classList.contains('ticket-title')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[2].textContent).toBe('Title: Some ticket title');
        expect(ticketInfoContainer?.children[0].children[3].classList.contains('ticket-status')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[3].textContent).toBe('Status: Unassigned');
        expect(ticketInfoContainer?.children[0].children[4].classList.contains('ticket-started')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[4].textContent).toBe('Started: 6/9/2021, 1:44:00 AM');
        expect(ticketInfoContainer?.children[0].children[5].classList.contains('ticket-last-modified')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[5].textContent).toBe('Last modified: 6/9/2021, 1:44:00 AM');
        expect(ticketInfoContainer?.children[0].children[6].classList.contains('ticket-started-by')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[6].textContent).toBe('Started by: Mateusz Dabrowski');
        expect(ticketInfoContainer?.children[0].children[7].classList.contains('ticket-assigned-to')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[7].textContent).toBe('Assigned to: None');
        expect(ticketInfoContainer?.children[0].children[8].classList.contains('ticket-category')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[8].textContent).toBe('Category: Some category');
        expect(ticketInfoContainer?.children[0].children[9].classList.contains('ticket-subcategory')).toBe(true);
        expect(ticketInfoContainer?.children[0].children[9].textContent).toBe('Subcategory: Some subcategory');
    });

    it('shows "assign technician" option for status UNASSIGNED and user role ADMINISTRATOR', () => {
        const ticket = {
            authorInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            categoryInfo: {
                id: 1,
                name: 'Some category',
                subcategories: [{
                    id: 1,
                    name: 'Some subcategory'
                }]
            },
            createdAt: new Date(2021, 5, 9, 1, 44),
            id: 1,
            lastModifiedDate: new Date(2021, 5, 9, 1, 44),
            priority: 'LOW',
            status: 'UNASSIGNED',
            subcategoryInfo: {
                id: 1,
                name: 'Some subcategory'
            },
            technicianInfo: null,
            title: 'Some ticket title'
        } as Ticket;
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({
                categories: [] as Category[]
            }),
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'ADMINISTRATOR' }]
            } as User)
        }));
        const assignees = [{
            id: '1',
            firstName: 'Mateusz',
            lastName: 'Dabrowski',
            roles: [{ role: 'TECHNICIAN' }]
        }] as User[];

        render(<Provider store={store}><Info assignees={assignees} ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const ticketInfoContainer = document.querySelector('.ticket-info');

        expect(ticketInfoContainer?.children[1].classList.contains('ticket-info-actions')).toBe(true);
        expect(ticketInfoContainer?.children[1].children[0].classList.contains('assign-technician')).toBe(true);
    });

    it('shows "start progress" option for status ASSIGNED and user role TECHNICIAN', () => {
        const ticket = {
            authorInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            categoryInfo: {
                id: 1,
                name: 'Some category',
                subcategories: [{
                    id: 1,
                    name: 'Some subcategory'
                }]
            },
            createdAt: new Date(2021, 5, 9, 1, 44),
            id: 1,
            lastModifiedDate: new Date(2021, 5, 9, 1, 44),
            priority: 'LOW',
            status: 'ASSIGNED',
            subcategoryInfo: {
                id: 1,
                name: 'Some subcategory'
            },
            technicianInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            title: 'Some ticket title'
        } as Ticket;
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({
                categories: [] as Category[]
            }),
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'TECHNICIAN' }]
            } as User)
        }));

        render(<Provider store={store}><Info assignees={[]} ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const ticketInfoContainer = document.querySelector('.ticket-info');

        expect(ticketInfoContainer?.children[1].classList.contains('ticket-info-actions')).toBe(true);
        expect(ticketInfoContainer?.children[1].children[0].classList.contains('start-progress')).toBe(true);
    });

    it('shows "resolve ticket" option for status IN_PROGRESS and user role TECHNICIAN', () => {
        const ticket = {
            authorInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            categoryInfo: {
                id: 1,
                name: 'Some category',
                subcategories: [{
                    id: 1,
                    name: 'Some subcategory'
                }]
            },
            createdAt: new Date(2021, 5, 9, 1, 44),
            id: 1,
            lastModifiedDate: new Date(2021, 5, 9, 1, 44),
            priority: 'LOW',
            status: 'IN_PROGRESS',
            subcategoryInfo: {
                id: 1,
                name: 'Some subcategory'
            },
            technicianInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            title: 'Some ticket title'
        } as Ticket;
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({
                categories: [] as Category[]
            }),
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'TECHNICIAN' }]
            } as User)
        }));

        render(<Provider store={store}><Info assignees={[]} ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const ticketInfoContainer = document.querySelector('.ticket-info');

        expect(ticketInfoContainer?.children[1].classList.contains('ticket-info-actions')).toBe(true);
        expect(ticketInfoContainer?.children[1].children[0].classList.contains('resolve-ticket')).toBe(true);
    });

    it('shows "verify solution" option for status DONE and user role EMPLOYEE', () => {
        const ticket = {
            authorInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            categoryInfo: {
                id: 1,
                name: 'Some category',
                subcategories: [{
                    id: 1,
                    name: 'Some subcategory'
                }]
            },
            createdAt: new Date(2021, 5, 9, 1, 44),
            id: 1,
            lastModifiedDate: new Date(2021, 5, 9, 1, 44),
            priority: 'LOW',
            status: 'DONE',
            subcategoryInfo: {
                id: 1,
                name: 'Some subcategory'
            },
            technicianInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            title: 'Some ticket title'
        } as Ticket;
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({
                categories: [] as Category[]
            }),
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'EMPLOYEE' }]
            } as User)
        }));

        render(<Provider store={store}><Info assignees={[]} ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const ticketInfoContainer = document.querySelector('.ticket-info');

        expect(ticketInfoContainer?.children[1].classList.contains('ticket-info-actions')).toBe(true);
        expect(ticketInfoContainer?.children[1].children[0].classList.contains('verify-solution')).toBe(true);
    });

    it('shows "assign me" option for status UNASSIGNED and user role TECHNICIAN', () => {
        const ticket = {
            authorInfo: {
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski'
            },
            categoryInfo: {
                id: 1,
                name: 'Some category',
                subcategories: [{
                    id: 1,
                    name: 'Some subcategory'
                }]
            },
            createdAt: new Date(2021, 5, 9, 1, 44),
            id: 1,
            lastModifiedDate: new Date(2021, 5, 9, 1, 44),
            priority: 'LOW',
            status: 'UNASSIGNED',
            subcategoryInfo: {
                id: 1,
                name: 'Some subcategory'
            },
            technicianInfo: null,
            title: 'Some ticket title'
        } as Ticket;
        const store = createStore(combineReducers({
            data: jest.fn().mockReturnValue({
                categories: [] as Category[]
            }),
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'TECHNICIAN' }]
            } as User)
        }));

        render(<Provider store={store}><Info assignees={[]} ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const ticketInfoContainer = document.querySelector('.ticket-info');

        expect(ticketInfoContainer?.children[1].classList.contains('ticket-info-actions')).toBe(true);
        expect(ticketInfoContainer?.children[1].children[0].classList.contains('assign-me')).toBe(true);
    });
});
