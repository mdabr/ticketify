import { render } from '@testing-library/react';
import Description from 'components/TicketDetails/Description';

describe('<Description />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders description properly', () => {
        const description = 'This is ticket description';

        render(<Description description={description} />, { container: container as HTMLDivElement });

        const descriptionContainer = document.querySelector('.description-container');

        expect(descriptionContainer?.textContent).toBe(description);
    });
});
