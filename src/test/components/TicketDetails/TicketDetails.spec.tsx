import { fireEvent, render } from '@testing-library/react';
import { TicketDetails } from 'components';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Ticket } from 'types/store/data';

jest.mock('utils/uiComponents', () => ({
    List: function List({ children }: any) {
        return <ul className="list">{children}</ul>;
    },
    ListItem: function ListItem({ children, ...props }: any) {
        return <li className="list-item" {...props}>{children}</li>;
    }
}));

jest.mock('components/TicketDetails/Info', () => function Info() {
    return <div className="info"></div>;
});

jest.mock('components/TicketDetails/Messages', () => function Messages() {
    return <div className="messages"></div>;
});

jest.mock('components/TicketDetails/Description', () => function Description() {
    return <div className="description"></div>;
});

describe('<TicketDetails />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders ticket details general info by default', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);
        const ticket = {
            description: 'test'
        } as Ticket;

        render(<Provider store={store}><TicketDetails ticket={ticket} assignees={[]}/></Provider>);

        const ticketDetailsContainer = document.querySelector('.ticket-details-container');

        expect(ticketDetailsContainer?.children[0].classList.contains('list')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[0].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[0].textContent).toBe('Info');
        expect(ticketDetailsContainer?.children[0].children[1].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[1].textContent).toBe('Description');
        expect(ticketDetailsContainer?.children[0].children[2].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[2].textContent).toBe('Messages');
        expect(ticketDetailsContainer?.children[1].classList.contains('info')).toBe(true);
        expect(ticketDetailsContainer?.children[2].classList.contains('edit-button')).toBe(true);
        expect(ticketDetailsContainer?.children[3].classList.contains('close-button')).toBe(true);
    });

    it('renders ticket description on "description" tab clicked', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);
        const ticket = {
            description: 'test'
        } as Ticket;

        render(<Provider store={store}><TicketDetails ticket={ticket} assignees={[]}/></Provider>);

        const ticketDetailsContainer = document.querySelector('.ticket-details-container');

        fireEvent.click(ticketDetailsContainer?.children[0].children[1] as HTMLDivElement);

        expect(ticketDetailsContainer?.children[0].classList.contains('list')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[0].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[0].textContent).toBe('Info');
        expect(ticketDetailsContainer?.children[0].children[1].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[1].textContent).toBe('Description');
        expect(ticketDetailsContainer?.children[0].children[2].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[2].textContent).toBe('Messages');
        expect(ticketDetailsContainer?.children[1].classList.contains('description')).toBe(true);
        expect(ticketDetailsContainer?.children[2].classList.contains('edit-button')).toBe(true);
        expect(ticketDetailsContainer?.children[3].classList.contains('close-button')).toBe(true);
    });

    it('renders ticket messages on "messages" tab clicked', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);
        const ticket = {
            description: 'test'
        } as Ticket;

        render(<Provider store={store}><TicketDetails ticket={ticket} assignees={[]}/></Provider>);

        const ticketDetailsContainer = document.querySelector('.ticket-details-container');

        fireEvent.click(ticketDetailsContainer?.children[0].children[2] as HTMLDivElement);

        expect(ticketDetailsContainer?.children[0].classList.contains('list')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[0].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[0].textContent).toBe('Info');
        expect(ticketDetailsContainer?.children[0].children[1].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[1].textContent).toBe('Description');
        expect(ticketDetailsContainer?.children[0].children[2].classList.contains('list-item')).toBe(true);
        expect(ticketDetailsContainer?.children[0].children[2].textContent).toBe('Messages');
        expect(ticketDetailsContainer?.children[1].classList.contains('messages')).toBe(true);
        expect(ticketDetailsContainer?.children[2].classList.contains('edit-button')).toBe(true);
        expect(ticketDetailsContainer?.children[3].classList.contains('close-button')).toBe(true);
    });

    it('dispatches HIDE_TICKET_DETAILS on close button pressed', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);
        const ticket = {
            description: 'test'
        } as Ticket;

        render(<Provider store={store}><TicketDetails ticket={ticket} assignees={[]}/></Provider>);

        const ticketDetailsContainer = document.querySelector('.ticket-details-container');

        fireEvent.click(ticketDetailsContainer?.children[3] as HTMLDivElement);

        expect(reducer).lastCalledWith(undefined, { type: 'HIDE_TICKET_DETAILS' });
    });

    it('dispatches HIDE_TICKET_DETAILS on backdrop clicked', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);
        const ticket = {
            description: 'test'
        } as Ticket;

        render(<Provider store={store}><TicketDetails ticket={ticket} assignees={[]}/></Provider>);

        const ticketDetailsBackdrop = document.querySelector('.ticket-details-backdrop');

        fireEvent.click(ticketDetailsBackdrop as HTMLDivElement);

        expect(reducer).lastCalledWith(undefined, { type: 'HIDE_TICKET_DETAILS' });
    });

    it('dispatches HIDE_TICKET_DETAILS on backdrop clicked', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);
        const ticket = {
            description: 'test'
        } as Ticket;

        render(<Provider store={store}><TicketDetails ticket={ticket} assignees={[]}/></Provider>);

        const ticketDetailsContainer = document.querySelector('.ticket-details-container');

        fireEvent.click(ticketDetailsContainer?.children[2] as HTMLDivElement);

        expect(reducer).lastCalledWith(undefined, { type: 'SET_EDITED_TICKET', payload: ticket });
    });
});
