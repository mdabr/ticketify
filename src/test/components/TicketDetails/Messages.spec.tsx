import { fireEvent, render } from '@testing-library/react';
import Messages from 'components/TicketDetails/Messages';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';
import { Ticket } from 'types/store/data';
import { User } from 'types/store/user';

jest.mock('utils/uiComponents', () => ({
    TextField: function TextField({ ...props }) {
        return <input {...props} />;
    }
}));

describe('<Messages />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders messages correctly', () => {
        const ticket = {
            comments: [{
                id: 1,
                authorInfo: {
                    id: '1',
                    firstName: 'Mateusz',
                    lastName: 'Dabrowski'
                },
                comment: 'Hello'
            }, {
                id: 2,
                authorInfo: {
                    id: '2',
                    firstName: 'John',
                    lastName: 'Doe'
                },
                comment: 'Hi'
            }]
        } as Ticket;
        const store = createStore(combineReducers({
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'NONE' }]
            } as User)
        }));

        render(<Provider store={store}><Messages ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const messagesContainer = document.querySelector('.messages-container');

        expect(messagesContainer?.children[0].classList.contains('messages')).toBe(true);
        expect(messagesContainer?.children[0].children[0].nodeName).toBe('P');
        expect(messagesContainer?.children[0].children[0].getAttribute('style')).toBe('text-align: right;');
        expect(messagesContainer?.children[0].children[0].textContent).toBe('Mateusz Dabrowski: Hello');
        expect(messagesContainer?.children[0].children[1].nodeName).toBe('P');
        expect(messagesContainer?.children[0].children[1].getAttribute('style')).toBe('text-align: left;');
        expect(messagesContainer?.children[0].children[1].textContent).toBe('John Doe: Hi');
        expect(messagesContainer?.children[1].classList.contains('new-message-container')).toBe(true);
        expect(messagesContainer?.children[1].children[0].nodeName).toBe('INPUT');
        expect(messagesContainer?.children[1].children[0].classList.contains('new-message')).toBe(true);
    });

    it('sends a new message on enter pressed', () => {
        const ticket = {
            comments: [{
                id: 1,
                authorInfo: {
                    id: '1',
                    firstName: 'Mateusz',
                    lastName: 'Dabrowski'
                },
                comment: 'Hello'
            }, {
                id: 2,
                authorInfo: {
                    id: '2',
                    firstName: 'John',
                    lastName: 'Doe'
                },
                comment: 'Hi'
            }],
            id: 1
        } as Ticket;
        const reducer = jest.fn().mockReturnValue({
            user: jest.fn().mockReturnValue({
                id: '1',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                roles: [{ role: 'NONE' }]
            } as User)
        });
        const store = createStore(reducer);

        render(<Provider store={store}><Messages ticket={ticket} /></Provider>, { container: container as HTMLDivElement });

        const messagesContainer = document.querySelector('.messages-container');

        fireEvent.change(messagesContainer?.children[1].children[0] as HTMLInputElement, { target: { value: 'Anybody there?' } });
        fireEvent.keyDown(messagesContainer?.children[1].children[0] as HTMLInputElement, { keyCode: 13 });

        expect(reducer).lastCalledWith(expect.anything(), {
            type: 'SEND_MESSAGE',
            payload: {
                message: 'Anybody there?',
                ticketId: 1
            }
        });
    });
});
