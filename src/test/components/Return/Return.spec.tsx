import { render } from '@testing-library/react';
import { Return } from 'components';
import { MemoryRouter } from 'react-router';

describe('<Return />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders a return button', () => {
        render(<MemoryRouter>
            <Return image={'/path/to/image'} height={10} width={10} to="/some/path" />
        </MemoryRouter>, { container: container as HTMLDivElement });

        expect(document.querySelector('.go-back')).toBeDefined();
    });

    it('renders a return button with default size', () => {
        render(<MemoryRouter>
            <Return image={'/path/to/image'} to="/some/path" />
        </MemoryRouter>, { container: container as HTMLDivElement });

        expect(document.querySelector('.go-back')).toBeDefined();
    });
});
