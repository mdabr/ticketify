import { Infobox } from 'components';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';

jest.mock('components/Infobox/ToastsList/ToastsList', () => function ToastsList(props: Record<string, any>) {
    return <div>{props.toasts.map((toast: Record<string, string>) => `${toast.header}${toast.message}`)}</div>;
});

describe('<Infobox />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        unmountComponentAtNode(container as HTMLDivElement);
        (container as HTMLDivElement).remove();
        container = null;
        jest.useRealTimers();
    });

    it('shows list of toasts from store', () => {
        const toasts = [{
            id: '1', message: 'Something succeeded', header: 'Success!', level: 'SUCCESS', time: 2000
        }];
        const mockReducer = jest.fn().mockReturnValue(toasts);
        const store = createStore(combineReducers({ toasts: mockReducer }));

        act(() => {
            render(<Provider store={store}><Infobox /></Provider>, container);
        });

        const infobox = document.querySelector('.toasts-container');

        expect(infobox?.textContent).toEqual('Success!Something succeeded');
    });
});
