import ToastsList from 'components/Infobox/ToastsList/ToastsList';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

jest.mock('components/Infobox/ToastsList/Toast/Toast', () => function Toast(props: Record<string, any>) {
    return <div>{`${props.header}${props.message}`}</div>;
});

describe('<ToastsList />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        unmountComponentAtNode(container as HTMLDivElement);
        (container as HTMLDivElement).remove();
        container = null;
        jest.useRealTimers();
    });

    it('shows nothing for empty list of toasts', () => {
        act(() => {
            render(<ToastsList toasts={[]} />, container);
        });

        const toastsList = container?.children[0];

        expect(toastsList?.textContent).toEqual(undefined);
    });

    it('shows list of toasts for non-empty list of toasts passed', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);
        const toasts = [{
            id: '1', message: 'Something succeeded', header: 'Success!', level: 'SUCCESS', time: 2000
        }];

        act(() => {
            render(<Provider store={store}><ToastsList toasts={toasts} /></Provider>, container);
        });

        const toastsList = container?.children[0];

        expect(toastsList?.textContent).toEqual('Success!Something succeeded');
    });
});
