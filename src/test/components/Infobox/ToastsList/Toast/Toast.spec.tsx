import { Toast } from 'components';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

describe('<Toast />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        unmountComponentAtNode(container as HTMLDivElement);
        (container as HTMLDivElement).remove();
        container = null;
        jest.useRealTimers();
    });

    it('returns element with class "success" for level SUCCESS', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);

        act(() => {
            render(<Provider store={store}>
                <Toast id="1234abcd" header="Success!" level="SUCCESS" message="Something succeeded" time={2000} />
            </Provider>, container);
        });

        const toast = document.querySelector('.toast');

        expect(toast?.classList.contains('success')).toEqual(true);
    });

    it('returns element with class "danger" for level DANGER', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);

        act(() => {
            render(<Provider store={store}>
                <Toast id="1234abcd" header="Error!" level="DANGER" message="Something failed" time={2000} />
            </Provider>, container);
        });

        const toast = document.querySelector('.toast');

        expect(toast?.classList.contains('danger')).toEqual(true);

    });

    it('returns element with class "warning" for level WARNING', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);

        act(() => {
            render(<Provider store={store}>
                <Toast id="1234abcd" header="Warning!" level="WARNING" message="Something needs attention" time={2000} />
            </Provider>, container);
        });

        const toast = document.querySelector('.toast');

        expect(toast?.classList.contains('warning')).toEqual(true);
    });

    it('returns element with class "info" for level INFO', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);

        act(() => {
            render(<Provider store={store}>
                <Toast id="1234abcd" header="Info!" level="INFO" message="Something to note" time={2000} />
            </Provider>, container);
        });

        const toast = document.querySelector('.toast');

        expect(toast?.classList.contains('info')).toEqual(true);
    });

    it('returns element with class "none" for level NONE', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);

        act(() => {
            render(<Provider store={store}>
                <Toast id="1234abcd" header="Nothing!" level="NONE" message="Nothing happend" time={2000} />
            </Provider>, container);
        });

        const toast = document.querySelector('.toast');

        expect(toast?.classList.contains('none')).toEqual(true);
    });

    it('removes itself after time passes', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);
        const toastProps = {
            id: '1234abcd',
            time: 2000
        };

        act(() => {
            render(<Provider store={store}>
                <Toast id={toastProps.id} header="Nothing!" level="NONE" message="Nothing happend" time={toastProps.time} />
            </Provider>, container);
        });

        act(() => {
            jest.advanceTimersByTime(toastProps.time);
        });

        expect(mockReducer).toBeCalledWith(undefined, { type: 'REMOVE_TOAST', payload: { id: toastProps.id } });
    });
});
