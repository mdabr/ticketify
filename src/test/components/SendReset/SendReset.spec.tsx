import { fireEvent, render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

const { SendReset } = jest.requireActual('components');

jest.mock('components', () => ({
    Return: function Return() {
        return <div className="return"></div>;
    }
}));

describe('<SendReset />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders form for resetting password', () => {
        const store = createStore(jest.fn());

        render(<Provider store={store}><SendReset /></Provider>, { container: container as HTMLDivElement });

        const sendResetContainer = document.querySelector('.send-reset');
        const form = document.querySelector('form');

        expect(sendResetContainer?.children[0].nodeName).toBe('H1');
        expect(sendResetContainer?.children[0].classList.contains('login-title')).toBe(true);
        expect(sendResetContainer?.children[0].textContent).toBe('Reset password');
        expect(sendResetContainer?.children[1].classList.contains('return')).toBe(true);
        expect(form).toBeDefined();
        expect(form?.children[0].nodeName).toBe('DIV');
        expect(form?.children[0].children[0].nodeName).toBe('P');
        expect(form?.children[0].children[0].textContent).toBe('Provide login for account you want to reset the password for :');
        expect(form?.children[0].children[1].nodeName).toBe('LABEL');
        expect(form?.children[0].children[1].children[0].nodeName).toBe('H3');
        expect(form?.children[0].children[1].children[0].textContent).toBe('Login');
        expect(form?.children[0].children[1].children[1].nodeName).toBe('INPUT');
        expect(form?.children[1].nodeName).toBe('INPUT');
        expect((form?.children[1] as HTMLInputElement).value).toBe('Reset password');
    });

    it('sends RESET_PASSWORD action on submit', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);

        render(<Provider store={store}><SendReset /></Provider>, { container: container as HTMLDivElement });

        const sendResetContainer = document.querySelector('.send-reset');

        fireEvent.change(sendResetContainer?.children[2].children[0].children[1].children[1] as HTMLInputElement, { target: { value: 'test@gmail.com' } });
        fireEvent.click(sendResetContainer?.children[2].children[1] as HTMLInputElement);

        expect(reducer).lastCalledWith(undefined, {
            type: 'RESET_PASSWORD',
            payload: {
                email: 'test@gmail.com'
            }
        });
    });
});
