import { fireEvent, render } from '@testing-library/react';
import { Register } from 'components';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { registerCompany } from 'store/actions';


jest.mock('store/actions', () => ({
    registerCompany: jest.fn()
}));

describe('<Register />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        container?.remove();
        container = null;
        jest.useRealTimers();
    });

    it('renders register form', () => {
        const reducer = jest.fn();
        const store = createStore(reducer);

        render(<Provider store={store}>
            <Register />
        </Provider>, { container: container as HTMLDivElement });

        const registerContainer = document.querySelector('.register-container');

        expect(registerContainer?.children.length).toBe(2);
        expect(registerContainer?.children[0].textContent).toBe('Register');
        expect(registerContainer?.children[1].children[0].textContent).toBe('Company');
        expect(registerContainer?.children[1].children[1].textContent).toBe('Contact email');
        expect((registerContainer?.children[1].children[2] as HTMLInputElement).value).toBe('Register');
    });

    it('dispatches register company action on submit', () => {
        (registerCompany as jest.Mock).mockImplementation((companyName, contactEmail) => ({
            type: 'REGISTER_COMPANY',
            companyName,
            contactEmail
        }));

        const reducer = jest.fn();
        const store = createStore(reducer);

        render(<Provider store={store}>
            <Register />
        </Provider>, { container: container as HTMLDivElement });

        const registerContainer = document.querySelector('.register-container');

        fireEvent.change(registerContainer?.children[1].children[0].children[1] as Element, { target: { value: 'RicCorp' } });
        fireEvent.change(registerContainer?.children[1].children[1].children[1] as Element, { target: { value: 'ric@corp.com' } });
        fireEvent.click(registerContainer?.children[1].children[2] as Element);

        expect(registerCompany).toBeCalledWith('RicCorp', 'ric@corp.com');
        expect(reducer).toBeCalledWith(undefined, {
            type: 'REGISTER_COMPANY',
            companyName: 'RicCorp',
            contactEmail: 'ric@corp.com'
        });
    });
});
