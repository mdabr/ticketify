import React from 'react';
import { AddNewTicket } from 'components';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

describe('<AddNewTicket />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
    });

    afterEach(() => {
        unmountComponentAtNode(container as HTMLDivElement);
        (container as HTMLDivElement).remove();
        container = null;
    });

    it('opens new ticket popup on click', () => {
        const mockReducer = jest.fn();
        const store = createStore(mockReducer);
        act(() => {
            render(<Provider store={store}><AddNewTicket /></Provider>, container);
        });

        const newTicketIcon = document.querySelector('.add-new-ticket');

        act(() => {
            newTicketIcon?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
        });

        expect(mockReducer).toBeCalledWith(undefined, { type: 'SHOW_NEW_TICKET' });
    });
});
