import { Loader } from 'components';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';

describe('<Loader />', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        unmountComponentAtNode(container as HTMLDivElement);
        (container as HTMLDivElement).remove();
        container = null;
        jest.useRealTimers();
    });

    it('shows nothing if loaderVisible is set to false in store', () => {
        const store = createStore(combineReducers({ loader: jest.fn().mockReturnValue({ loaderVisible: false }) }));

        act(() => {
            render(<Provider store={store}><Loader /></Provider>, container);
        });

        const loaderBackdrop = document.querySelector('.loader-backdrop');
        const loaderContainer = document.querySelector('.loader-container');

        expect(loaderBackdrop).toBe(null);
        expect(loaderContainer).toBe(null);
    });

    it('show loader if loaderVisible is set to true in store', () => {
        const store = createStore(combineReducers({ loader: jest.fn().mockReturnValue({ loaderVisible: true }) }));

        act(() => {
            render(<Provider store={store}><Loader /></Provider>, container);
        });

        const loaderBackdrop = document.querySelector('.loader-backdrop');
        const loaderContainer = document.querySelector('.loader-container');

        expect(loaderBackdrop).not.toBe(null);
        expect(loaderContainer).not.toBe(null);
    });
});
