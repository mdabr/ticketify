import { User } from 'types/store/user';
import { USER_TYPE } from 'utils/constants';
import {
    capitalizeFirst,
    getFullname,
    getRole,
    isArrayEmpty,
    isAuthorized,
    parseGet,
    parseServerDate,
    replacePlaceholders,
    underscoreToSpace,
    path,
    zeroHour,
    fullHour,
    isIdentifiableUnique
} from 'utils/helpers';

describe('helpers.ts', () => {
    describe('capitalizeFirst', () => {
        it('capitalizes first letter in a string', () => {
            const str = 'hello';

            const capitalizedStr = capitalizeFirst(str);

            expect(capitalizedStr).toEqual('Hello');
        });

        it('decapitalizes every letter in a string after first one', () => {
            const str = 'HELLO';

            const capitalizedStr = capitalizeFirst(str);

            expect(capitalizedStr).toEqual('Hello');
        });

        it('leaves non-alphabetical symbols in a string unchanged', () => {
            const str = '12345';

            const capitalizedStr = capitalizeFirst(str);

            expect(capitalizedStr).toEqual('12345');
        });

        it('leaves an empty string unchanged', () => {
            const str = '';

            const capitalizedStr = capitalizeFirst(str);

            expect(capitalizedStr).toEqual('');
        });
    });

    describe('underscoreToSpace', () => {
        it('changes every underscore to empty space', () => {
            const str = 'Hello_world';

            const changedStr = underscoreToSpace(str);

            expect(changedStr).toEqual('Hello world');
        });

        it('leaves a string without underscore unchanged', () => {
            const str = 'Helloworld';

            const changedStr = underscoreToSpace(str);

            expect(changedStr).toEqual('Helloworld');
        });

        it('leaves an empty string unchanged', () => {
            const str = '';

            const changedStr = underscoreToSpace(str);

            expect(changedStr).toEqual('');
        });

        it('leaves a numerical string unchanged', () => {
            const str = '12345';

            const changedStr = underscoreToSpace(str);

            expect(changedStr).toEqual('12345');
        });
    });

    describe('parseGet', () => {
        it('parses a GET endpoint', () => {
            const endpoint = '/user/{category}';
            const placeholders: Record<string, string | number> = {
                category: 'admin'
            };
            const params: Record<string, string> = {
                createdAfter: '2020-01-19',
                sort: 'ASC'
            };

            const parsedGet = parseGet(endpoint, placeholders, params);

            expect(parsedGet).toEqual('/user/admin?createdAfter=2020-01-19&sort=ASC');
        });

        it('returns initial endpoint for no placeholders and params passed', () => {
            const endpoint = '/user/{category}';

            const parsedGet = parseGet(endpoint);

            expect(parsedGet).toEqual('/user/{category}');
        });
    });

    describe('getRole', () => {
        it('retrieves user role', () => {
            const roles = [{
                role: 'UNKNOWN' as USER_TYPE
            }, {
                role: 'ADMINISTRATOR' as USER_TYPE
            }];

            const retrievedRole = getRole(roles);

            expect(retrievedRole).toEqual('ADMINISTRATOR');
        });

        it('retrieves user unknown role if no known was found', () => {
            const roles = [{
                role: 'UNKNOWN' as USER_TYPE
            }];

            const retrievedRole = getRole(roles);

            expect(retrievedRole).toEqual('UNKNOWN');
        });
    });

    describe('replacePlaceholders', () => {
        it('replaces placeholders in an endpoint', () => {
            const endpoint = '/user/{id}';
            const placeholders = {
                id: 1337
            };

            const formattedEndpoint = replacePlaceholders(endpoint, placeholders);

            expect(formattedEndpoint).toEqual('/user/1337');
        });

        it('returns initial endpoint if no placeholders were passed', () => {
            const endpoint = '/user/{id}';

            const formattedEndpoint = replacePlaceholders(endpoint);

            expect(formattedEndpoint).toEqual('/user/{id}');
        });
    });

    describe('isArrayEmpty', () => {
        it('returns true if array is empty', () => {
            const array: any[] = [];

            const isEmpty = isArrayEmpty(array);

            expect(isEmpty).toBe(true);
        });

        it('returns false if array is not empty', () => {
            const array = [1];

            const isEmpty = isArrayEmpty(array);

            expect(isEmpty).toBe(false);
        });
    });

    describe('isAuthorized', () => {
        it('returns true if user is authorized', () => {
            const roles = [{
                role: 'ADMINISTRATOR' as USER_TYPE
            }];
            const authorizedRoles = ['ADMINISTRATOR'];

            const authorized = isAuthorized(authorizedRoles, roles);

            expect(authorized).toEqual(true);
        });

        it('returns false if user is not authorized', () => {
            const roles = [{
                role: 'USER' as USER_TYPE
            }];
            const authorizedRoles = ['ADMINISTRATOR'];

            const authorized = isAuthorized(authorizedRoles, roles);

            expect(authorized).toEqual(false);
        });
    });

    describe('getFullname', () => {
        it('returns full name of a user', () => {
            const user = {
                firstName: 'Mateusz',
                lastName: 'Dąbrowski'
            } as User;

            const fullname = getFullname(user);

            expect(fullname).toEqual(`${user.firstName} ${user.lastName}`);
        });

        it('returns "None" for non-existent user', () => {
            const user = null;

            const fullname = getFullname(user);

            expect(fullname).toEqual('None');
        });
    });

    describe('parseServerDate', () => {
        it('parses server date to readable date', () => {
            const serverDate = new Date(2021, 4, 30, 0, 0, 0, 0).toISOString();

            const parsedDate = parseServerDate(serverDate);

            expect(parsedDate).toEqual('5/30/2021, 12:00:00 AM');
        });
    });

    describe('path', () => {
        it('returns nested property of an object', () => {
            const paths = ['path', 'to', 'property'];
            const obj = {
                path: {
                    to: {
                        property: 'hello'
                    }
                }
            };

            const property = path(paths, obj);

            expect(property).toEqual('hello');
        });

        it('returns undefined if nested property does not exist', () => {
            const paths = ['path', 'to', 'property'];
            const obj = {};

            const property = path(paths, obj);

            expect(property).toEqual(undefined);
        });
    });

    describe('zeroHour', () => {
        it('returns the same date with zeroed hour', () => {
            const date = new Date(2021, 4, 30, 13, 37, 48);

            const zeroHoured = zeroHour(date);

            expect(zeroHoured).toEqual(new Date(2021, 4, 30, 0, 0, 0));
        });
    });

    describe('fullHour', () => {
        it('returns the same date with latest hour', () => {
            const date = new Date(2021, 4, 30, 13, 37, 48);

            const fullHoured = fullHour(date);

            expect(fullHoured).toEqual(new Date(2021, 4, 30, 23, 59, 59));
        });
    });

    describe('isIdentifiableUnique', () => {
        it('returns true if identifiable is unique within an array', () => {
            const array = [{
                id: 2
            }, {
                id: 1
            }];
            const identifiable = {
                id: 1
            };
            const index = 1;

            const unique = isIdentifiableUnique(identifiable, index, array);

            expect(unique).toBe(true);
        });

        it('returns false if identifiable is not unique within an array', () => {
            const array = [{
                id: 1
            }, {
                id: 1
            }];
            const identifiable = {
                id: 1
            };
            const index = 1;

            const unique = isIdentifiableUnique(identifiable, index, array);

            expect(unique).toBe(false);
        });
    });
});
