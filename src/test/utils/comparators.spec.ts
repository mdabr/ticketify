import { tickets } from 'utils/comparators';
import { Ticket } from 'types/store/data';

describe('comparators.ts', () => {
    describe('tickets', () => {
        describe('startedAsc', () => {
            it('returns less than 0 if first ticket was created earlier than the second one', () => {
                const t1 = {
                    createdAt: new Date(2021, 4, 30)
                } as Ticket;
                const t2 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.startedAsc(t1, t2);

                expect(compared).toBeLessThan(0);
            });

            it('returns more than 0 if second ticket was created earlier than the first one', () => {
                const t1 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    createdAt: new Date(2021, 4, 30)
                } as Ticket;

                const compared = tickets.startedAsc(t1, t2);

                expect(compared).toBeGreaterThan(0);
            });

            it('returns 0 if first ticket and the second one were created at the same time', () => {
                const t1 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.startedAsc(t1, t2);

                expect(compared).toBe(0);
            });
        });

        describe('startedDesc', () => {
            it('returns less than 0 if second ticket was created earlier than the first one', () => {
                const t1 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    createdAt: new Date(2021, 4, 30)
                } as Ticket;

                const compared = tickets.startedDesc(t1, t2);

                expect(compared).toBeLessThan(0);
            });

            it('returns more than 0 if first ticket was created earlier than the second one', () => {
                const t1 = {
                    createdAt: new Date(2021, 4, 30)
                } as Ticket;
                const t2 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.startedDesc(t1, t2);

                expect(compared).toBeGreaterThan(0);
            });

            it('returns 0 if first ticket and the second one were created at the same time', () => {
                const t1 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    createdAt: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.startedDesc(t1, t2);

                expect(compared).toBe(0);
            });
        });

        describe('modifiedAsc', () => {
            it('returns less than 0 if first ticket was modified earlier than the second one', () => {
                const t1 = {
                    lastModifiedDate: new Date(2021, 4, 30)
                } as Ticket;
                const t2 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.modifiedAsc(t1, t2);

                expect(compared).toBeLessThan(0);
            });

            it('returns more than 0 if second ticket was modified earlier than the first one', () => {
                const t1 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    lastModifiedDate: new Date(2021, 4, 30)
                } as Ticket;

                const compared = tickets.modifiedAsc(t1, t2);

                expect(compared).toBeGreaterThan(0);
            });

            it('returns 0 if first ticket and the second one were modified at the same time', () => {
                const t1 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.modifiedAsc(t1, t2);

                expect(compared).toBe(0);
            });
        });

        describe('modifiedDesc', () => {
            it('returns less than 0 if second ticket was modifed earlier than the first one', () => {
                const t1 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    lastModifiedDate: new Date(2021, 4, 30)
                } as Ticket;

                const compared = tickets.modifiedDesc(t1, t2);

                expect(compared).toBeLessThan(0);
            });

            it('returns more than 0 if first ticket was modified earlier than the second one', () => {
                const t1 = {
                    lastModifiedDate: new Date(2021, 4, 30)
                } as Ticket;
                const t2 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.modifiedDesc(t1, t2);

                expect(compared).toBeGreaterThan(0);
            });

            it('returns 0 if first ticket and the second one were modified at the same time', () => {
                const t1 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;
                const t2 = {
                    lastModifiedDate: new Date(2021, 5, 16)
                } as Ticket;

                const compared = tickets.modifiedDesc(t1, t2);

                expect(compared).toBe(0);
            });
        });
    });
});
