import { Category, Subcategory } from 'types/store/data';
import { getName, findCategory } from 'utils/ticket';

describe('ticket.ts', () => {
    describe('getName', () => {
        it('returns name of an object', () => {
            const object = {
                name: 'Hello'
            };

            const name = getName(object);

            expect(name).toEqual('Hello');
        });

        it('return --- if object does not have a name', () => {
            const object = {
                abc: 'Hello'
            };

            const name = getName(object);

            expect(name).toEqual('---');
        });
    });

    describe('findCategory', () => {
        it('finds category in categories list based on category with subcategories', () => {
            const category = {
                id: 1,
                subcategories: [{
                    id: 1
                }]
            } as Category;
            const subcategory = null;
            const categories = [{
                id: 1
            } as Category];

            const foundCategory = findCategory(category, subcategory, categories);

            expect(foundCategory).toEqual(category);
        });

        it('finds category in categories list based on category without subcategories', () => {
            const category = {
                id: 1
            } as Category;
            const subcategory = null;
            const categories = [{
                id: 1
            } as Category];

            const foundCategory = findCategory(category, subcategory, categories);

            expect(foundCategory).toEqual(category);
        });

        it('finds category in categories list based on subcategory', () => {
            const category = null;
            const subcategory = {
                name: '1'
            } as Subcategory;
            const categories = [{
                id: 1,
                subcategories: [{
                    name: '1'
                }]
            } as Category];

            const foundCategory = findCategory(category, subcategory, categories);

            expect(foundCategory).toEqual(categories[0]);
        });

        it('returns null if category was not found based on category', () => {
            const category = {
                id: 2
            } as Category;
            const subcategory = null;
            const categories = [{
                id: 1
            } as Category];

            const foundCategory = findCategory(category, subcategory, categories);

            expect(foundCategory).toEqual(null);
        });

        it('return null if category was not found based on subcategory', () => {
            const category = null;
            const subcategory = {
                name: '1'
            } as Subcategory;
            const categories = [{
                id: 1,
                subcategories: [{
                    name: '2'
                }]
            } as Category];

            const foundCategory = findCategory(category, subcategory, categories);

            expect(foundCategory).toEqual(null);
        });

        it('returns null if category was not found', () => {
            const category = null;
            const subcategory = null;
            const categories = [{
                id: 1,
                subcategories: [{
                    name: '1'
                }]
            } as Category];

            const foundCategory = findCategory(category, subcategory, categories);

            expect(foundCategory).toEqual(null);
        });
    });
});
