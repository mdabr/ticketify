import { UserState } from 'types/store/user';
import SessionService from 'utils/SessionService';

describe('SessionService.ts', () => {
    beforeAll(() => {
        window.sessionStorage.clear();
    });

    afterEach(() => {
        window.sessionStorage.clear();
    });

    describe('saveUser', () => {

        it('saves user to session storage', () => {
            const user: UserState = {
                email: 'm@d.com',
                firstName: 'm',
                lastName: 'd',
                companyInfo: null,
                enabled: true,
                id: '1',
                roles: []
            };

            SessionService.saveUser(user);

            const loadedUser = SessionService.loadUser();

            expect(loadedUser).toEqual(user);
        });
    });

    describe('loadUser', () => {

        it('loads user from session storage when it exists', () => {
            const user: UserState = {
                email: 'm@d.com',
                firstName: 'm',
                lastName: 'd',
                companyInfo: null,
                enabled: true,
                id: '1',
                roles: []
            };

            SessionService.saveUser(user);

            const loadedUser = SessionService.loadUser();

            expect(loadedUser).toEqual(user);
        });

        it('does not load user from session storage when if does not exist', () => {
            const loadedUser = SessionService.loadUser();

            expect(loadedUser).toEqual(null);
        });
    });

    describe('resetUser', () => {
        it('removes user from session storage', () => {
            const user: UserState = {
                email: 'm@d.com',
                firstName: 'm',
                lastName: 'd',
                companyInfo: null,
                enabled: true,
                id: '1',
                roles: []
            };

            SessionService.saveUser(user);

            SessionService.resetUser();

            const loadedUser = SessionService.loadUser();

            expect(loadedUser).toEqual(null);
        });
    });
});
