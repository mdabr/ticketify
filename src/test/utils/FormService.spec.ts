import { Category, Subcategory } from 'types/store/data';
import { Company, User } from 'types/store/user';
import { AddCategoryBody, AddCompanyBody, AddNewTicketBody, AddSubcategoryBody, AddUserBody, EditCategoryBody, EditCompanyBody, EditSubcategoryBody, EditTicketBody, ResetPasswordBody, SubmitLoginBody, SubmitRegisterBody } from 'types/utils/FormService';
import { USER_TYPES } from 'utils/constants';
import { addUser, fetchMyself, fetchUsers, fetchUserWithRole, resetPassword, submitLogin, submitRegister, fetchCategories, addCategory, editCategory, deleteCategory, addSubcategory, editSubcategory, fetchCompanies, addCompany, editCompany, fetchTickets, addNewTicket, editTicket, fetchTicketDetails, sendComment } from 'utils/FormService';

describe('FormService', () => {
    beforeAll(() => jest.spyOn(window, 'fetch'));

    describe('submitLogin', () => {
        it('calls POST /login with proper body', async () => {
            const body: SubmitLoginBody = {
                email: 'test@gmail.com',
                password: 'test123'
            };

            await submitLogin(body);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/login',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST'
                })
            );
        });
    });

    describe('submitRegister', () => {
        it('calls POST /tickets/newCompany with proper body', async () => {
            const body: SubmitRegisterBody = {
                companyName: 'RicCorp',
                contactEmail: 'ric@corp.com'
            };

            await submitRegister(body);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/tickets/newCompany',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST'
                })
            );
        });
    });

    describe('resetPassword', () => {
        it('calls POST /tickets/resetPassword with proper body', async () => {
            const body: ResetPasswordBody = {
                email: 'ric@corp.com'
            };

            await resetPassword(body);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/tickets/resetPassword',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST'
                })
            );
        });
    });

    describe('fetchUsers', () => {
        it('calls GET /users with token', async () => {
            const token = '12345';

            await fetchUsers(token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/users',
                expect.objectContaining({
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    }),
                    method: 'GET'
                })
            );
        });
    });

    describe('addUser', () => {
        it('calls POST /users with token and proper body', async () => {
            const token = '12345';
            const body: AddUserBody = {
                company: '1',
                email: 'ric@corp.com',
                firstName: 'Mateusz',
                lastName: 'Dabrowski',
                password: 'test123'
            };

            await addUser(body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/users',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('fetchUserWithRole', () => {
        it('calls GET /users/role/{role} with token', async () => {
            const token = '12345';

            await fetchUserWithRole(USER_TYPES.ADMINISTRATOR, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/users/role/ADMINISTRATOR',
                expect.objectContaining({
                    method: 'GET',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('fetchMyself', () => {
        it('calls GET /users/myself with token', async () => {
            const token = '12345';

            await fetchMyself(token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/users/myself',
                expect.objectContaining({
                    method: 'GET',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('fetchCategories', () => {
        it('calls GET /categories with token', async () => {
            const token = '12345';

            await fetchCategories(token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/categories',
                expect.objectContaining({
                    method: 'GET',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('addCategory', () => {
        it('calls POST /categories with token and proper body', async () => {
            const token = '12345';
            const body: AddCategoryBody = {
                name: 'cat1',
                enabled: true,
                company: {
                    id: 1
                }
            };

            await addCategory(body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/categories',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('editCategory', () => {
        it('calls PUT /categories with token and proper body', async () => {
            const token = '12345';
            const body: EditCategoryBody = {
                id: '1',
                name: 'cat2',
                enabled: true
            };

            await editCategory(body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/categories',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'PUT',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('deleteCategory', () => {
        it('calls DELETE /category with token', async () => {
            const token = '12345';
            const id = '1';

            await deleteCategory(id, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/categories/1',
                expect.objectContaining({
                    method: 'DELETE',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('addSubcategory', () => {
        it('calls POST /categories/{id}/subcategories with token and proper body', async () => {
            const token = '12345';
            const categoryId = '1';
            const body: AddSubcategoryBody = {
                name: 'sub1',
                enabled: true
            };

            await addSubcategory(categoryId, body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/categories/1/subcategories',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('editSubcategory', () => {
        it('calls PUT /categories/{id}/subcategories with token and proper body', async () => {
            const token = '12345';
            const categoryId = '1';
            const body: EditSubcategoryBody = {
                id: 1,
                name: 'sub2',
                enabled: true
            };

            await editSubcategory(categoryId, body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/categories/1/subcategories',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'PUT',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('fetchCompanies', () => {
        it('calls GET /companies with token', async () => {
            const token = '12345';

            await fetchCompanies(token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/companies',
                expect.objectContaining({
                    method: 'GET',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('addCompany', () => {
        it('calls POST /companies with token and proper body', async () => {
            const token = '12345';
            const body: AddCompanyBody = {
                city: 'Warsaw',
                country: 'Poland',
                localIdCode: '123456',
                name: 'RicCorp',
                postalCode: '00-000',
                premisesAddress: 'Some polish street',
                enabled: true
            };

            await addCompany(body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/companies',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('editCompany', () => {
        it('calls PUT /companies with token and proper body', async () => {
            const token = '12345';
            const body: EditCompanyBody = {
                id: 1,
                city: 'Warsaw',
                country: 'Poland',
                localIdCode: '123456',
                name: 'RicCorp',
                postalCode: '00-000',
                premisesAddress: 'Some polish street',
                enabled: true
            };

            await editCompany(body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/companies',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'PUT',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('fetchTickets', () => {
        it('calls GET /tickets with token', async () => {
            const token = '12345';

            await fetchTickets(token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/tickets',
                expect.objectContaining({
                    method: 'GET',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('addNewTicket', () => {
        it('calls POST /tickets with token and proper body', async () => {
            const token = '12345';
            const body: AddNewTicketBody = {
                authorInfo: {},
                categoryInfo: {},
                companyInfo: {},
                createdAt: new Date(),
                description: '',
                priority: '',
                subcategoryInfo: {},
                title: ''
            } as AddNewTicketBody;

            await addNewTicket(body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/tickets',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'POST',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('editTicket', () => {
        it('calls PUT /tickets with token and proper body', async () => {
            const token = '12345';
            const body: EditTicketBody = {
                id: 1,
                comments: [],
                lastModifiedDate: new Date(),
                status: '',
                technicianInfo: {} as User,
                authorInfo: {} as User,
                categoryInfo: {} as Category,
                companyInfo: {} as Company,
                createdAt: new Date(),
                description: '',
                priority: '',
                subcategoryInfo: {} as Subcategory,
                title: ''
            } as EditTicketBody;

            await editTicket(body, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/tickets',
                expect.objectContaining({
                    body: JSON.stringify(body),
                    method: 'PUT',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('fetchTicketDetails', () => {
        it('calls GET /tickets/{id} with token', async () => {
            const token = '12345';
            const id = 1;

            await fetchTicketDetails(id, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/tickets/1',
                expect.objectContaining({
                    method: 'GET',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });

    describe('sendComment', () => {
        it('calls POST /tickets/{id}/comments with token and proper body', async () => {
            const token = '12345';
            const id = '1';
            const comment = 'Hello';

            await sendComment(id, comment, token);

            expect(window.fetch).lastCalledWith(
                'http://localhost:8080/tickets/1/comments',
                expect.objectContaining({
                    body: JSON.stringify({ comment }),
                    method: 'POST',
                    headers: expect.objectContaining({
                        Authorization: `Bearer ${token}`
                    })
                })
            );
        });
    });
});
