import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode, render } from 'react-dom';
import {
    DescTextField,
    List,
    ListItem,
    TextField,
    InputLabel,
    ListItemText,
    Button,
    Input,
    Select,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Checkbox,
    Icon,
    FilterIcon,
    KeyboardDatePicker,
    Table,
    TableHead,
    TableRow,
    TableBodyRow,
    TableHeadCell,
    TableCell,
    TableBody,
    TableHeadCellCategory,
    TableCellCategory,
    TableRowCategory
} from 'utils/uiComponents';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

describe('uiComponents.ts', () => {
    let container: HTMLDivElement | null = null;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        jest.useFakeTimers();
    });

    afterEach(() => {
        unmountComponentAtNode(container as HTMLDivElement);
        (container as HTMLDivElement).remove();
        container = null;
        jest.useRealTimers();
    });

    describe('<List>', () => {
        it('renders a styled list', () => {
            act(() => {
                render(<List></List>, container);
            });

            const list = container?.childNodes[0];

            expect(list?.nodeName).toBe('UL');
        });
    });

    describe('<ListItem>', () => {
        it('renders a styled list item', () => {
            act(() => {
                render(<ListItem></ListItem>, container);
            });

            const list = container?.childNodes[0];

            expect(list?.nodeName).toBe('LI');
        });
    });

    describe('<DescTextField>', () => {
        it('renders a styled text field', () => {
            act(() => {
                render(<DescTextField></DescTextField>, container);
            });

            const textFieldContainer = document.querySelector('.MuiTextField-root');

            expect(textFieldContainer?.classList.toString().includes('makeStyles-select')).toBe(true);
            expect(textFieldContainer?.children[0].classList.toString().includes('makeStyles-underline')).toBe(true);
        });
    });

    describe('<TextField>', () => {
        it('renders a styled text field', () => {
            act(() => {
                render(<TextField></TextField>, container);
            });

            const textFieldContainer = document.querySelector('.MuiTextField-root');

            expect(textFieldContainer?.classList.toString().includes('makeStyles-select')).toBe(true);
            expect(textFieldContainer?.children[0].classList.toString().includes('makeStyles-underline')).toBe(true);
        });
    });

    describe('<InputLabel>', () => {
        it('renders a styled input label', () => {
            act(() => {
                render(<InputLabel></InputLabel>, container);
            });

            const label = document.querySelector('label');

            expect(label?.classList.toString().includes('makeStyles-selectLabel')).toBe(true);
        });
    });

    describe('<ListItemText>', () => {
        it('renders a styled list item text', () => {
            act(() => {
                render(<ListItemText></ListItemText>, container);
            });

            const listItemText = document.querySelector('.MuiListItemText-root');

            expect(listItemText).toBeDefined();
        });
    });

    describe('<Button>', () => {
        it('renders a styled button', () => {
            act(() => {
                render(<Button></Button>, container);
            });

            const button = document.querySelector('.MuiButton-root');

            expect(button).toBeDefined();
        });
    });

    describe('<Input>', () => {
        it('renders a styled input', () => {
            act(() => {
                render(<Input></Input>, container);
            });

            const input = document.querySelector('.MuiInput-root');

            expect(input?.classList.toString().includes('makeStyles-underline')).toBe(true);
        });
    });

    describe('<Select>', () => {
        it('renders a styled select', () => {
            act(() => {
                render(<Select value=""></Select>, container);
            });

            const input = document.querySelector('.MuiInput-root');
            const select = document.querySelector('.MuiSelect-root');
            const icon = document.querySelector('.MuiSelect-icon');

            expect(input?.classList.toString().includes('makeStyles-underline')).toBe(true);
            expect(select).toBeDefined();
            expect(icon?.classList.toString().includes('makeStyles-selectIcon')).toBe(true);
        });
    });

    describe('<Accordion>', () => {
        it('renders a styled accordion', () => {
            act(() => {
                render(<Accordion><div className="mock"></div></Accordion>, container);
            });

            const accordion = document.querySelector('.MuiAccordion-root');

            expect(accordion?.classList.toString().includes('makeStyles-expansionPanel')).toBe(true);
        });
    });

    describe('<AccordionSummary>', () => {
        it('renders a styled accordion summary', () => {
            act(() => {
                render(<AccordionSummary></AccordionSummary>, container);
            });

            const accordionSummary = document.querySelector('.MuiAccordionSummary-root');
            const icon = document.querySelector('svg');

            expect(accordionSummary?.classList.toString().includes('makeStyles-expansionSummary')).toBe(true);
            expect(accordionSummary?.classList.toString().includes('makeStyles-expansionPanelExpanded')).toBe(true);
            expect(icon?.classList.toString().includes('makeStyles-expandMore')).toBe(true);
        });
    });

    describe('<AccordionDetails>', () => {
        it('renders a styled accordion details', () => {
            act(() => {
                render(<AccordionDetails></AccordionDetails>, container);
            });

            const accordionDetails = document.querySelector('.MuiAccordionDetails-root');

            expect(accordionDetails?.classList.toString().includes('makeStyles-expansionSummary')).toBe(true);
        });
    });

    describe('<Checkbox>', () => {
        it('renders a styled checkbox', () => {
            act(() => {
                render(<Checkbox></Checkbox>, container);
            });

            const checkbox = document.querySelector('.MuiCheckbox-root');

            expect(checkbox).toBeDefined();
        });
    });

    describe('<Icon>', () => {
        it('renders a styled icon', () => {
            act(() => {
                render(<Icon></Icon>, container);
            });

            const icon = document.querySelector('.MuiIcon-root');

            expect(icon).toBeDefined();
        });
    });

    describe('<FilterIcon>', () => {
        it('renders a styled filter icon', () => {
            act(() => {
                render(<FilterIcon></FilterIcon>, container);
            });

            const icon = document.querySelector('.MuiIcon-root');

            expect(icon?.classList.toString().includes('makeStyles-filterIcon')).toBe(true);
        });
    });

    describe('<KeyboardDatePicker>', () => {
        it('renders a styled date picker', () => {
            act(() => {
                render(<MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker value="" onChange={jest.fn()}></KeyboardDatePicker>
                </MuiPickersUtilsProvider>, container);
            });

            const textField = document.querySelector('.MuiTextField-root');
            const input = document.querySelector('.MuiInput-root');

            expect(textField?.classList.toString().includes('makeStyles-select')).toBe(true);
            expect(input?.classList.toString().includes('makeStyles-underline')).toBe(true);
        });
    });

    describe('<Table>', () => {
        it('renders a styled table', () => {
            act(() => {
                render(<Table><tbody></tbody></Table>, container);
            });

            const table = document.querySelector('.MuiTable-root');

            expect(table).toBeDefined();
        });
    });

    describe('<TableHead>', () => {
        it('renders a styled table head', () => {
            act(() => {
                render(<table>
                    <TableHead></TableHead>
                </table>, container);
            });

            const tableHead = document.querySelector('.MuiTableHead-root');

            expect(tableHead?.classList.toString().includes('makeStyles-thead')).toBe(true);
        });
    });

    describe('<TableRow>', () => {
        it('renders a styled table row', () => {
            act(() => {
                render(<table>
                    <tbody>
                        <TableRow></TableRow>
                    </tbody>
                </table>, container);
            });

            const tableRow = document.querySelector('.MuiTableRow-root');

            expect(tableRow).toBeDefined();
        });
    });

    describe('<TableBodyRow>', () => {
        it('renders a styled table body row', () => {
            act(() => {
                render(<table>
                    <tbody>
                        <TableBodyRow></TableBodyRow>
                    </tbody>
                </table>, container);
            });

            const tableRow = document.querySelector('.MuiTableRow-root');

            expect(tableRow?.classList.toString().includes('makeStyles-row')).toBe(true);
        });
    });

    describe('<TableHeadCell>', () => {
        it('renders a styled table head cell', () => {
            act(() => {
                render(<table>
                    <thead>
                        <tr>
                            <TableHeadCell></TableHeadCell>
                        </tr>
                    </thead>
                </table>, container);
            });

            const tableCell = document.querySelector('.MuiTableCell-root');

            expect(tableCell?.classList.toString().includes('makeStyles-thead')).toBe(true);
        });
    });

    describe('<TableCell>', () => {
        it('renders a styled table cell', () => {
            act(() => {
                render(<table>
                    <tbody>
                        <tr>
                            <TableCell></TableCell>
                        </tr>
                    </tbody>
                </table>, container);
            });

            const tableCell = document.querySelector('.MuiTableCell-root');

            expect(tableCell?.classList.toString().includes('makeStyles-tbody')).toBe(true);
        });
    });

    describe('<TableBody>', () => {
        it('renders a styled table body', () => {
            act(() => {
                render(<table>
                    <TableBody></TableBody>
                </table>, container);
            });

            const tableBody = document.querySelector('.MuiTableBody-root');

            expect(tableBody).toBeDefined();
        });
    });

    describe('<TableHeadCellCategory>', () => {
        it('renders a styled category table head cell', () => {
            act(() => {
                render(<table>
                    <thead>
                        <tr>
                            <TableHeadCellCategory></TableHeadCellCategory>
                        </tr>
                    </thead>
                </table>, container);
            });

            const tableCell = document.querySelector('.MuiTableCell-root');

            expect(tableCell?.classList.toString().includes('makeStyles-thead')).toBe(true);
        });
    });

    describe('<TableCellCategory>', () => {
        it('renders a styled category table cell', () => {
            act(() => {
                render(<table>
                    <tbody>
                        <tr>
                            <TableCellCategory></TableCellCategory>
                        </tr>
                    </tbody>
                </table>, container);
            });

            const tableCell = document.querySelector('.MuiTableCell-root');

            expect(tableCell?.classList.toString().includes('makeStyles-tbody')).toBe(true);
        });
    });

    describe('<TableRowCategory>', () => {
        it('renders a styled category table row', () => {
            act(() => {
                render(<table>
                    <tbody>
                        <TableRowCategory></TableRowCategory>
                    </tbody>
                </table>, container);
            });

            const tableRow = document.querySelector('.MuiTableRow-root');

            expect(tableRow).toBeDefined();
        });
    });
});
