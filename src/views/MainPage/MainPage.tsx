import './MainPage.scss';
import { Topbar, Search, TicketList, UserDetails, TicketDetails, AddNewTicket, NewTicket } from 'components';
import { StoreState } from 'store';
import { useSelector } from 'react-redux';
import { Ticket } from 'types/store/data';

export default function MainPage(): JSX.Element {
    const userDetailsVisible = useSelector((state: StoreState) => state.userdetails.userDetailsVisible);
    const { ticketDetailsVisible, ticket, assignees } = useSelector((state: StoreState) => state.ticketdetails);
    const newTicketVisible = useSelector((state: StoreState) => state.newticket.newTicketVisible);

    return <div className="main-page-container">
        <Topbar />
        <Search />
        <TicketList />
        <AddNewTicket />
        {userDetailsVisible ? <UserDetails /> : null}
        {ticketDetailsVisible ? <TicketDetails ticket={ticket as Ticket} assignees={assignees} /> : null}
        {newTicketVisible ? <NewTicket /> : null}
    </div>;
}
