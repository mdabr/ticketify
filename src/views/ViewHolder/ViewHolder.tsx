import { Route, Redirect, Switch } from 'react-router-dom';
import { ViewHolderProps } from 'types/components/ViewHolder';
import { Prelogin, MainPage } from 'views';

export default function ViewHolder({ user }: ViewHolderProps): JSX.Element {
    return <>
        <Switch>
            <Route exact path="/" component={() => <Redirect to="/prelogin" />} />
            <Route path="/prelogin" component={() => user.id ? <Redirect to="/main-page" /> : <Prelogin />} />
            <Route path="/main-page" component={() => user.id ? <MainPage /> : <Redirect to="/prelogin" />} />
        </Switch>
    </>;
}
