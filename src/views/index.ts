import Prelogin from './Prelogin/Prelogin';
import MainPage from './MainPage/MainPage';
import ViewHolder from './ViewHolder/ViewHolder';

export {
    Prelogin,
    MainPage,
    ViewHolder
};
