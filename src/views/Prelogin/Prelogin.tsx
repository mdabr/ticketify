import './Prelogin.scss';
import { Login, Register, SendReset } from 'components';
import styled from 'styled-components';
import { Route } from 'react-router-dom';

const AppTitle = styled.h1`
    font-size: 15vmin;
    text-shadow: 0px 1px 4px #000;
    margin-top: 0;
    margin-bottom: 5vmin;
`;

export default function Prelogin(): JSX.Element {
    return <div className="prelogin">
        <AppTitle>Ticketify</AppTitle>
        <div className="login-register-container">
            <Route exact path="/prelogin" component={() => <>
                <Login></Login>
                <Register></Register>
            </>} />
            <Route exact path="/prelogin/forgot" component={() => <SendReset />} />
        </div>
    </div>;
}
