import {
    AcceptSolutionAction,
    AddNewTicketAction,
    AddToastAction,
    ChangeAssigneeAction,
    ChangeUserAction,
    CloseTicketAction,
    EditTicketAction,
    FetchTicketDetailsAction,
    FilterTicketsAction,
    FinishTicketAction,
    HideLoaderAction,
    HideNewTicketAction,
    HideTicketDetailsAction,
    HideUserDetailsAction,
    LogInAction,
    RefreshTicketsAction,
    RegisterCompanyAction,
    RejectSolutionAction,
    RemoveToastAction,
    ResetPasswordAction,
    ResetUserAction,
    SendMessageAction,
    SetCategoriesAction,
    SetEditedTicketAction,
    SetTicketDetailsAction,
    SetTicketsAction,
    ShowLoaderAction,
    ShowNewTicketAction,
    ShowUserDetailsAction,
    StartProgressAction,
    UpdateTokenAction
} from '../types/store/actions';
import { Token } from 'types/store/token';
import { Ticket, Category, Specifiers, TicketInfo } from 'types/store/data';
import { User } from 'types/store/user';
import { TOAST_LEVELS } from 'utils/constants';

const changeUser = (user: User): ChangeUserAction => ({ type: 'CHANGE_USER', payload: user });
const resetUser = (): ResetUserAction => ({ type: 'RESET_USER' });

const showToast = (level: string) => ({ header, message }: { header: string, message: string }): AddToastAction => ({ type: 'ADD_TOAST', payload: { header, message, level } });
const showSuccess = showToast(TOAST_LEVELS.SUCCESS);
const showDanger = showToast(TOAST_LEVELS.DANGER);
const showWarning = showToast(TOAST_LEVELS.WARNING);
const showInfo = showToast(TOAST_LEVELS.INFO);
const removeToast = (id: string): RemoveToastAction => ({ type: 'REMOVE_TOAST', payload: { id } });

const showLoader = (): ShowLoaderAction => ({ type: 'SHOW_LOADER' });
const hideLoader = (): HideLoaderAction => ({ type: 'HIDE_LOADER' });

const showUserDetails = (): ShowUserDetailsAction => ({ type: 'SHOW_USER_DETAILS' });
const hideUserDetails = (): HideUserDetailsAction => ({ type: 'HIDE_USER_DETAILS' });

const hideTicketDetails = (): HideTicketDetailsAction => ({ type: 'HIDE_TICKET_DETAILS' });

const showNewTicket = (): ShowNewTicketAction => ({ type: 'SHOW_NEW_TICKET' });
const hideNewTicket = (): HideNewTicketAction => ({ type: 'HIDE_NEW_TICKET' });
const setEditedTicket = (ticket: Ticket | null): SetEditedTicketAction => ({ type: 'SET_EDITED_TICKET', payload: ticket });

const setTickets = (tickets: Ticket[]): SetTicketsAction => ({ type: 'SET_TICKETS', payload: tickets });
const setCategories = (categories: Category[]): SetCategoriesAction => ({ type: 'SET_CATEGORIES', payload: categories });
const filterTickets = (specifiers: Specifiers): FilterTicketsAction => ({ type: 'FILTER_TICKETS', payload: specifiers });

const updateToken = ({ expires, token }: Token): UpdateTokenAction => ({
    type: 'UPDATE_TOKEN',
    payload: {
        expires,
        token
    }
});

const logIn = (email: string, password: string): LogInAction => ({
    type: 'LOG_IN',
    payload: {
        email,
        password
    }
});

const editTicket = (ticket: Ticket): EditTicketAction => ({
    type: 'EDIT_TICKET',
    payload: ticket
});

const addNewTicket = (ticket: TicketInfo): AddNewTicketAction => ({
    type: 'ADD_NEW_TICKET',
    payload: ticket
});

const registerCompany = (companyName: string, contactEmail: string): RegisterCompanyAction => ({
    type: 'REGISTER_COMPANY',
    payload: {
        companyName,
        contactEmail
    }
});

const resetPassword = (email: string): ResetPasswordAction => ({
    type: 'RESET_PASSWORD',
    payload: {
        email
    }
});

const refreshTickets = (): RefreshTicketsAction => ({
    type: 'REFRESH_TICKETS'
});

const sendMessage = (message: string, ticketId: number): SendMessageAction => ({
    type: 'SEND_MESSAGE',
    payload: {
        message,
        ticketId
    }
});

const changeAssignee = (ticket: Ticket, assignee: User): ChangeAssigneeAction => ({
    type: 'CHANGE_ASSIGNEE',
    payload: {
        assignee,
        ticket
    }
});

const startProgress = (ticket: Ticket): StartProgressAction => ({
    type: 'START_PROGRESS',
    payload: ticket
});

const finishTicket = (ticket: Ticket): FinishTicketAction => ({
    type: 'FINISH_TICKET',
    payload: ticket
});

const closeTicket = (ticket: Ticket): CloseTicketAction => ({
    type: 'CLOSE_TICKET',
    payload: ticket
});

const acceptSolution = (ticket: Ticket): AcceptSolutionAction => ({
    type: 'ACCEPT_SOLUTION',
    payload: ticket
});

const rejectSolution = (ticket: Ticket): RejectSolutionAction => ({
    type: 'REJECT_SOLUTION',
    payload: ticket
});

const setTicketDetails = (ticket: Ticket | null, assignees: User[]): SetTicketDetailsAction => ({
    type: 'SET_TICKET_DETAILS',
    payload: {
        ticket,
        assignees
    }
});

const showTicketDetails = (ticketId: number, withLoader?: boolean): FetchTicketDetailsAction => ({
    type: 'FETCH_TICKET_DETAILS',
    payload: {
        ticketId,
        withLoader
    }
});

export {
    changeUser,
    resetUser,
    showSuccess,
    showDanger,
    showWarning,
    showInfo,
    removeToast,
    showLoader,
    hideLoader,
    showUserDetails,
    hideUserDetails,
    hideTicketDetails,
    showNewTicket,
    hideNewTicket,
    setTickets,
    setCategories,
    filterTickets,
    updateToken,
    setEditedTicket,
    logIn,
    editTicket,
    addNewTicket,
    registerCompany,
    resetPassword,
    refreshTickets,
    sendMessage,
    changeAssignee,
    startProgress,
    finishTicket,
    closeTicket,
    acceptSolution,
    rejectSolution,
    setTicketDetails,
    showTicketDetails
};
