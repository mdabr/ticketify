import { all, Effect } from 'redux-saga/effects';
import newTicketRootSaga from './newticket';
import preloginRootSaga from './prelogin';
import ticketDetailsRootSaga from './ticketdetails';
import userDetailsRootSaga from './userdetails';

export default function *rootSaga(): Generator<Effect> {
    yield all([
        preloginRootSaga(),
        newTicketRootSaga(),
        ticketDetailsRootSaga(),
        userDetailsRootSaga()
    ]);
}
