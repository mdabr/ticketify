import { StoreState } from 'store';
import { addCategory, addCompany, addSubcategory, addUser, deleteCategory, editCategory, editCompany, editSubcategory, editUser, fetchCategories } from 'utils/FormService';
import { all, call, Effect, takeEvery, put, select } from '@redux-saga/core/effects';
import { setCategories, showDanger, showSuccess } from 'store/actions';
import { Category } from 'types/store/data';
import { AddCategoryAction, AddCompanyAction, AddSubcategoryAction, AddUserAction, DeleteCategorySaga, EditCategoryAction, EditCompanyAction, EditSubcategoryAction, EditUserAction, FetchCategoriesAction } from 'types/store/actions';

function *addCompanySaga({ payload: { name, country, city, postalCode, premisesAddress, localIdCode } }: AddCompanyAction): Generator<Effect> {
    try {
        if (!name || !country || !city || !postalCode || !premisesAddress || !localIdCode) {
            throw new Error('Company information are incomplete!');
        }

        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(addCompany, { name, country, city, postalCode, premisesAddress, localIdCode, enabled: true }, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Couldn\'t add a new company!');
        }

        yield put(showSuccess({ header: 'Success!', message: 'Company successfully added!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *addUserSaga({ payload: { email, firstName, lastName, company, password } }: AddUserAction): Generator<Effect> {
    try {
        if (!email || !firstName || !lastName || !company || !password) {
            throw new Error('User information are incomplete!');
        }

        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(addUser, { email, firstName, lastName, company, password }, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Couldn\'t add a new user!');
        }

        yield put(showSuccess({ header: 'Success!', message: 'User successfully created!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *fetchCategoriesSaga({ payload: { companyId } }: FetchCategoriesAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(fetchCategories, token, undefined, { companyId })) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to fetch categories!');
        }

        const categories = (yield call([response, 'json'])) as Category[];

        yield put(setCategories(categories));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *addSubcategorySaga({ payload: { name, categoryId } }: AddSubcategoryAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(addSubcategory, categoryId, { name, enabled: true }, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to add subcategory!');
        }

        yield put(showSuccess({ header: '', message: 'Subcategory successfully created!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *addCategorySaga({ payload: { name, companyId } }: AddCategoryAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(addCategory, { name, enabled: true, company: { id: companyId } }, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to add category!');
        }

        yield put(showSuccess({ header: '', message: 'Category successfully created!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *deleteCategorySaga({ payload: { categoryId } }: DeleteCategorySaga): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(deleteCategory, categoryId, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to remove category!');
        }

        yield put(showSuccess({ header: '', message: 'Category successfully deleted!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *editSubcategorySaga({ payload: { subcategory, categoryId } }: EditSubcategoryAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(editSubcategory, categoryId, subcategory, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to edit subcategory!');
        }

        yield put(showSuccess({ header: 'Success!', message: 'Subcategory successfully edited!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *editCategorySaga({ payload: { id, name, enabled } }: EditCategoryAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(editCategory, { id, name, enabled }, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to edit category!');
        }
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *editCompanySaga({ payload: company }: EditCompanyAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(editCompany, company, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to update company!');
        }

        yield put(showSuccess({ header: '', message: 'Company successfully updated!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *editUserSaga({ payload: user }: EditUserAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const response = (yield call(editUser, user, token)) as Response;

        if (response.status !== 200) {
            throw new Error('Failed to update user!');
        }

        yield put(showSuccess({ header: '', message: 'User successfully updated!' }));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

export default function *userDetailsRootSaga(): Generator<Effect> {
    yield all([
        takeEvery('ADD_COMPANY', addCompanySaga),
        takeEvery('ADD_USER', addUserSaga),
        takeEvery('FETCH_CATEGORIES', fetchCategoriesSaga),
        takeEvery('ADD_SUBCATEGORY', addSubcategorySaga),
        takeEvery('ADD_CATEGORY', addCategorySaga),
        takeEvery('DELETE_CATEGORY', deleteCategorySaga),
        takeEvery('EDIT_SUBCATEGORY', editSubcategorySaga),
        takeEvery('EDIT_CATEGORY', editCategorySaga),
        takeEvery('EDIT_COMPANY', editCompanySaga),
        takeEvery('EDIT_USER', editUserSaga)
    ]);
}
