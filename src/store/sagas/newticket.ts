import { hideNewTicket, showSuccess, showDanger, setEditedTicket, refreshTickets } from 'store/actions';
import { addNewTicket, editTicket } from 'utils/FormService';
import { Effect } from '@redux-saga/types';
import { all, call, put, select, takeEvery } from '@redux-saga/core/effects';
import { StoreState } from 'store';
import { AddNewTicketAction, EditTicketAction } from 'types/store/actions';

function *addNewTicketSaga({ payload: ticketInfo }: AddNewTicketAction): Generator<Effect> {
    try {
        if (!ticketInfo.title || !ticketInfo.priority || !ticketInfo.categoryInfo || !ticketInfo.subcategoryInfo || !ticketInfo.description) {
            throw new Error('Ticket is incomplete!');
        }

        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const newTicketResponse = (yield call(addNewTicket, ticketInfo, token)) as Response;

        if (newTicketResponse.status !== 200) {
            throw new Error('Failed to create ticket!');
        }

        yield put(showSuccess({ header: 'Success!', message: 'Ticket successfully created!' }));
        yield put(refreshTickets());
        yield put(hideNewTicket());
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *editTicketSaga({ payload: ticket }: EditTicketAction): Generator<Effect> {
    try {
        if (!ticket.title || !ticket.priority || !ticket.categoryInfo || !ticket.subcategoryInfo || !ticket.description) {
            throw new Error('Ticket is incomplete!');
        }

        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const editTicketResponse = (yield call(editTicket, ticket, token)) as Response;

        if (editTicketResponse.status !== 200) {
            throw new Error('Failed to edit ticket!');
        }

        yield put(showSuccess({ header: 'Success!', message: 'Ticket successfully edited!' }));
        yield put(refreshTickets());
        yield put(setEditedTicket(null));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

export default function *newTicketRootSaga(): Generator<Effect> {
    yield all([
        takeEvery('ADD_NEW_TICKET', addNewTicketSaga),
        takeEvery('EDIT_TICKET', editTicketSaga)
    ]);
}
