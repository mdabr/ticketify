import {
    all,
    call,
    takeEvery,
    put,
    select
} from '@redux-saga/core/effects';
import {
    setTickets,
    showDanger,
    refreshTickets as refreshTicketsAction,
    setTicketDetails
} from 'store/actions';
import { Effect } from '@redux-saga/types';
import {
    fetchTickets,
    sendComment,
    editTicket,
    fetchTicketDetails,
    fetchUserWithRole
} from 'utils/FormService';
import { Ticket } from 'types/store/data';
import { STATUS, USER_TYPES } from 'utils/constants';
import {
    AcceptSolutionAction,
    ChangeAssigneeAction,
    CloseTicketAction,
    FetchTicketDetailsAction,
    FinishTicketAction,
    RejectSolutionAction,
    SendMessageAction,
    StartProgressAction
} from 'types/store/actions';
import { StoreState } from 'store';
import { User } from 'types/store/user';

function *refreshTickets(): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const ticketsResponse = (yield call(fetchTickets, token)) as Response;

        if (ticketsResponse.status !== 200) {
            throw new Error('Failed to fetch tickets!');
        }

        const tickets = (yield call([ticketsResponse, 'json'])) as Ticket[];

        yield put(setTickets(tickets));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *sendMessage({ payload: { message, ticketId } }: SendMessageAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const commentResponse = (yield call(sendComment, ticketId, message, token)) as Response;

        if (commentResponse.status !== 200) {
            throw new Error('Failed to send message!');
        }
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *changeAssignee({ payload: { ticket, assignee } }: ChangeAssigneeAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const changeAssigneeResponse = (yield call(editTicket, { ...ticket, technicianInfo: assignee, status: STATUS.ASSIGNED }, token)) as Response;

        if (changeAssigneeResponse.status !== 200) {
            throw new Error('Failed to assign technician!');
        }

        yield put(refreshTicketsAction());
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *startProgress({ payload: ticket }: StartProgressAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const startProgressResponse = (yield call(editTicket, { ...ticket, status: STATUS.IN_PROGRESS }, token)) as Response;

        if (startProgressResponse.status !== 200) {
            throw new Error('Failed to start progress!');
        }

        yield put(refreshTicketsAction());
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *finishTicket({ payload: ticket }: FinishTicketAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const finishTicketResponse = (yield call(editTicket, { ...ticket, status: STATUS.DONE }, token)) as Response;

        if (finishTicketResponse.status !== 200) {
            throw new Error('Failed to finish ticket!');
        }

        yield put(refreshTicketsAction());
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *closeTicket({ payload: ticket }: CloseTicketAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const closeTicketResponse = (yield call(editTicket, { ...ticket, status: STATUS.CLOSED }, token)) as Response;

        if (closeTicketResponse.status !== 200) {
            throw new Error('Failed to close ticket!');
        }

        yield put(refreshTicketsAction());
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *acceptSolution({ payload: ticket }: AcceptSolutionAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const acceptSolutionResponse = (yield call(editTicket, { ...ticket, technicianInfo: null, status: STATUS.COMPLETED }, token)) as Response;

        if (acceptSolutionResponse.status !== 200) {
            throw new Error('Failed to accept solution!');
        }

        yield put(refreshTicketsAction());
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *rejectSolution({ payload: ticket }: RejectSolutionAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const rejectSolutionResponse = (yield call(editTicket, { ...ticket, status: STATUS.ASSIGNED }, token)) as Response;

        if (rejectSolutionResponse.status !== 200) {
            throw new Error('Failed to reject solution!');
        }

        yield put(refreshTicketsAction());
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

function *fetchTicketDetailsSaga({ payload: { ticketId } }: FetchTicketDetailsAction): Generator<Effect> {
    try {
        const token = (yield select((state: StoreState) => state.token.token)) as string;
        const ticketDetailsResponse = (yield call(fetchTicketDetails, ticketId, token)) as Response;
        const assigneesResponse = (yield call(fetchUserWithRole, USER_TYPES.TECHNICIAN, token)) as Response;

        if (ticketDetailsResponse.status !== 200) {
            throw new Error('Failed to fetch ticket details');
        }

        if (assigneesResponse.status !== 200) {
            throw new Error('Failed to fetch assignees');
        }

        const ticket = (yield call([ticketDetailsResponse, 'json'])) as Ticket;
        const assignees = (yield call([assigneesResponse, 'json'])) as User[];

        yield put(setTicketDetails(ticket, assignees));
    } catch (error) {
        yield put(showDanger({ header: 'Error!', message: error.message }));
    }
}

export default function *ticketDetailsRootSaga(): Generator<Effect> {
    yield all([
        takeEvery('REFRESH_TICKETS', refreshTickets),
        takeEvery('SEND_MESSAGE', sendMessage),
        takeEvery('CHANGE_ASSIGNEE', changeAssignee),
        takeEvery('START_PROGRESS', startProgress),
        takeEvery('FINISH_TICKET', finishTicket),
        takeEvery('CLOSE_TICKET', closeTicket),
        takeEvery('ACCEPT_SOLUTION', acceptSolution),
        takeEvery('REJECT_SOLUTION', rejectSolution),
        takeEvery('FETCH_TICKET_DETAILS', fetchTicketDetailsSaga)
    ]);
}
