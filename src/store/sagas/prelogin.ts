import { Ticket, Category } from 'types/store/data';
import { User } from 'types/store/user';
import { Token } from 'types/store/token';
import { all, call, Effect, put, takeEvery } from 'redux-saga/effects';
import { TOAST_LEVELS } from 'utils/constants';
import { fetchCategories, fetchMyself, fetchTickets, submitLogin, submitRegister, resetPassword as resetPasswordUtil } from 'utils/FormService';
import { path } from 'ramda';
import { LogInAction, RegisterCompanyAction, ResetPasswordAction } from 'types/store/actions';

function *logIn({ payload: { email, password } }: LogInAction): Generator<Effect> {
    try {
        if (!email) {
            throw new Error('Fill the login input!');
        }

        if (!password) {
            throw new Error('Fill the password input!');
        }

        yield put({ type: 'SHOW_LOADER' });

        const loginResponse = (yield call(submitLogin, { email, password })) as Response;

        if (loginResponse.status !== 200) {
            throw new Error('Failed to login');
        }

        const { expires, token } = (yield call([loginResponse, 'json'])) as Token;

        yield put({ type: 'UPDATE_TOKEN', expires, token });

        const meResponse = (yield call(fetchMyself, token)) as Response;
        const user = (yield call([meResponse, 'json'])) as User;
        const allResponses = (yield all([
            call(fetchTickets, token),
            call(fetchCategories, token, undefined, { companyId: path(['companyInfo', 'id'], user) as string })
        ])) as Response[];

        if (allResponses.some(response => response.status !== 200)) {
            throw new Error('Something went wrong on our part. Please try again later. We\'re sorry :(');
        }

        const data = (yield all(allResponses.map(response => call([response, 'json'])))) as Array<any>;

        yield put({ type: 'CHANGE_USER', user });
        yield put({ type: 'SET_TICKETS', tickets: data[0] as Ticket[] });
        yield put({ type: 'SET_CATEGORIES', categories: data[1] as Category[] });
    } catch (error) {
        yield put({ type: 'ADD_TOAST', header: 'Error!', message: error.message, level: TOAST_LEVELS.DANGER });
    } finally {
        yield put({ type: 'HIDE_LOADER' });
    }
}

function *registerCompany({ payload: { companyName, contactEmail } }: RegisterCompanyAction): Generator<Effect> {
    try {
        if (!companyName) {
            throw new Error('Fill the company input!');
        }

        if (!contactEmail) {
            throw new Error('Fill the email input!');
        }

        yield put({ type: 'SHOW_LOADER' });

        const registerResponse = (yield call(submitRegister, { companyName, contactEmail })) as Response;

        if (registerResponse.status !== 200) {
            throw new Error('Failed to register company');
        }

        yield put({ type: 'ADD_TOAST', header: 'Success!', message: 'Company registration submitted!', level: TOAST_LEVELS.SUCCESS });
    } catch (error) {
        yield put({ type: 'ADD_TOAST', header: 'Error!', message: error.message, level: TOAST_LEVELS.DANGER });
    } finally {
        yield put({ type: 'HIDE_LOADER' });
    }
}

function *resetPassword({ payload: { email } }: ResetPasswordAction) {
    try {
        if (!email) {
            throw new Error('Fill the login input!');
        }

        yield put({ type: 'SHOW_LOADER' });

        const resetResponse: Response = yield call(resetPasswordUtil, { email });

        if (resetResponse.status !== 200) {
            throw new Error('Failed to send password reset request!');
        }

        yield put({ type: 'ADD_TOAST', header: 'Success!', message: 'Password reset email was sent if account exists!', level: TOAST_LEVELS.SUCCESS });
    } catch (error) {
        yield put({ type: 'ADD_TOAST', header: 'Error!', message: error.message, level: TOAST_LEVELS.DANGER });
    } finally {
        yield put({ type: 'HIDE_LOADER' });
    }
}

export default function *preloginRootSaga(): Generator<Effect> {
    yield all([
        takeEvery('LOG_IN', logIn),
        takeEvery('REGISTER_COMPANY', registerCompany),
        takeEvery('RESET_PASSWORD', resetPassword)
    ]);
}
