import { showDanger, showSuccess, setCategories } from 'store/actions';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { AddCategoryEpic, AddCompanyEpic, AddSubcategoryEpic, AddUserEpic, DeleteCategoryEpic, EditCategoryEpic, EditCompanyEpic, EditSubcategoryEpic, EditUserEpic, FetchCategoriesEpic } from 'types/store/epics';
import { from, merge, of, throwError } from 'rxjs';
import { isCompanyValid } from 'utils/company';
import { isUserValid } from 'utils/user';

export const addCompanyEpic: AddCompanyEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('ADD_COMPANY'),
    mergeMap(
        ({ payload }) => isCompanyValid(payload)
            ? from(FormService.addCompany(payload, state$.value.token.token)).pipe(
                mergeMap(
                    response => response.status === 200
                        ? of(showSuccess({ header: 'Success!', message: 'Company successfully added!' }))
                        : throwError(new Error('Failed to add a new company!'))
                )
            )
            : throwError(new Error('Company information are incomplete!'))
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const addUserEpic: AddUserEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('ADD_USER'),
    mergeMap(
        ({ payload }) => isUserValid(payload)
            ? from(FormService.addUser(payload, state$.value.token.token)).pipe(
                mergeMap(
                    response => response.status === 200
                        ? of(showSuccess({ header: 'Success!', message: 'User successfully added!' }))
                        : throwError(new Error('Failed to add a new user!'))
                )
            )
            : throwError(new Error('User information are incomplete!'))
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const fetchCategoriesEpic: FetchCategoriesEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('FETCH_CATEGORIES'),
    mergeMap(
        ({ payload: { companyId } }) => from(FormService.fetchCategories(state$.value.token.token, undefined, { companyId })).pipe(
            mergeMap(
                response => response.status === 200
                    ? from(response.json()).pipe(
                        map(setCategories)
                    )
                    : throwError(new Error('Failed to fetch categories!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const addSubcategoryEpic: AddSubcategoryEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('ADD_SUBCATEGORY'),
    mergeMap(
        ({ payload: { categoryId, name } }) => from(FormService.addSubcategory(categoryId, { name, enabled: true }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? of(showSuccess({ header: 'Success!', message: 'Subcategory successfully created!' }))
                    : throwError(new Error('Failed to add subcategory!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const addCategoryEpic: AddCategoryEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('ADD_CATEGORY'),
    mergeMap(
        ({ payload: { companyId, name } }) => from(FormService.addCategory({ name, enabled: true, company: { id: companyId } }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? of(showSuccess({ header: 'Success!', message: 'Category successfully created!' }))
                    : throwError(new Error('Failed to add category!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const deleteCategoryEpic: DeleteCategoryEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('DELETE_CATEGORY'),
    mergeMap(
        ({ payload: { categoryId } }) => from(FormService.deleteCategory(categoryId, state$.value.token.token)).pipe(
            mergeMap(response => response.status === 200
                ? of(showSuccess({ header: 'Success!', message: 'Category successfully deleted!' }))
                : throwError(new Error('Failed to delete category!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const editSubcategoryEpic: EditSubcategoryEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('EDIT_SUBCATEGORY'),
    mergeMap(
        ({ payload }) => from(FormService.editSubcategory(payload.categoryId, payload.subcategory, state$.value.token.token)).pipe(
            mergeMap(response => response.status === 200
                ? of(showSuccess({ header: 'Success!', message: 'Subcategory successfully edited!' }))
                : throwError(new Error('Failed to edit subcategory!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const editCategoryEpic: EditCategoryEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('EDIT_CATEGORY'),
    mergeMap(
        ({ payload }) => from(FormService.editCategory(payload, state$.value.token.token)).pipe(
            mergeMap(response => response.status === 200
                ? of(showSuccess({ header: 'Success!', message: 'Category successfully edited!' }))
                : throwError(new Error('Failed to edit category!')))
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const editCompanyEpic: EditCompanyEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('EDIT_COMPANY'),
    mergeMap(
        ({ payload }) => from(FormService.editCompany(payload, state$.value.token.token)).pipe(
            mergeMap(response => response.status === 200
                ? of(showSuccess({ header: 'Success!', message: 'Company successfully edited!' }))
                : throwError(new Error('Failed to edit company!')))
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const editUserEpic: EditUserEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('EDIT_USER'),
    mergeMap(
        ({ payload }) => from(FormService.editUser(payload, state$.value.token.token)).pipe(
            mergeMap(response => response.status === 200
                ? of(showSuccess({ header: 'Success!', message: 'User successfully edited!' }))
                : throwError(new Error('Failed to edit user!')))
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);
