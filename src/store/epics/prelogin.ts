import { Dependencies } from 'types/store/common';
import { path } from 'utils/helpers';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Epic, ofType } from 'redux-observable';
import { LogInAction, RegisterCompanyAction, ResetPasswordAction } from 'types/store/actions';
import { concat, from, merge, of } from 'rxjs';
import { hideLoader, setTickets, showLoader, updateToken, setCategories, changeUser, showDanger, showSuccess } from 'store/actions';
import { Token } from 'types/store/token';
import { User } from 'types/store/user';

export const logInEpic: Epic = (action$, $state, { FormService }: Dependencies) => action$.pipe(
    ofType<LogInAction>('LOG_IN'),
    mergeMap(({ payload: { email, password } }) => concat(
        of(showLoader()),
        from(FormService.submitLogin({ email, password })).pipe(
            mergeMap(response => from(response.json())),
            mergeMap(
                ({ expires, token }: Token) => concat(
                    of(updateToken({ token, expires })),
                    from(FormService.fetchMyself(token)).pipe(
                        mergeMap(response => from(response.json())),
                        mergeMap((user: User) => concat(
                            from(FormService.fetchTickets(token)).pipe(
                                mergeMap(response => from(response.json())),
                                map(setTickets)
                            ),
                            from(FormService.fetchCategories(token, undefined, { companyId: path(['companyInfo', 'id'], user) as string })).pipe(
                                mergeMap(response => from(response.json())),
                                map(setCategories)
                            ),
                            of(changeUser(user))
                        ))
                    )
                )
            )
        ),
        of(hideLoader())
    )),
    catchError((error, caught) => merge(
        of(showDanger({ header: 'Error!', message: 'Failed to log in' })),
        of(hideLoader()),
        caught
    ))
);

export const registerCompanyEpic: Epic = (action$, $state, { FormService }: Dependencies) => action$.pipe(
    ofType<RegisterCompanyAction>('REGISTER_COMPANY'),
    mergeMap(({ payload: { companyName, contactEmail } }) => concat(
        of(showLoader()),
        from(FormService.submitRegister({ companyName, contactEmail })).pipe(
            map(response => response.status),
            map(status => status === 200
                ? showSuccess({ header: 'Success!', message: 'Company registration submitted!' })
                : showDanger({ header: 'Error!', message: 'Company registration failed!' }))
        ),
        of(hideLoader())
    )),
    catchError((error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        of(hideLoader()),
        caught
    ))
);

export const resetPasswordEpic: Epic = (action$, $state, { FormService }: Dependencies) => action$.pipe(
    ofType<ResetPasswordAction>('RESET_PASSWORD'),
    mergeMap(({ payload: { email } }) => concat(
        of(showLoader()),
        from(FormService.resetPassword({ email })).pipe(
            map(response => response.status),
            map(status => status === 200
                ? showSuccess({ header: 'Success!', message: 'Password reset email was sent if account exists!' })
                : showDanger({ header: 'Error!', message: 'Failed to send password reset request!' }))
        ),
        of(hideLoader())
    )),
    catchError((error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        of(hideLoader()),
        caught
    ))
);
