import { combineEpics } from 'redux-observable';
import { logInEpic, registerCompanyEpic, resetPasswordEpic } from './prelogin';
import { refreshTicketsEpic, changeAssigneeEpic, finishTicketEpic, sendMessageEpic, startProgressEpic, acceptSolutionEpic, closeTicketEpic, fetchTicketDetailsEpic, rejectSolutionEpic } from './ticketdetails';
import { addNewTicketEpic, editTicketEpic } from './newticket';
import { addCategoryEpic, addCompanyEpic, addSubcategoryEpic, addUserEpic, deleteCategoryEpic, editCategoryEpic, editCompanyEpic, editSubcategoryEpic, editUserEpic, fetchCategoriesEpic } from './userdetails';

export const rootEpic = combineEpics(
    refreshTicketsEpic,
    logInEpic,
    registerCompanyEpic,
    resetPasswordEpic,
    sendMessageEpic,
    changeAssigneeEpic,
    startProgressEpic,
    finishTicketEpic,
    acceptSolutionEpic,
    closeTicketEpic,
    fetchTicketDetailsEpic,
    rejectSolutionEpic,
    addNewTicketEpic,
    editTicketEpic,
    addCategoryEpic,
    addCompanyEpic,
    addSubcategoryEpic,
    addUserEpic,
    deleteCategoryEpic,
    editCategoryEpic,
    editCompanyEpic,
    editSubcategoryEpic,
    editUserEpic,
    fetchCategoriesEpic
);
