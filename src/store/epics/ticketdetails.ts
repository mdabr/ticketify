import { ofType } from 'redux-observable';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { merge, of, throwError, forkJoin, concat, from } from 'rxjs';
import { hideLoader, refreshTickets, setTicketDetails, setTickets, showDanger, showLoader, showTicketDetails } from 'store/actions';
import { Dependencies } from 'types/store/common';
import { SendMessageEpic, RefreshTicketsEpic, ChangeAssigneeEpic, StartProgressEpic, FinishTicketEpic, CloseTicketEpic, AcceptSolutionEpic, RejectSolutionEpic, FetchTicketDetailsEpic } from 'types/store/epics';
import { STATUS, USER_TYPES } from 'utils/constants';

export const refreshTicketsEpic: RefreshTicketsEpic = (action$, state$, { FormService }: Dependencies) => action$.pipe(
    ofType('REFRESH_TICKETS'),
    mergeMap(
        () => from(FormService.fetchTickets(state$.value.token.token)).pipe(
            mergeMap(response => from(response.json()).pipe(
                map(tickets => setTickets(tickets))
            ))
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: 'Failed to fetch tickets!' })),
        caught
    ))
);

export const sendMessageEpic: SendMessageEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('SEND_MESSAGE'),
    mergeMap(
        ({ payload: { message, ticketId } }) => from(FormService.sendComment(ticketId, message, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? of(showTicketDetails(ticketId))
                    : throwError(new Error('Failed to send message!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const changeAssigneeEpic: ChangeAssigneeEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('CHANGE_ASSIGNEE'),
    mergeMap(
        ({ payload: { assignee, ticket } }) => from(FormService.editTicket({ ...ticket, technicianInfo: assignee, status: STATUS.ASSIGNED }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? concat(
                        of(refreshTickets()),
                        of(showTicketDetails(ticket.id))
                    )
                    : throwError(new Error('Failed to assign technician!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const startProgressEpic: StartProgressEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('START_PROGRESS'),
    mergeMap(
        ({ payload }) => from(FormService.editTicket({ ...payload, status: STATUS.IN_PROGRESS }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? concat(
                        of(refreshTickets()),
                        of(showTicketDetails(payload.id))
                    )
                    : throwError(new Error('Failed to start progress!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const finishTicketEpic: FinishTicketEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('FINISH_TICKET'),
    mergeMap(
        ({ payload }) => from(FormService.editTicket({ ...payload, status: STATUS.DONE }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? concat(
                        of(refreshTickets()),
                        of(showTicketDetails(payload.id))
                    )
                    : throwError(new Error('Failed to finish ticket!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const closeTicketEpic: CloseTicketEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('CLOSE_TICKET'),
    mergeMap(
        ({ payload }) => from(FormService.editTicket({ ...payload, status: STATUS.DONE }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? concat(
                        of(refreshTickets()),
                        of(showTicketDetails(payload.id))
                    )
                    : throwError(new Error('Failed to close ticket!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const acceptSolutionEpic: AcceptSolutionEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('ACCEPT_SOLUTION'),
    mergeMap(
        ({ payload }) => from(FormService.editTicket({ ...payload, status: STATUS.DONE }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? concat(
                        of(refreshTickets()),
                        of(showTicketDetails(payload.id))
                    )
                    : throwError(new Error('Failed to accept solution!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const rejectSolutionEpic: RejectSolutionEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('REJECT_SOLUTION'),
    mergeMap(
        ({ payload }) => from(FormService.editTicket({ ...payload, status: STATUS.DONE }, state$.value.token.token)).pipe(
            mergeMap(
                response => response.status === 200
                    ? concat(
                        of(refreshTickets()),
                        of(showTicketDetails(payload.id))
                    )
                    : throwError(new Error('Failed to reject solution!'))
            )
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const fetchTicketDetailsEpic: FetchTicketDetailsEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('FETCH_TICKET_DETAILS'),
    mergeMap(
        ({ payload: { ticketId, withLoader } }) => concat(
            withLoader ? of(showLoader()) : of(),
            forkJoin({
                ticket: from(FormService.fetchTicketDetails(ticketId, state$.value.token.token)).pipe(
                    mergeMap(
                        response => response.status === 200
                            ? from(response.json())
                            : throwError(new Error('Failed to fetch ticket details'))
                    )
                ),
                assignees: from(FormService.fetchUserWithRole(USER_TYPES.TECHNICIAN, state$.value.token.token)).pipe(
                    mergeMap(
                        response => response.status === 200
                            ? from(response.json())
                            : throwError(new Error('Failed to fetch ticket details'))
                    )
                )
            }).pipe(map(({ ticket, assignees }) => setTicketDetails(ticket, assignees))),
            withLoader ? of(hideLoader()) : of()
        )
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        of(hideLoader()),
        caught
    ))
);
