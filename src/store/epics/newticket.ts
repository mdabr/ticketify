import { catchError, mergeMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { AddNewTicketEpic, EditTicketEpic } from 'types/store/epics';
import { from, merge, of, throwError, concat } from 'rxjs';
import { showDanger, showSuccess, refreshTickets, hideNewTicket, setEditedTicket } from 'store/actions';
import { isTicketValid } from 'utils/ticket';

export const addNewTicketEpic: AddNewTicketEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('ADD_NEW_TICKET'),
    mergeMap(
        ({ payload }) => isTicketValid(payload)
            ? from(FormService.addNewTicket(payload, state$.value.token.token)).pipe(
                mergeMap(
                    response => response.status === 200
                        ? concat(
                            of(showSuccess({ header: 'Success!', message: 'Ticket successfully created!' })),
                            of(refreshTickets()),
                            of(hideNewTicket())
                        )
                        : throwError(new Error('Failed to create ticket!'))
                )
            )
            : throwError(new Error('Ticket is incomplete!'))
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);

export const editTicketEpic: EditTicketEpic = (action$, state$, { FormService }) => action$.pipe(
    ofType('EDIT_TICKET'),
    mergeMap(
        ({ payload }) => isTicketValid(payload)
            ? from(FormService.editTicket(payload, state$.value.token.token)).pipe(
                mergeMap(
                    response => response.status === 200
                        ? concat(
                            of(showSuccess({ header: 'Success!', message: 'Ticket successfully edited!' })),
                            of(refreshTickets()),
                            of(setEditedTicket(null))
                        )
                        : throwError(new Error('Failed to edit ticket!'))
                )
            )
            : throwError(new Error('Ticket is incomplete!'))
    ),
    catchError((error: Error, caught) => merge(
        of(showDanger({ header: 'Error!', message: error.message })),
        caught
    ))
);
