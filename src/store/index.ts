import { configureStore } from '@reduxjs/toolkit';
import { createEpicMiddleware } from 'redux-observable';
import combinedReducer from './reducers';
import * as FormService from 'utils/FormService';
import { rootEpic } from 'store/epics';
import { Dependencies } from 'types/store/common';
// import rootSaga from './store/sagas';

const epicMiddleware = createEpicMiddleware({
    dependencies: {
        FormService
    } as Dependencies
});
// const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: combinedReducer,
    devTools: process.env.NODE_ENV !== 'production',
    middleware: [epicMiddleware]
});

epicMiddleware.run(rootEpic);
// sagaMiddleware.run(rootSaga);

export type StoreState = ReturnType<typeof store.getState>;

export default store;
