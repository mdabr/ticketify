import { Reducer } from 'redux';
import { SetTicketDetailsAction } from 'types/store/actions';
import { TicketDetailsActions, TicketDetailsState } from 'types/store/ticketdetails';
import { defaultReducer } from 'utils/store';

const defaultTicketDetails: TicketDetailsState = {
    ticketDetailsVisible: false,
    ticket: null,
    assignees: []
};

const HIDE_TICKET_DETAILS = (state: TicketDetailsState) => ({
    ...state,
    ticketDetailsVisible: false,
    ticket: null
});
const SET_TICKET_DETAILS = (state: TicketDetailsState, action: TicketDetailsActions) => ({
    ...state,
    ticketDetailsVisible: true,
    ticket: (action as SetTicketDetailsAction).payload.ticket,
    assignees: (action as SetTicketDetailsAction).payload.assignees
});
const actions: Record<string, (state: TicketDetailsState, action: TicketDetailsActions) => any> = {
    HIDE_TICKET_DETAILS,
    SET_TICKET_DETAILS
};

export default defaultReducer(actions, defaultTicketDetails) as Reducer<TicketDetailsState, TicketDetailsActions>;
