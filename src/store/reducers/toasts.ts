import { Reducer } from 'redux';
import { v4 } from 'uuid';
import {
    Toast,
    ToastInfo,
    ToastsActions,
    ToastsState
} from 'types/store/toasts';
import { defaultReducer } from 'utils/store';
import { AddToastAction, RemoveToastAction } from 'types/store/actions';

const defaultToasts: Toast[] = [];
const mapActionToToast = (action: AddToastAction): Toast => ({
    id: v4(),
    header: action.payload.header,
    message: action.payload.message,
    level: action.payload.level,
    time: action.payload.time || 5000
});
const isDuplicateToast = (newToast: ToastInfo) => (toast: Toast) => newToast.header === toast.header && newToast.message === toast.message;
const ADD_TOAST = (state: ToastsState, action: ToastsActions): ToastsState => state
    .filter(toast => !isDuplicateToast((action as AddToastAction).payload)(toast))
    .concat(mapActionToToast(action as AddToastAction));
const REMOVE_TOAST = (state: ToastsState, action: ToastsActions) => state.filter(toast => toast.id !== (action as RemoveToastAction).payload.id);
const actions: Record<string, (state: ToastsState, action: ToastsActions) => Toast[]> = {
    ADD_TOAST,
    REMOVE_TOAST
};

export default defaultReducer(actions, defaultToasts) as Reducer<ToastsState, ToastsActions>;
