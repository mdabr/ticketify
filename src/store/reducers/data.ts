import { Reducer } from 'redux';
import { path, isEmpty, isNil, type, equals } from 'ramda';
import { Ticket, Specifiers, TicketPredicate, DataState, Category, DataActions } from 'types/store/data';
import { defaultReducer } from 'utils/store';

const defaultData: DataState = {
    tickets: [],
    categories: [],
    filteredTickets: []
};

const SET_TICKETS = (state: DataState, action: DataActions): DataState => ({
    ...state,
    tickets: action.payload as Ticket[],
    filteredTickets: action.payload as Ticket[]
});
const SET_CATEGORIES = (state: DataState, action: DataActions): DataState => ({
    ...state,
    categories: action.payload as Category[]
});
const FILTER_TICKETS = (state: DataState, action: DataActions): DataState => ({
    ...state,
    filteredTickets: searchIn(state.tickets)(action.payload as Specifiers)
});
const actions: Record<string, (state: DataState, action: DataActions) => DataState> = {
    SET_TICKETS,
    SET_CATEGORIES,
    FILTER_TICKETS
};

export default defaultReducer(actions, defaultData) as Reducer<DataState, DataActions>;

/**
 * Gives tool for searching in specified tickets list
 * @param {Ticket[]} tickets list of all available tickets
 * @returns {(specifiers: Specifier[]) => Ticket[]} searcher that needs specifiers
 */
function searchIn(tickets: Ticket[]): (specifiers: Specifiers) => Ticket[] {

    /**
     * Searches for tickets matching provided specifiers
     * @param {Object} specifiers define parameters for search function
     * @return {Ticket[]} tickets matching provided specifiers
     */
    return function searchFor(specifiers: Specifiers): Ticket[] {
        const predicates: Record<string, TicketPredicate> = {
            authorInfo: ticket => isEmpty(specifiers.creators) || !isNil(specifiers.creators.find(creator => creator.id === path(['authorInfo', 'id'], ticket))),

            categoryInfo: ticket => isEmpty(specifiers.categories) ? true : !isNil(specifiers.categories.find(category => category.id === path(['categoryInfo', 'id'], ticket))),

            createdAt: ticket => new Date(ticket.createdAt) >= new Date(specifiers.startedFrom) && new Date(ticket.createdAt) <= new Date(specifiers.startedTo),

            id: ticket => specifiers.id === '' ? true : ticket.id === parseInt(specifiers.id, 10),

            lastModifiedDate: ticket => new Date(ticket.lastModifiedDate) >= new Date(specifiers.modifiedFrom) && new Date(ticket.lastModifiedDate) <= new Date(specifiers.modifiedTo),

            priority: ticket => isEmpty(specifiers.priorities) ? true : !isNil(specifiers.priorities.find(priority => priority === ticket.priority)),

            status: ticket => isEmpty(specifiers.status) ? true : !isNil(specifiers.status.find(status => status === ticket.status)),

            subcategoryInfo: ticket => isEmpty(specifiers.subcategories) ? true : !isNil(specifiers.subcategories.find(subcategory => subcategory.id === path(['subcategoryInfo', 'id'], ticket))),

            technicianInfo: ticket => isEmpty(specifiers.assignees) ? true : !isNil(specifiers.assignees.find(assignee => assignee.id === path(['technicianInfo', 'id'], ticket))),

            title: ticket => specifiers.title === '' ? true : ticket.title.includes(specifiers.title),

            companyInfo: ticket => isEmpty(specifiers.companies) ? true : !isNil(specifiers.companies.find(company => company.id === path(['companyInfo', 'id'], ticket)))
        };

        const isValidTicket: TicketPredicate = ticket => Object.keys(ticket).every(specifier => equals(type(predicates[specifier]), 'Function') ? predicates[specifier](ticket) : true);

        return tickets.filter(isValidTicket);
    };
}
