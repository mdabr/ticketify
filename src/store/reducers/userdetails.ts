import { Reducer } from 'redux';
import { UserDetailsActions, UserDetailsState } from 'types/store/userdetails';
import { defaultReducer } from 'utils/store';

const defaultUserDetails: UserDetailsState = {
    userDetailsVisible: false
};

const SHOW_USER_DETAILS = () => ({ userDetailsVisible: true });
const HIDE_USER_DETAILS = () => ({ userDetailsVisible: false });
const actions: Record<string, (state: UserDetailsState, action: UserDetailsActions) => UserDetailsState> = {
    SHOW_USER_DETAILS,
    HIDE_USER_DETAILS
};

export default defaultReducer(actions, defaultUserDetails) as Reducer<UserDetailsState, UserDetailsActions>;
