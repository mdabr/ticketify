import { defaultReducer } from 'utils/store';
import { USER_TYPES } from 'utils/constants';
import { UserActions, UserState } from 'types/store/user';
import { ChangeUserAction } from 'types/store/actions';
import { Reducer } from 'redux';

const defaultUser: UserState = {
    id: null,
    firstName: null,
    lastName: null,
    roles: [{ role: USER_TYPES.NONE }],
    companyInfo: null,
    email: null,
    enabled: false
};
const CHANGE_USER = (_state: UserState, action: UserActions): UserState => (action as ChangeUserAction).payload;
const RESET_USER = () => defaultUser;
const actions: Record<string, (state: UserState, action: UserActions) => UserState> = {
    CHANGE_USER,
    RESET_USER
};

export default defaultReducer(actions, defaultUser) as Reducer<UserState, UserActions>;
