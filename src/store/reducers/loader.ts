import { Reducer } from 'redux';
import { LoaderActions, LoaderState } from 'types/store/loader';
import { defaultReducer } from 'utils/store';

const defaultLoader: LoaderState = {
    loaderVisible: false
};

const SHOW_LOADER = () => ({ loaderVisible: true });
const HIDE_LOADER = () => ({ loaderVisible: false });
const actions: Record<string, (state: LoaderState, action: LoaderActions) => { loaderVisible: boolean }> = {
    SHOW_LOADER,
    HIDE_LOADER
};

export default defaultReducer(actions, defaultLoader) as Reducer<LoaderState, LoaderActions>;
