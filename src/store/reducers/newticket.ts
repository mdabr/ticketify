import { Reducer } from 'redux';
import { SetEditedTicketAction } from 'types/store/actions';
import { NewTicketActions, NewTicketState } from 'types/store/newticket';
import { defaultReducer } from 'utils/store';

const defaultNewTicket: NewTicketState = {
    newTicketVisible: false,
    editedTicket: null
};

const SHOW_NEW_TICKET = (): NewTicketState => ({ editedTicket: null, newTicketVisible: true });
const HIDE_NEW_TICKET = (): NewTicketState => ({ editedTicket: null, newTicketVisible: false });
const SET_EDITED_TICKET = (_state: NewTicketState, action: NewTicketActions): NewTicketState => ({ editedTicket: (action as SetEditedTicketAction).payload, newTicketVisible: Boolean((action as SetEditedTicketAction).payload) });
const actions: Record<string, (state: NewTicketState, action: NewTicketActions) => NewTicketState> = {
    SHOW_NEW_TICKET,
    HIDE_NEW_TICKET,
    SET_EDITED_TICKET
};

export default defaultReducer(actions, defaultNewTicket) as Reducer<NewTicketState, NewTicketActions>;
