import { Reducer } from 'redux';
import {
    TokenActions, TokenState
} from 'types/store/token';
import { defaultReducer } from 'utils/store';

const defaultToken: TokenState = {
    expires: new Date(),
    token: ''
};
const UPDATE_TOKEN = (_state: TokenState, action: TokenActions): TokenState => ({
    expires: new Date(action.payload.expires),
    token: action.payload.token
});
const actions: Record<string, (state: TokenState, action: TokenActions) => TokenState> = {
    UPDATE_TOKEN
};

export default defaultReducer(actions, defaultToken) as Reducer<TokenState, TokenActions>;
