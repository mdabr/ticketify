import { combineReducers } from 'redux';
import user from './user';
import toasts from './toasts';
import loader from './loader';
import userdetails from './userdetails';
import ticketdetails from './ticketdetails';
import newticket from './newticket';
import data from './data';
import token from './token';

export default combineReducers({
    user,
    toasts,
    loader,
    userdetails,
    ticketdetails,
    newticket,
    data,
    token
});
