import { BrowserRouter as Router } from 'react-router-dom';
import './App.scss';
import { Infobox, Loader } from 'components';
import { useSelector } from 'react-redux';
import ViewHolder from 'views/ViewHolder/ViewHolder';
import { StoreState } from 'store';
import { User } from 'types/store/user';

export default function App(): JSX.Element {
    const user = useSelector((state: StoreState) => state.user);

    return (
        <div className="app-route">
            <Router basename="/ticketify">
                <ViewHolder user={user as User} />
            </Router>
            <Infobox />
            <Loader />
        </div>
    );
}
