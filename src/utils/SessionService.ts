import { UserState } from 'types/store/user';

const SessionService = {
    saveUser(user: UserState): void {
        window.sessionStorage.setItem('user', JSON.stringify(user));
    },
    loadUser(): UserState {
        const stringifiedUser = window.sessionStorage.getItem('user');

        return stringifiedUser ? JSON.parse(stringifiedUser) : null;
    },
    resetUser(): void {
        window.sessionStorage.removeItem('user');
    }
};

export default SessionService;
