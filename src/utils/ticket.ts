import { Category, Subcategory, Ticket, TicketInfo } from 'types/store/data';
import { path } from 'utils/helpers';

export function getName(object: Record<string, any>): string {
    return path(['name'], object) || '---';
}

export function findCategory(category: Category | null, subcategory: Subcategory | null, CATEGORIES: Category[]): Category | null {
    if (category) {
        return category.subcategories ? category : CATEGORIES.find(cat => cat.id === category.id) || null;
    }

    if (subcategory) {
        return CATEGORIES.find(cat => cat.subcategories.find(sub => sub.name === subcategory.name)) || null;
    }

    return null;
}

export function isTicketValid(ticketInfo?: TicketInfo | Ticket | null): boolean {
    return Boolean(ticketInfo) &&
        Boolean((ticketInfo as TicketInfo).authorInfo) &&
        Boolean((ticketInfo as TicketInfo).categoryInfo) &&
        Boolean((ticketInfo as TicketInfo).subcategoryInfo) &&
        Boolean((ticketInfo as TicketInfo).companyInfo) &&
        Boolean((ticketInfo as TicketInfo).createdAt) &&
        Boolean((ticketInfo as TicketInfo).description) &&
        Boolean((ticketInfo as TicketInfo).priority) &&
        Boolean((ticketInfo as TicketInfo).title);
}
