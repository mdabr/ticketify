import {
    List,
    ListItem,
    TextField,
    InputLabel,
    ListItemText,
    Button,
    Input,
    Select,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Checkbox,
    Icon,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    ListProps,
    TextFieldProps,
    InputLabelProps,
    SelectProps,
    ListItemTextProps,
    ButtonProps,
    TableProps,
    TableHeadProps,
    TableRowProps,
    TableCellProps,
    TableBodyProps,
    CheckboxProps,
    AccordionProps,
    AccordionSummaryProps,
    AccordionDetailsProps,
    IconProps,
    InputProps,
    ListItemProps
} from '@material-ui/core';
import {
    KeyboardDatePicker, KeyboardDatePickerProps
} from '@material-ui/pickers';
import { ExpandMore } from '@material-ui/icons';
import {
    calendarTheme,
    useListStyles,
    useListItemStyles,
    useDescTextFieldStyles,
    useTextFieldStyles,
    useInputLabelStyles,
    useInputStyles,
    useSelectStyles,
    useAccordionStyles,
    useAccordionSummaryStyles,
    useAccordionDetailsStyles,
    useFilterIconStyles,
    useKeyboardDatePickerStyles,
    useTableHeadStyles,
    useTableBodyRowStyles,
    useTableHeadCellStyles,
    useTableCellStyles,
    useTableHeadCellCategoryStyles,
    useTableCellCategoryStyles,
    useTableRowCategoryStyles
} from './uiStyles';

function TList({ children, ...props }: ListProps): JSX.Element {
    const classes = useListStyles();

    return <List classes={{
        root: classes.list
    }} {...props}>{children}</List>;
}

function TListItem({ children, ...props }: ListItemProps): JSX.Element {
    const classes = useListItemStyles();

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return <ListItem classes={{
        root: classes.listItem,
        selected: classes.listItemSelected
    }} {...props}>{children}</ListItem>;
}

function TDescTextField({ children, ...props }: TextFieldProps): JSX.Element {
    const classes = useDescTextFieldStyles();

    return <TextField
        {...props}
        className={classes.select}
        InputLabelProps={{
            classes: {
                root: classes.textFieldLabel
            }
        }}
        InputProps={{
            className: classes.underline
        }}>{children}</TextField>;
}

function TTextField({ children, ...props }: TextFieldProps): JSX.Element {
    const classes = useTextFieldStyles();

    return <TextField
        {...props}
        className={classes.select}
        InputLabelProps={{
            classes: {
                root: classes.textFieldLabel
            }
        }}
        InputProps={{
            className: classes.underline
        }}
    >{children}</TextField>;
}

function TInputLabel({ children, ...props }: InputLabelProps): JSX.Element {
    const classes = useInputLabelStyles();

    return <InputLabel className={classes.selectLabel} {...props}>{children}</InputLabel>;
}

function TListItemText({ ...props }: ListItemTextProps): JSX.Element {
    return <ListItemText {...props} />;
}

function TButton({ children, ...props }: ButtonProps): JSX.Element {
    return <Button {...props}>{children}</Button>;
}

function TInput({ ...props }: InputProps): JSX.Element {
    const classes = useInputStyles();

    return <Input classes={{
        underline: classes.underline
    }} {...props}></Input>;
}

function TSelect({ children, ...props }: SelectProps): JSX.Element {
    const classes = useSelectStyles();
    const selectCommons = {
        className: classes.select,
        classes: {
            icon: classes.selectIcon
        },
        input: <TInput />
    };

    return <Select {...selectCommons} {...props}>{children}</Select>;
}

function TAccordion({ children, ...props }: AccordionProps): JSX.Element {
    const classes = useAccordionStyles();

    return <Accordion {...props} classes={{
        root: classes.expansionPanel,
        expanded: ''
    }}>{children}</Accordion>;
}

function TAccordionSummary({ children, ...props }: AccordionSummaryProps): JSX.Element {
    const classes = useAccordionSummaryStyles();

    return <AccordionSummary {...props} classes={{
        root: classes.expansionSummary
    }} className={classes.expansionPanelExpanded} expandIcon={<ExpandMore classes={{
        root: classes.expandMore
    }} />}>{children}</AccordionSummary>;
}

function TAccordionDetails({ children, ...props }: AccordionDetailsProps): JSX.Element {
    const classes = useAccordionDetailsStyles();

    return <AccordionDetails {...props} classes={{
        root: classes.expansionSummary
    }}>{children}</AccordionDetails>;
}

function TCheckbox({ ...props }: CheckboxProps): JSX.Element {
    return <Checkbox {...props} />;
}

function TIcon({ children, ...props }: IconProps): JSX.Element {
    return <Icon {...props}>{children}</Icon>;
}

function FilterIcon({ children, ...props }: IconProps): JSX.Element {
    const classes = useFilterIconStyles();

    return <Icon {...props} className={classes.filterIcon}>{children}</Icon>;
}

function TKeyboardDatePicker({ children, ...props }: KeyboardDatePickerProps): JSX.Element {
    const classes = useKeyboardDatePickerStyles();

    return <KeyboardDatePicker {...props}
        className={classes.select}
        InputProps={{
            className: classes.underline
        }}>{children}</KeyboardDatePicker>;
}

function TTable({ children, ...props }: TableProps): JSX.Element {
    return <Table {...props}>{children}</Table>;
}

function TTableHead({ children, ...props }: TableHeadProps): JSX.Element {
    const classes = useTableHeadStyles();

    return <TableHead classes={{ root: classes.thead }} {...props}>{children}</TableHead>;
}

function TTableRow({ children, ...props }: TableRowProps): JSX.Element {
    return <TableRow {...props}>{children}</TableRow>;
}

function TTableBodyRow({ children, ...props }: TableRowProps): JSX.Element {
    const classes = useTableBodyRowStyles();

    return <TableRow classes={{ root: classes.row }} {...props}>{children}</TableRow>;
}

function TTableHeadCell({ children, ...props }: TableCellProps): JSX.Element {
    const classes = useTableHeadCellStyles();

    return <TableCell classes={{ root: classes.thead }} {...props}>{children}</TableCell>;
}

function TTableCell({ children, ...props }: TableCellProps): JSX.Element {
    const classes = useTableCellStyles();
    const tableCellCommon = {
        classes: { root: classes.tbody }
    };

    return <TableCell align="center" {...tableCellCommon} {...props}>{children}</TableCell>;
}

function TTableBody({ children, ...props }: TableBodyProps): JSX.Element {
    return <TableBody {...props}>{children}</TableBody>;
}

function TTableHeadCellCategory({ children, ...props }: TableCellProps): JSX.Element {
    const classes = useTableHeadCellCategoryStyles();

    return <TableCell classes={{ root: classes.thead }} {...props}>{children}</TableCell>;
}

function TTableCellCategory({ children, ...props }: TableCellProps): JSX.Element {
    const classes = useTableCellCategoryStyles();

    return <TableCell classes={{
        root: classes.tbody
    }} {...props}>{children}</TableCell>;
}

function TTableRowCategory({ children, ...props }: TableRowProps): JSX.Element {
    const classes = useTableRowCategoryStyles();

    return <TableRow classes={{
        selected: classes.rowSelected
    }} {...props}>{children}</TableRow>;
}

export {
    TList as List,
    TListItem as ListItem,
    TDescTextField as DescTextField,
    TTextField as TextField,
    TInputLabel as InputLabel,
    TListItemText as ListItemText,
    TButton as Button,
    TInput as Input,
    TSelect as Select,
    TAccordion as Accordion,
    TAccordionSummary as AccordionSummary,
    TAccordionDetails as AccordionDetails,
    calendarTheme,
    TCheckbox as Checkbox,
    TIcon as Icon,
    FilterIcon,
    TKeyboardDatePicker as KeyboardDatePicker,
    TTable as Table,
    TTableHead as TableHead,
    TTableRow as TableRow,
    TTableBodyRow as TableBodyRow,
    TTableCell as TableCell,
    TTableBody as TableBody,
    TTableHeadCell as TableHeadCell,
    TTableHeadCellCategory as TableHeadCellCategory,
    TTableCellCategory as TableCellCategory,
    TTableRowCategory as TableRowCategory
};
