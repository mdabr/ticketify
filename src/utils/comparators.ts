import { Ticket } from 'types/store/data';

export const tickets = {
    startedAsc(t1: Ticket, t2: Ticket): number {
        return new Date(t1.createdAt).getTime() - new Date(t2.createdAt).getTime();
    },
    startedDesc(t1: Ticket, t2: Ticket): number {
        return new Date(t2.createdAt).getTime() - new Date(t1.createdAt).getTime();
    },
    modifiedAsc(t1: Ticket, t2: Ticket): number {
        return new Date(t1.lastModifiedDate).getTime() - new Date(t2.lastModifiedDate).getTime();
    },
    modifiedDesc(t1: Ticket, t2: Ticket): number {
        return new Date(t2.lastModifiedDate).getTime() - new Date(t1.lastModifiedDate).getTime();
    }
};
