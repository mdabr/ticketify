import { USER_TYPE } from 'utils/constants';
import { AddCategoryBody, AddCompanyBody, AddNewTicketBody, AddSubcategoryBody, AddUserBody, EditCategoryBody, EditTicketBody, EditUserBody, ResetPasswordBody, SubmitLoginBody, SubmitRegisterBody } from 'types/utils/FormService';
import {
    LOGIN,
    TICKETS,
    TICKETS_ID,
    USERS,
    USERS_ID,
    CATEGORIES,
    SUBCATEGORIES,
    CATEGORIES_ID,
    COMPANIES,
    USERS_ROLE,
    TICKETS_COMMENTS,
    REGISTER,
    RESET_PASSWORD
} from './backend';
import { parseGet, replacePlaceholders } from './helpers';

const postForm = <T extends Record<string, any>>(endpoint: string) => (form: T) => fetch(endpoint, {
    body: JSON.stringify(form),
    headers: {
        'Content-Type': 'application/json'
    },
    method: 'POST'
});

const authorizedPost = <T extends Record<string, any>>(endpoint: string) => (form: T, token: string, placeholders?: Record<string, any>) => fetch(replacePlaceholders(endpoint, placeholders), {
    body: JSON.stringify(form),
    headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    },
    method: 'POST'
});
const authorizedGet = (endpoint: string) => (token: string, placeholders?: Record<string, any>, params?: Record<string, string>) => fetch(parseGet(endpoint, placeholders, params), {
    headers: {
        'Authorization': `Bearer ${token}`
    },
    method: 'GET'
});
const authorizedPut = <T extends Record<string, any>>(endpoint: string) => (form: T, token: string, placeholders?: Record<string, any>) => fetch(replacePlaceholders(endpoint, placeholders), {
    body: JSON.stringify(form),
    headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    },
    method: 'PUT'
});
const authorizedDelete = (endpoint: string) => (token: string, placeholders?: Record<string, string>) => fetch(replacePlaceholders(endpoint, placeholders), {
    headers: {
        'Authorization': `Bearer ${token}`
    },
    method: 'DELETE'
});

// Login
const submitLogin = postForm<SubmitLoginBody>(LOGIN);

// Register
const submitRegister = postForm<SubmitRegisterBody>(REGISTER);

// Reset password
const resetPassword = postForm<ResetPasswordBody>(RESET_PASSWORD);

// Users
const fetchUsers = authorizedGet(USERS);
const addUser = authorizedPost<AddUserBody>(USERS);
const editUser = authorizedPut<EditUserBody>(USERS);

// User - placeholder: {role}
const fetchUserWithRole = (role: USER_TYPE, token: string): Promise<Response> => authorizedGet(USERS_ROLE)(token, { role });

// Users - placeholder: {id}
const fetchMyself = (token: string): Promise<Response> => authorizedGet(USERS_ID)(token, { id: 'myself' });

// Categories
// To fetch categories for specific company add 'companyId' as param
const fetchCategories = authorizedGet(CATEGORIES);
const addCategory = authorizedPost<AddCategoryBody>(CATEGORIES);
const editCategory = authorizedPut<EditCategoryBody>(CATEGORIES);

// Categories - placeholder: {id}
const deleteCategory = (id: string | number, token: string): Promise<Response> => authorizedDelete(CATEGORIES_ID)(token, { id: `${id}` });

// Subcategories - placeholder: {id}
const addSubcategory = (categoryId: string | number, subcategory: AddSubcategoryBody, token: string): Promise<Response> => authorizedPost(SUBCATEGORIES)(subcategory, token, { id: categoryId });
const editSubcategory = (categoryId: string | number, subcategory: AddSubcategoryBody, token: string): Promise<Response> => authorizedPut(SUBCATEGORIES)(subcategory, token, { id: categoryId });

// Companies
const fetchCompanies = authorizedGet(COMPANIES);
const addCompany = authorizedPost<AddCompanyBody>(COMPANIES);
const editCompany = authorizedPut(COMPANIES);

// Tickets
const fetchTickets = authorizedGet(TICKETS);
const addNewTicket = authorizedPost<AddNewTicketBody>(TICKETS);
const editTicket = authorizedPut<EditTicketBody>(TICKETS);

// Tickets - placeholder: {id}
const fetchTicketDetails = (id: string | number, token: string): Promise<Response> => authorizedGet(TICKETS_ID)(token, { id });
const sendComment = (id: string | number, comment: string, token: string): Promise<Response> => authorizedPost(TICKETS_COMMENTS)({ comment }, token, { id });

export {
    submitLogin,

    submitRegister,

    resetPassword,

    fetchUsers,
    addUser,
    editUser,
    fetchMyself,
    fetchUserWithRole,

    fetchCategories,
    addCategory,
    editCategory,
    deleteCategory,

    addSubcategory,
    editSubcategory,

    fetchCompanies,
    addCompany,
    editCompany,

    fetchTickets,
    addNewTicket,
    editTicket,
    fetchTicketDetails,
    sendComment
};
