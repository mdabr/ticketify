const variables = {
    '$main-color': '#121212',
    '$main-color-lighter': '#191919',
    '$main-color-text': '#dcdcdc',
    '$brighter-color-text': '#b2b2b2',
    '$second-color': '#1f7a8c',
    '$second-color-lighter': '#279bb2',
    '$third-color': '#f3d3bd',
    '$third-color-lighter': '#ffc6a0'
};

export default variables;
