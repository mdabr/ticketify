import { equals, type } from 'ramda';
import { Action } from 'redux';

export function actionValidator<State, ActionTypes extends Action>(actions: Record<string, (_state: State, action: ActionTypes) => State>) {
    return (action: ActionTypes): boolean => equals(type(actions[action.type]), 'Function');
}

export function defaultReducer<ActionTypes extends Action, State>(actions: Record<string, (_state: State, action: ActionTypes) => State>, defaultState: State): (state: State, action: ActionTypes) => State {
    const isActionHandled = actionValidator(actions);

    return function reducer(state: State = defaultState, action: ActionTypes): State {
        return isActionHandled(action)
            ? actions[action.type](state, action)
            : state;
    };
}
