export type TOAST_LEVEL = 'SUCCESS' | 'DANGER' | 'WARNING' | 'INFO' | 'NONE';
export type USER_TYPE = 'EMPLOYEE' | 'SUPERVISOR' | 'TECHNICIAN' | 'ADMINISTRATOR' | 'NONE'

const USER_TYPES: Record<USER_TYPE, USER_TYPE> = Object.freeze({
    EMPLOYEE: 'EMPLOYEE',
    SUPERVISOR: 'SUPERVISOR',
    TECHNICIAN: 'TECHNICIAN',
    ADMINISTRATOR: 'ADMINISTRATOR',
    NONE: 'NONE'
});

const TOAST_LEVELS: Record<TOAST_LEVEL, TOAST_LEVEL> = Object.freeze({
    SUCCESS: 'SUCCESS',
    DANGER: 'DANGER',
    WARNING: 'WARNING',
    INFO: 'INFO',
    NONE: 'NONE'
});

const PRIORITIES: Record<string, string> = Object.freeze({
    HIGH: 'HIGH',
    MEDIUM: 'MEDIUM',
    LOW: 'LOW'
});

const STATUS: Record<string, string> = Object.freeze({
    UNASSIGNED: 'UNASSIGNED',
    ASSIGNED: 'ASSIGNED',
    IN_PROGRESS: 'IN_PROGRESS',
    DONE: 'DONE',
    CLOSED: 'CLOSED',
    COMPLETED: 'COMPLETED'
});

const CODES: Record<string, number> = Object.freeze({
    OK: 200,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    INTERNAL_ERROR: 500
});

export {
    USER_TYPES,
    TOAST_LEVELS,
    PRIORITIES,
    STATUS,
    CODES
};
