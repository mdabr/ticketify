const BASE_URL = '/';
const PRELOGIN: Record<string, string> = {
    BASE: '/prelogin',
    FORGOT: '/prelogin/forgot'
};
const MAIN_PAGE: Record<string, string> = {
    BASE: '/main-page'
};

export {
    BASE_URL,
    PRELOGIN,
    MAIN_PAGE
};
