import { UserInfo } from 'types/utils/user';

export function isUserValid(user?: UserInfo | null): boolean {
    return Boolean(user) &&
        Boolean((user as UserInfo).email) &&
        Boolean((user as UserInfo).firstName) &&
        Boolean((user as UserInfo).lastName) &&
        Boolean((user as UserInfo).company) &&
        Boolean((user as UserInfo).password);
}
