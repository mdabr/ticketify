import {
    createMuiTheme,
    makeStyles
} from '@material-ui/core';
import variables from 'utils/variables';
import FilterImage from 'assets/filter.svg';

const calendarTheme = createMuiTheme({
    palette: {
        primary: {
            main: variables['$main-color-lighter']
        }
    }
});

const useListStyles = makeStyles({
    list: {
        width: '15%',
        height: '100%',
        boxSizing: 'border-box',
        borderRight: '1px solid black'
    }
});

const useListItemStyles = makeStyles({
    listItem: {
        cursor: 'pointer'
    },
    listItemSelected: {
        fontWeight: 'bold'
    }
});

const useDescTextFieldStyles = makeStyles({
    select: {
        color: variables['$main-color-text'],
        marginTop: '10px',
        marginBottom: '10px',
        width: '95%',
        height: '100%'
    },
    textFieldLabel: {
        fontSize: '1rem',
        color: '#fff !important',
        fontWeight: 'bold',
        transform: 'scale(1)'
    },
    underline: {
        color: '#fff',

        '&:after': {
            borderColor: variables['$second-color']
        }
    }
});

const useTextFieldStyles = makeStyles({
    select: {
        color: variables['$main-color-text'],
        marginTop: '10px',
        marginBottom: '10px',
        width: '12vw'
    },
    textFieldLabel: {
        color: '#fff !important',
        fontWeight: 'bold',
        transform: 'scale(1)',
        fontSize: '0.9vw'
    },
    underline: {
        color: '#fff',

        '&:after': {
            borderColor: variables['$second-color']
        }
    }
});

const useInputLabelStyles = makeStyles({
    selectLabel: {
        color: '#fff',
        fontWeight: 'bold',
        cursor: 'pointer',
        fontSize: '0.9vw'
    }
});

const useInputStyles = makeStyles({
    underline: {
        color: '#fff',

        '&:after': {
            borderColor: variables['$second-color']
        }
    }
});

const useSelectStyles = makeStyles({
    select: {
        color: variables['$main-color-text'],
        marginTop: '10px',
        marginBottom: '10px',
        width: '12vw'
    },
    selectIcon: {
        color: 'rgba(255, 255, 255, 0.54)'
    }
});

const useAccordionStyles = makeStyles({
    expansionPanel: {
        background: 'transparent',
        padding: 0,
        boxShadow: 'none',

        '&:before': {
            height: 0
        }
    },
    expansionPanelExpanded: {
        margin: '0 !important'
    }
});

const useAccordionSummaryStyles = makeStyles({
    expansionSummary: {
        padding: 0
    },
    expansionPanelExpanded: {
        margin: '0 !important'
    },
    expandMore: {
        color: '#fff'
    }
});

const useAccordionDetailsStyles = makeStyles({
    expansionSummary: {
        padding: 0
    }
});

const useFilterIconStyles = makeStyles({
    filterIcon: {
        backgroundImage: `url('${FilterImage}')`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat'
    }
});

const useKeyboardDatePickerStyles = makeStyles({
    select: {
        color: variables['$main-color-text'],
        marginTop: '10px',
        marginBottom: '10px',
        width: '12vw'
    },
    underline: {
        color: '#fff',

        '&:after': {
            borderColor: variables['$second-color']
        }
    }
});

const useTableHeadStyles = makeStyles({
    thead: {
        fontWeight: 'bold',
        color: variables['$main-color-text'],
        fontSize: '2vmin'
    }
});

const useTableHeadCellStyles = makeStyles({
    thead: {
        fontWeight: 'bold',
        color: variables['$main-color-text'],
        fontSize: '2vmin'
    }
});

const useTableBodyRowStyles = makeStyles({
    row: {
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: 'rgba(220, 220, 220, 0.1)'
        }
    }
});

const useTableCellStyles = makeStyles({
    tbody: {
        color: variables['$main-color-text']
    }
});

const useTableHeadCellCategoryStyles = makeStyles({
    thead: {
        color: '#fff',
        fontSize: '0.875rem',
        fontWeight: 'bold',
        textAlign: 'center'
    }
});

const useTableCellCategoryStyles = makeStyles({
    tbody: {
        color: '#fff',
        fontSize: '0.775rem',
        textAlign: 'center',
        cursor: 'pointer'
    }
});

const useTableRowCategoryStyles = makeStyles({
    rowSelected: {
        backgroundColor: 'rgba(220, 220, 220, 0.08) !important'
    }
});

export {
    calendarTheme,
    useListStyles,
    useListItemStyles,
    useDescTextFieldStyles,
    useTextFieldStyles,
    useInputLabelStyles,
    useInputStyles,
    useSelectStyles,
    useAccordionStyles,
    useAccordionSummaryStyles,
    useAccordionDetailsStyles,
    useFilterIconStyles,
    useKeyboardDatePickerStyles,
    useTableHeadStyles,
    useTableHeadCellStyles,
    useTableBodyRowStyles,
    useTableCellStyles,
    useTableHeadCellCategoryStyles,
    useTableCellCategoryStyles,
    useTableRowCategoryStyles
};
