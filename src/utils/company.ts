import { Company } from 'types/store/user';
import { CompanyInfo } from 'types/utils/company';

export function isCompanyValid(company?: CompanyInfo | null): boolean {
    return Boolean(company) &&
        Boolean((company as Company).name) &&
        Boolean((company as Company).country) &&
        Boolean((company as Company).city) &&
        Boolean((company as Company).postalCode) &&
        Boolean((company as Company).premisesAddress) &&
        Boolean((company as Company).localIdCode);
}
