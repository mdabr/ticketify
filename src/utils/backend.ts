// remote
// const BASE_URL = 'http://krzyzakowsky.pl:8080/ticketify/api';
// local
const BASE_URL = 'http://localhost:8080';

// Login
const LOGIN = `${BASE_URL}/login`;

// Register
const REGISTER = `${BASE_URL}/tickets/newCompany`;

// Reset password
const RESET_PASSWORD = `${BASE_URL}/tickets/resetPassword`;

// Users
const USERS = `${BASE_URL}/users`;
const USERS_ID = `${USERS}/{id}`;
const USERS_ROLE = `${USERS}/role/{role}`;

// Category
const CATEGORIES = `${BASE_URL}/categories`;
const CATEGORIES_ID = `${CATEGORIES}/{id}`;

// Subcategory
const SUBCATEGORIES = `${CATEGORIES_ID}/subcategories`;

// Companies
const COMPANIES = `${BASE_URL}/companies`;

// Tickets
const TICKETS = `${BASE_URL}/tickets`;
const TICKETS_ID = `${TICKETS}/{id}`;
const TICKETS_COMMENTS = `${TICKETS_ID}/comments`;

export {
    LOGIN,
    REGISTER,
    RESET_PASSWORD,
    TICKETS,
    TICKETS_ID,
    TICKETS_COMMENTS,
    USERS,
    CATEGORIES,
    USERS_ID,
    USERS_ROLE,
    CATEGORIES_ID,
    COMPANIES,
    SUBCATEGORIES
};
