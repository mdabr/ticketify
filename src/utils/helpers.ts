import { Identifiable } from './../types/common';
import { User } from 'types/store/user';
import { Role } from 'types/utils/helpers';
import { USER_TYPE, USER_TYPES } from './constants';

const capitalizeFirst = (str: string): string => `${str.charAt(0).toUpperCase()}${str.slice(1).toLowerCase()}`;
const underscoreToSpace = (str: string): string => str.replace(/_/g, ' ');
const parseGet = (endpoint: string, placeholders?: Record<string, string | number>, params?: Record<string, string>): string => addParams(replacePlaceholders(endpoint, placeholders), params);
const getRole = (roles: Role[]): USER_TYPE => (roles.find(({ role }) => USER_TYPES[role] !== undefined) || roles[0]).role;
const replacePlaceholders = (endpoint: string, placeholders?: Record<string, string | number>): string => placeholders
    ? Object.keys(placeholders).reduce((formatted, placeholder) => formatted.replace(`{${placeholder}}`, `${placeholders[placeholder]}`), endpoint)
    : endpoint;
const addParams = (endpoint: string, params?: Record<string, string>): string => params
    ? Object.keys(params).reduce((formatted, param, index) => formatted.concat(`${index === 0 ? '?' : '&'}${param}=${params[param]}`), endpoint)
    : endpoint;
const isArrayEmpty = (array: any[]): boolean => !array || !array.length;
const isAuthorized = (authorizedRoles: string[], roles: Role[]): boolean => Boolean(roles.map(({ role }) => role).find(role => authorizedRoles.includes(role)));
const parseServerDate = (date: string | Date): string => new Date(date).toLocaleString();

function path(paths: string[], obj: Record<string, any> | null): any {
    return paths.reduce((acc, next) => acc === undefined || acc === null ? undefined : acc[next], obj);
}

function getFullname(personInfo: User | null): string {
    const firstName = path(['firstName'], personInfo);
    const lastName = path(['lastName'], personInfo);

    return firstName && lastName ? `${firstName} ${lastName}` : 'None';
}

function zeroHour(date: string | Date): Date {
    const newDate = new Date(date);

    newDate.setHours(0);
    newDate.setMinutes(0);
    newDate.setSeconds(0);

    return newDate;
}

function fullHour(date: string | Date): Date {
    const newDate = new Date(date);

    newDate.setHours(23);
    newDate.setMinutes(59);
    newDate.setSeconds(59);

    return newDate;
}

function isIdentifiableUnique<T extends Identifiable>(element: T, index: number, array: T[]): boolean {
    return !array.slice(0, index).find(c2 => c2.id === element.id);
}

export {
    capitalizeFirst,
    underscoreToSpace,
    parseGet,
    getRole,
    isArrayEmpty,
    replacePlaceholders,
    isAuthorized,
    getFullname,
    parseServerDate,
    path,
    zeroHour,
    fullHour,
    isIdentifiableUnique
};
