import { capitalizeFirst, getFullname, parseServerDate, getRole, path } from 'utils/helpers';
import {
    getName, findCategory
} from 'utils/ticket';
import { useSelector } from 'react-redux';
import {
    STATUS, USER_TYPES
} from 'utils/constants';
import {
    AssignTechnician,
    ResolveTicket,
    VerifySolution,
    AssignMe,
    StartProgress
} from './actions';
import { InfoProps } from 'types/components/TicketDetails';
import { Role } from 'types/utils/helpers';
import { Category } from 'types/store/data';
import { StoreState } from 'store';

export default function Info({ ticket, assignees }: InfoProps): JSX.Element {
    const categories = useSelector((state: StoreState) => state.data.categories.filter(category => path(['subcategories', 'length'], category) > 0));
    const user = useSelector((state: StoreState) => state.user);
    const { id, priority, title, status } = ticket;

    function chooseAction(status: string) {
        const actions = {
            [STATUS.UNASSIGNED]: getRole(user.roles as Role[]) === USER_TYPES.TECHNICIAN
                ? <AssignMe ticket={ticket} />
                : getRole(user.roles as Role[]) === USER_TYPES.ADMINISTRATOR
                    ? <AssignTechnician assignees={assignees} ticket={ticket} />
                    : null,
            [STATUS.ASSIGNED]: <StartProgress ticket={ticket} />,
            [STATUS.IN_PROGRESS]: <ResolveTicket ticket={ticket} />,
            [STATUS.DONE]: <VerifySolution ticket={ticket} />
        };

        return actions[status];
    }

    return <div className="ticket-info">
        <div className="ticket-info-details">
            <div className="ticket-id">ID: <b>{id}</b></div>
            <div className="ticket-priority">Priority: <b>{capitalizeFirst(priority)}</b></div>
            <div className="ticket-title">Title: <b>{title}</b></div>
            <div className="ticket-status">Status: <b>{capitalizeFirst(status)}</b></div>
            <div className="ticket-started">Started: <b>{parseServerDate(path(['createdAt'], ticket))}</b></div>
            <div className="ticket-last-modified">Last modified: <b>{parseServerDate(ticket.lastModifiedDate)}</b></div>
            <div className="ticket-started-by">Started by: <b>{getFullname(ticket.authorInfo)}</b></div>
            <div className="ticket-assigned-to">Assigned to: <b>{getFullname(ticket.technicianInfo)}</b></div>
            <div className="ticket-category">Category: <b>{getName(findCategory(ticket.categoryInfo, ticket.subcategoryInfo, categories) as Category)}</b></div>
            <div className="ticket-subcategory">Subcategory: <b>{getName(ticket.subcategoryInfo)}</b></div>
        </div>
        <div className="ticket-info-actions">
            {chooseAction(status)}
        </div>
    </div>;
}
