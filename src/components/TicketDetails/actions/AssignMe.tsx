import { useDispatch, useSelector } from 'react-redux';
import { changeAssignee } from 'store/actions';
import { StoreState } from 'store';
import { AssignMeProps } from 'types/components/TicketDetails';
import { User } from 'types/store/user';
import { USER_TYPES } from 'utils/constants';
import { getRole } from 'utils/helpers';
import { Button } from 'utils/uiComponents';

export default function AssignMe({ ticket }: AssignMeProps): JSX.Element {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);

    const assignMe = () => dispatch(changeAssignee(ticket, user as User));
    const isAuthorized = (user: User) => [USER_TYPES.TECHNICIAN].includes(getRole(user.roles));

    return <>
        {isAuthorized(user as User) ? <Button variant="contained" color="default" onClick={assignMe}>Assign me</Button> : null}
    </>;
}
