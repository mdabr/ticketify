import { equals, path, pipe } from 'ramda';
import { useDispatch, useSelector } from 'react-redux';
import { startProgress } from 'store/actions';
import { StoreState } from 'store';
import { StartProgressProps } from 'types/components/TicketDetails';
import { User } from 'types/store/user';
import { Button } from 'utils/uiComponents';

export default function StartProgress({ ticket }: StartProgressProps): JSX.Element | null {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);

    const onClick = pipe(
        () => ticket,
        startProgress,
        dispatch
    );
    const isAuthorized = (user: User) => equals(
        path<string>(['technicianInfo', 'id'], ticket),
        user.id
    );

    return isAuthorized(user as User)
        ? <Button variant="contained" color="default" onClick={onClick}>Start progress</Button>
        : null;
}
