import { getRole } from 'utils/helpers';
import { Select, InputLabel, ListItemText } from 'utils/uiComponents';
import { MenuItem } from '@material-ui/core';
import { AssignTechnicianProps } from 'types/components/TicketDetails';
import { User } from 'types/store/user';
import { useDispatch, useSelector } from 'react-redux';
import { changeAssignee } from 'store/actions';
import { Role } from 'types/utils/helpers';
import { USER_TYPES } from 'utils/constants';
import { StoreState } from 'store';
import { path, pipe } from 'ramda';
import { ChangeEvent } from 'react';

export default function AssignTechnician({ ticket, assignees }: AssignTechnicianProps): JSX.Element | null {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);

    const onChange = pipe(
        (event: ChangeEvent<{ value: unknown }>) => event.target.value as string,
        (userId: string) => assignees.find(assignee => assignee.id === userId) as User,
        (user: User) => changeAssignee(ticket, user),
        dispatch
    );
    const isAuthorized = (user: User) => [USER_TYPES.ADMINISTRATOR].includes(getRole(user.roles as Role[]));

    return isAuthorized(user as User)
        ? <>
            <InputLabel htmlFor="assignee-select">Assign technician: </InputLabel>
            <Select
                value={path(['technicianInfo', 'id'], ticket) || ''}
                onChange={onChange}
                inputProps={{
                    name: 'assignee-select',
                    id: 'assignee-select'
                }}
            >
                {assignees.map(assignee => <MenuItem key={assignee.id} value={assignee.id}>
                    <ListItemText primary={`${assignee.firstName} ${assignee.lastName}`} />
                </MenuItem>)}
            </Select>
        </>
        : null;
}
