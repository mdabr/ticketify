import { path, pipe } from 'ramda';
import { useDispatch, useSelector } from 'react-redux';
import { closeTicket, finishTicket } from 'store/actions';
import { StoreState } from 'store';
import { ResolveTicketProps } from 'types/components/TicketDetails';
import { User } from 'types/store/user';
import { Button } from 'utils/uiComponents';

export default function ResolveTicket({ ticket }: ResolveTicketProps): JSX.Element | null {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);

    const onSuccess = pipe(
        () => ticket,
        finishTicket,
        dispatch
    );
    const onClose = pipe(
        () => ticket,
        closeTicket,
        dispatch
    );
    const isAuthorized = (user: User) => path(['technicianInfo', 'id'], ticket) === user.id;

    return isAuthorized(user as User)
        ? <>
            <Button variant="contained" color="default" onClick={onSuccess}>Done</Button>
            <Button variant="contained" color="default" onClick={onClose}>Close</Button>
        </>
        : null;
}
