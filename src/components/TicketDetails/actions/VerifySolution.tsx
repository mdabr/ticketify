import { useDispatch, useSelector } from 'react-redux';
import { acceptSolution, rejectSolution } from 'store/actions';
import { StoreState } from 'store';
import { VerifySolutionProps } from 'types/components/TicketDetails';
import { User } from 'types/store/user';
import { Button } from 'utils/uiComponents';
import { pipe } from 'rxjs';
import { equals } from 'ramda';

export default function VerifySolution({ ticket }: VerifySolutionProps): JSX.Element | null {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);

    const onCorrect = pipe(
        () => ticket,
        acceptSolution,
        dispatch
    );
    const onIncorrect = pipe(
        () => ticket,
        rejectSolution,
        dispatch
    );
    const isAuthorized = (user: User) => equals(
        ticket.authorInfo.id,
        user.id
    );

    return isAuthorized(user as User)
        ? <>
            <Button variant="contained" color="default" onClick={onCorrect}>Accept</Button>
            <Button variant="contained" color="default" onClick={onIncorrect}>Reject</Button>
        </>
        : null;
}
