import { useState } from 'react';
import './TicketDetails.scss';
import { useDispatch } from 'react-redux';
import { hideTicketDetails, setEditedTicket } from 'store/actions';
import styled from 'styled-components';
import closeIcon from 'assets/close.svg';
import editIcon from 'assets/edit.svg';
import Info from './Info';
import Messages from './Messages';
import Description from './Description';
import { List, ListItem } from 'utils/uiComponents';
import { CloseButtonProps } from 'types/components/NewTicket';
import { EditButtonProps, TicketDetailsProps } from 'types/components/TicketDetails';

const CloseButton = styled.div<CloseButtonProps>`
    background-image: url(${props => props.image});
`;

const EditButton = styled.div<EditButtonProps>`
    background-image: url(${props => props.image});
`;

export default function TicketDetails({ ticket, assignees }: TicketDetailsProps): JSX.Element {
    const dispatch = useDispatch();
    const closeTicketDetails = () => {
        dispatch(hideTicketDetails());
    };
    const views: Record<string, JSX.Element> = {
        Info: <Info ticket={ticket} assignees={assignees} />,
        Description: <Description description={ticket.description}></Description>,
        Messages: <Messages ticket={ticket} />
    };
    const tabs = Object.keys(views);
    const [activeTab, setActiveTab] = useState(tabs[0]);
    const getActiveView = () => views[activeTab];

    return <>
        <div className="ticket-details-backdrop" onClick={closeTicketDetails}></div>
        <div className="ticket-details-container">
            <List>
                {tabs.map((tab, index) => <ListItem key={index} selected={activeTab === tab} onClick={() => setActiveTab(tab)}>{tab}</ListItem>)}
            </List>
            {getActiveView()}
            <EditButton image={editIcon} onClick={() => dispatch(setEditedTicket(ticket))} className="edit-button"></EditButton>
            <CloseButton image={closeIcon} onClick={closeTicketDetails} className="close-button"></CloseButton>
        </div>
    </>;
}
