import { DescriptionProps } from 'types/components/TicketDetails';

export default function Description({ description }: DescriptionProps): JSX.Element {
    return <div className="description-container">{description}</div>;
}
