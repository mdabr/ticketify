import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StoreState } from 'store';
import { sendMessage } from 'store/actions';
import { MessagesProps } from 'types/components/TicketDetails';
import { TextField } from 'utils/uiComponents';

export default function Messages({ ticket }: MessagesProps): JSX.Element {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);
    const [newMessage, setNewMessage] = useState('');

    useEffect(() => setNewMessage(''), [ticket]);

    const addComment = (message: string) => message && dispatch(sendMessage(message, ticket.id));

    return <div className="messages-container">
        <div className="messages">
            {ticket && ticket.comments.map(comment => <p key={comment.id} style={{ textAlign: comment.authorInfo.id === user.id ? 'right' : 'left' }}><b>{`${comment.authorInfo.firstName} ${comment.authorInfo.lastName}`}</b>: {comment.comment}</p>)}
        </div>
        <div className="new-message-container">
            <TextField
                label="New message"
                className="new-message"
                placeholder="Press 'enter' to send..."
                value={newMessage}
                onChange={event => setNewMessage(event.target.value)}
                onKeyDown={event => event.keyCode === 13 && addComment(newMessage)}
            />
        </div>
    </div>;
}
