import './Loader.scss';
import Spinner from 'react-loader-spinner';
import { useSelector } from 'react-redux';
import { StoreState } from 'store';

export default function Loader(): JSX.Element | null {
    const loaderVisible = useSelector((state: StoreState) => state.loader.loaderVisible);

    return loaderVisible ? <div className="loader-backdrop">
        <div className="loader-container">
            <Spinner type="Rings" color="#fff" height={300} width={300}></Spinner>
        </div>
    </div> : null;
}
