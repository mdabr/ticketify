import { FormEvent, useState } from 'react';
import './Login.scss';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { logIn } from 'store/actions';

export default function Login(): JSX.Element {
    const dispatch = useDispatch();
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    function onSubmit(event: FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        dispatch(logIn(login, password));
    }

    return <div className="login-container">
        <h1 className="login-title">Log in</h1>
        <form autoComplete="off" onSubmit={onSubmit}>
            <label htmlFor="LOGIN">
                <h3>Login</h3>
                <input id="LOGIN" name="LOGIN" type="email" value={login} onChange={event => setLogin(event.target.value)} required />
            </label>
            <label htmlFor="PASSWORD">
                <h3>Password</h3>
                <input id="PASSWORD" name="PASSWORD" type="password" autoComplete="password" value={password} onChange={event => setPassword(event.target.value)} required />
            </label>
            <input type="submit" value="Log in" />
            <span className="forgot-password">Forgot password? Click <Link to={'/prelogin/forgot'}><b>here</b></Link> to reset</span>
        </form>
    </div>;
}
