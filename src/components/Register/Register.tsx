import { FormEvent, useState } from 'react';
import './Register.scss';
import { useDispatch } from 'react-redux';
import { registerCompany } from 'store/actions';

export default function Register(): JSX.Element {
    const dispatch = useDispatch();
    const [company, setCompany] = useState('');
    const [contact, setContact] = useState('');

    function onSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();

        dispatch(registerCompany(company, contact));
    }

    return <div className="register-container">
        <h1 className="login-title">Register</h1>
        <form onSubmit={onSubmit}>
            <label htmlFor="COMPANY">
                <h3>Company</h3>
                <input id="COMPANY" name="COMPANY" type="text" autoComplete="company" value={company} onChange={event => setCompany(event.target.value)} required />
            </label>
            <label htmlFor="EMAIL">
                <h3>Contact email</h3>
                <input id="EMAIL" name="EMAIL" type="email" value={contact} onChange={event => setContact(event.target.value)} required />
            </label>
            <input type="submit" value="Register"></input>
        </form>
    </div>;
}
