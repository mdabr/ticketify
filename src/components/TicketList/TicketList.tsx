import './TicketList.scss';
import { useDispatch, useSelector } from 'react-redux';
import { capitalizeFirst, getFullname, parseServerDate } from 'utils/helpers';
import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    TableBodyRow,
    TableHeadCell
} from 'utils/uiComponents';
import {
    getName, findCategory
} from 'utils/ticket';
import {
    path
} from 'ramda';
import { Category, Ticket } from 'types/store/data';
import { StoreState } from 'store';
import { showTicketDetails } from 'store/actions';

const tabs = [
    'ID', 'Priority', 'Status', 'Company', 'Category', 'Subcategory', 'Title', 'Assigned to', 'Started by', 'Started', 'Last modified'
];

export default function TicketList(): JSX.Element {
    const dispatch = useDispatch();
    const tickets = useSelector((state: StoreState) => state.data.filteredTickets);
    const categories = useSelector((state: StoreState) => state.data.categories.filter(category => path(['subcategories', 'length'], category) as number > 0));

    function getCompanyName(ticket: Ticket): string {
        return path(['authorInfo', 'companyInfo', 'name'], ticket) || '---';
    }

    return <div className="ticket-list-container">
        <Table className="ticket-list">
            <TableHead>
                <TableRow>
                    {tabs.map((tab, index) => <TableHeadCell align="center" key={index}>{tab}</TableHeadCell>)}
                </TableRow>
            </TableHead>
            <TableBody>
                {tickets.sort((t1, t2) => t2.id - t1.id).map(ticket => <TableBodyRow key={ticket.id} onClick={() => dispatch(showTicketDetails(ticket.id, true))}>
                    <TableCell>{ticket.id}</TableCell>
                    <TableCell>{capitalizeFirst(ticket.priority)}</TableCell>
                    <TableCell>{capitalizeFirst(ticket.status)}</TableCell>
                    <TableCell>{getCompanyName(ticket)}</TableCell>
                    <TableCell>{getName(findCategory(ticket.categoryInfo, ticket.subcategoryInfo, categories) as Category)}</TableCell>
                    <TableCell>{getName(ticket.subcategoryInfo)}</TableCell>
                    <TableCell>{ticket.title}</TableCell>
                    <TableCell>{getFullname(ticket.technicianInfo)}</TableCell>
                    <TableCell>{getFullname(ticket.authorInfo)}</TableCell>
                    <TableCell>{parseServerDate(ticket.createdAt)}</TableCell>
                    <TableCell>{parseServerDate(ticket.lastModifiedDate)}</TableCell>
                </TableBodyRow>
                )}
            </TableBody>
        </Table>
    </div>;
}
