import { FormEvent, useState } from 'react';
import './SendReset.scss';
import { useDispatch } from 'react-redux';
import { Return } from 'components';
import ReturnUrl from 'assets/return-white.svg';
import { resetPassword } from 'store/actions';

export default function SendReset(): JSX.Element {
    const dispatch = useDispatch();
    const [login, setLogin] = useState('');

    function onSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();

        dispatch(resetPassword(login));
    }

    return <div className="send-reset">
        <h1 className="login-title">Reset password</h1>
        <Return to={'/prelogin'} image={ReturnUrl} width={4} height={4} />
        <form autoComplete="off" onSubmit={onSubmit}>
            <div>
                <p>Provide login for account you want to reset the password for :</p>
                <label htmlFor="LOGIN">
                    <h3>Login</h3>
                    <input id="LOGIN" name="LOGIN" type="email" value={login} onChange={event => setLogin(event.target.value)} required />
                </label>
            </div>
            <input type="submit" value="Reset password" />
        </form>
    </div>;
}
