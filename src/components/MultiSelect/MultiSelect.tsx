import { MenuItem } from '@material-ui/core';
import { useState } from 'react';
import { MultiSelectProps } from 'types/components/MultiSelect';
import { capitalizeFirst, underscoreToSpace } from 'utils/helpers';
import { Checkbox, ListItemText, Select } from 'utils/uiComponents';

export default function MultiSelect({ options, inputProps, onChange }: MultiSelectProps): JSX.Element {
    const [elements, setElements] = useState([] as string[]);

    return <Select
        value={elements}
        onChange={event => {
            const newElements = event.target.value as string[];

            setElements(newElements);

            if (onChange) {
                onChange(newElements);
            }
        }}
        inputProps={inputProps}
        renderValue={selected => (selected as string[]).map(value => underscoreToSpace(capitalizeFirst(value))).join(', ')}
        multiple
    >
        {options.map((option, index) => <MenuItem key={index} value={option}>
            <Checkbox checked={elements.indexOf(option) > -1} />
            <ListItemText primary={capitalizeFirst(option)} />
        </MenuItem>)}
    </Select>;
}
