import { InputProps } from '@material-ui/core';
import { MultiSelectConfig, MultiSelectProps } from 'types/components/MultiSelect';
import MultiSelect from './MultiSelect';

export default function ConfigurableMultiSelect(config: MultiSelectConfig): (props: MultiSelectProps) => JSX.Element {
    const inputProps: InputProps['inputProps'] | undefined = config.name ? { id: config.name, name: config.name } : undefined;

    return (props: MultiSelectProps) => <MultiSelect
        inputProps={inputProps} {...props}
    />;
}
