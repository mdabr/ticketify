import { useState } from 'react';
import { Modal } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import styled from 'styled-components';
import closeIcon from 'assets/close.svg';
import { useDispatch, useSelector } from 'react-redux';
import { capitalizeFirst, getRole } from 'utils/helpers';
import { USER_TYPE, USER_TYPES } from 'utils/constants';
import { TextField, InputLabel, ListItemText, Button, Select } from 'utils/uiComponents';
import { CloseButtonProps } from 'types/components/NewTicket';
import { ChosenUserModalProps } from 'types/components/UserDetails';
import { Role } from 'types/utils/helpers';
import { StoreState } from 'store';

const CloseButton = styled.div<CloseButtonProps>`
    background-image: url(${props => props.image});
`;

export default function ChosenUserModal({ user, onClose, onChange }: ChosenUserModalProps): JSX.Element {
    const dispatch = useDispatch();
    const token = useSelector((state: StoreState) => state.token.token);
    const [newFirstname, setNewFirstname] = useState((user || {}).firstName);
    const [newLastname, setNewLastname] = useState((user || {}).lastName);
    const [newEmail, setNewEmail] = useState((user || {}).email);
    const [newRole, setNewRole] = useState(getRole((user || { roles: [] }).roles));
    const [newStatus, setNewStatus] = useState((user || {}).enabled);

    const onSaveClicked = () => {
        if (newFirstname === (user || {}).firstName &&
            newLastname === (user || {}).lastName &&
            newEmail === (user || {}).email &&
            ((user || { roles: [] }).roles as Role[]).find(role => role.role === newRole) &&
            newStatus === (user || {}).enabled) {
            onClose();
            return;
        }

        dispatch({
            type: 'EDIT_USER',
            user: Object.assign(user, { firstName: newFirstname, lastName: newLastname, email: newEmail, roles: [{ role: newRole }, { role: 'USER' }], enabled: newStatus }),
            token,
            onSuccess: () => {
                onChange();
                onClose();
            }
        });
    };

    return <Modal open={(user || {}).email !== undefined} onClose={onClose}>
        <div className="user-modal-container">
            <div>
                <TextField
                    label="Firstname"
                    value={newFirstname}
                    onChange={event => setNewFirstname(event.target.value)}
                />
                <TextField
                    label="Lastname"
                    value={newLastname}
                    onChange={event => setNewLastname(event.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Email"
                    value={newEmail}
                    onChange={event => setNewEmail(event.target.value)}
                />

                <div>
                    <InputLabel htmlFor="role-select">Role</InputLabel>
                    <Select
                        value={newRole}
                        onChange={event => setNewRole(event.target.value as USER_TYPE)}
                        inputProps={{
                            name: 'role-select',
                            id: 'role-select'
                        }}
                        renderValue={value => capitalizeFirst(value as string)}
                    >
                        {Object.keys(USER_TYPES).filter(role => role !== USER_TYPES.NONE)
                            .map((role, index) => <MenuItem key={index} value={role}>
                                <ListItemText primary={capitalizeFirst(role)} />
                            </MenuItem>)}
                    </Select>
                </div>
            </div>
            <div className="buttons">
                <Button
                    variant="contained"
                    color="default"
                    onClick={() => setNewStatus(!newStatus)}
                >
                    <b>{newStatus ? 'Block' : 'Unblock'}</b>
                </Button>

                <Button
                    variant="contained"
                    color="default"
                    onClick={onSaveClicked}
                >
                    <b>Save</b>
                </Button>
            </div>
            <CloseButton image={closeIcon} onClick={onClose} className="close-button"></CloseButton>
        </div>
    </Modal>;
}
