import { useState } from 'react';
import { Modal } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { TextField, Button } from 'utils/uiComponents';
import { ChosenCategoryModalProps } from 'types/components/UserDetails';
import { StoreState } from 'store';

export default function ChosenCategoryModal({ open, category, subcategory, onClose, onSave, onCategoryEdit, onSubcategoryEdit }: ChosenCategoryModalProps): JSX.Element {
    const dispatch = useDispatch();
    const token = useSelector((state: StoreState) => state.token.token);
    const [newName, setNewName] = useState(subcategory ? subcategory.name : category.name);
    const [newStatus, setNewStatus] = useState(subcategory ? subcategory.enabled : category.enabled);

    function onSaveClick() {
        if (newName === (subcategory ? subcategory.name : category.name) && newStatus === (subcategory ? subcategory.enabled : category.enabled)) {
            onClose();
            return;
        }

        dispatch({
            type: subcategory ? 'EDIT_SUBCATEGORY' : 'EDIT_CATEGORY',
            id: subcategory ? subcategory.id : category.id,
            name: newName,
            enabled: newStatus,
            token,
            onSuccess: () => {
                if (subcategory) {
                    onSubcategoryEdit(Object.assign(subcategory, { name: newName, enabled: newStatus }));
                } else {
                    onCategoryEdit(Object.assign(category, { name: newName, enabled: newStatus }));
                }

                onSave();
                onClose();
            }
        });
    }

    return <Modal open={open} onClose={onClose}>
        <div className="category-modal-container">
            <div className="inputs">
                <TextField
                    label="Name"
                    value={newName}
                    onChange={event => setNewName(event.target.value)}
                />
                <Button
                    variant="contained"
                    color="default"
                    onClick={() => setNewStatus(!newStatus)}
                >
                    <b>{newStatus ? 'DEACTIVATE' : 'ACTIVATE'}</b>
                </Button>
            </div>
            <div className="buttons">
                <Button
                    variant="contained"
                    color="default"
                    onClick={onSaveClick}
                >
                    <b>Save</b>
                </Button>
                <Button
                    variant="contained"
                    color="default"
                    onClick={onClose}
                >
                    <b>Cancel</b>
                </Button>
            </div>
        </div>
    </Modal>;
}
