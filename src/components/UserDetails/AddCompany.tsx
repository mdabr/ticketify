import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TextField, Button } from 'utils/uiComponents';
import { AddCompanyProps } from 'types/components/UserDetails';
import { StoreState } from 'store';

export default function AddCompany({ onChange }: AddCompanyProps): JSX.Element {
    const dispatch = useDispatch();
    const token = useSelector((state: StoreState) => state.token.token);
    const [name, setName] = useState('');
    const [country, setCountry] = useState('');
    const [city, setCity] = useState('');
    const [postalCode, setPostalCode] = useState('');
    const [premisesAddress, setPremisesAddress] = useState('');
    const [localIdCode, setLocalIdCode] = useState('');

    function onAddCompanyClicked() {
        dispatch({
            type: 'ADD_COMPANY',
            name,
            country,
            city,
            postalCode,
            premisesAddress,
            localIdCode,
            token,
            onSuccess: () => onChange()
        });
    }

    return <div className="add-company-container">
        <div className="left">
            <TextField
                label="Name"
                autoComplete="new-name"
                value={name}
                onChange={event => setName(event.target.value)}
            />

            <TextField
                label="Country"
                autoComplete="new-country"
                value={country}
                onChange={event => setCountry(event.target.value)}
            />

            <TextField
                label="City"
                autoComplete="new-city"
                value={city}
                onChange={event => setCity(event.target.value)}
            />
        </div>

        <div className="right">
            <TextField
                label="Postal code"
                autoComplete="new-postal-code"
                value={postalCode}
                onChange={event => setPostalCode(event.target.value)}
            />

            <TextField
                label="Premises address"
                value={premisesAddress}
                autoComplete="new-premises-address"
                onChange={event => setPremisesAddress(event.target.value)}
            />

            <TextField
                label="Local ID code"
                autoComplete="new-local-id-code"
                value={localIdCode}
                onChange={event => setLocalIdCode(event.target.value)}
            />
        </div>

        <Button
            variant="contained"
            color="default"
            className="add-button"
            onClick={onAddCompanyClicked}
        >Add company</Button>
    </div>;
}
