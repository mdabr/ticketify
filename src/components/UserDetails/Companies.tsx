import { useState } from 'react';
import { Table, TableHead, TableRow, TableBody } from '@material-ui/core';
import ChosenCompanyModal from './ChosenCompanyModal';
import {
    TableHeadCellCategory,
    TableBodyRow,
    TableCellCategory
} from 'utils/uiComponents';
import { CompaniesProps } from 'types/components/UserDetails';
import { Company } from 'types/store/user';

const tabs = ['Name', 'Country', 'City', 'Status'];

export default function Companies({ companies, onChange }: CompaniesProps): JSX.Element {
    const [chosenCompany, setChosenCompany] = useState(null as Company | null);

    return <div className="companies-container">
        <Table>
            <TableHead>
                <TableRow>
                    {tabs.map((tab, index) => <TableHeadCellCategory key={index}>{tab}</TableHeadCellCategory>)}
                </TableRow>
            </TableHead>
            <TableBody>
                {companies.map((company, index) => <TableBodyRow key={index} onClick={() => {
                    setChosenCompany(company);
                }}>
                    <TableCellCategory>{company.name}</TableCellCategory>
                    <TableCellCategory>{company.country}</TableCellCategory>
                    <TableCellCategory>{company.city}</TableCellCategory>
                    <TableCellCategory>{company.enabled ? 'ACTIVE' : 'INACTIVE'}</TableCellCategory>
                </TableBodyRow>)}
            </TableBody>
        </Table>
        {chosenCompany && <ChosenCompanyModal company={chosenCompany} onClose={() => setChosenCompany(null)} onChange={onChange} />}
    </div>;
}
