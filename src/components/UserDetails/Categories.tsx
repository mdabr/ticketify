import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ChosenCategoryModal from './ChosenCategoryModal';
import { isAuthorized, path } from 'utils/helpers';
import { USER_TYPES } from 'utils/constants';
import { TextField, Button, Table, TableHead, TableRow, TableHeadCellCategory, TableCellCategory, TableBody, TableRowCategory } from 'utils/uiComponents';
import { Category, Subcategory } from 'types/store/data';
import { Role } from 'types/utils/helpers';
import { StoreState } from 'store';

const tabs = ['Category', 'Status'];
const subtabs = ['Subcategory', 'Status'];

export default function Categories(): JSX.Element {
    const dispatch = useDispatch();
    const categories = useSelector((state: StoreState) => state.data.categories);
    const token = useSelector((state: StoreState) => state.token.token);
    const user = useSelector((state: StoreState) => state.user);
    const [chosenCategory, setChosenCategory] = useState(null as Category | null);
    const [chosenSubcategory, setChosenSubcategory] = useState(null as Subcategory | null);
    const [newCategory, setNewCategory] = useState('');
    const [subcategories, setSubcategories] = useState([] as Subcategory[]);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const hasSubcategories = (category: Category | null): boolean => category !== null && category.subcategories && category.subcategories.length > 0;
    const onChange = () => {
        dispatch({
            type: 'FETCH_CATEGORIES',
            token,
            companyId: path(['companyInfo', 'id'], user),
            onSuccess: (categories: Category[]) => {
                const currentCategory = categories.find(category => chosenCategory && category.name === path(['name'], chosenCategory));

                setSubcategories(currentCategory ? currentCategory.subcategories : []);
            }
        });
    };
    const getSelected = () => {
        if (chosenCategory) {
            return chosenSubcategory ? `${path(['name'], chosenCategory)} -> ${path(['name'], chosenSubcategory)}` : path(['name'], chosenCategory);
        }
        return 'None';
    };

    function onAddCategoryClicked() {
        if (!newCategory) {
            return;
        }

        dispatch({
            type: chosenCategory ? 'ADD_SUBCATEGORY' : 'ADD_CATEGORY',
            name: newCategory,
            token,
            onSuccess: () => {
                onChange();
                setNewCategory('');
            }
        });
    }

    const onEditCategoryClicked = () => {
        setIsModalOpen(true);
    };
    const onDeleteCategory = () => {
        dispatch({
            type: 'DELETE_CATEGORY',
            token,
            categoryId: path(['id'], chosenCategory),
            onSuccess: () => {
                setChosenCategory(null);
                onChange();
            }
        });
    };

    return <div className="categories-container">
        <div className="tables-container">
            <div className="table-container">
                <Table>
                    <TableHead>
                        <TableRow>
                            {tabs.map((tab, index) => <TableHeadCellCategory key={index}>{tab}</TableHeadCellCategory>)}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {categories.map((category, index) => <TableRowCategory selected={chosenCategory === null ? undefined : path(['id'], chosenCategory) === category.id} key={index} onClick={() => {
                            setChosenSubcategory(null);
                            if (chosenCategory && path(['id'], chosenCategory) === category.id) {
                                setChosenCategory(null);
                                setSubcategories([]);
                            } else {
                                setChosenCategory(category);
                                setSubcategories(category.subcategories);
                            }
                        }}>
                            <TableCellCategory>{category.name}</TableCellCategory>
                            <TableCellCategory>{category.enabled ? 'ACTIVE' : 'INACTIVE'}</TableCellCategory>
                        </TableRowCategory>)}
                    </TableBody>
                </Table>
            </div>
            <div className="table-container">
                <Table>
                    <TableHead>
                        <TableRow>
                            {subtabs.map((tab, index) => <TableHeadCellCategory key={index}>{tab}</TableHeadCellCategory>)}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {subcategories.map((subcategory, index) => <TableRowCategory selected={chosenSubcategory === null ? undefined : path(['id'], chosenSubcategory) === subcategory.id} key={index} onClick={() => {
                            if (chosenSubcategory && path(['id'], chosenSubcategory) === subcategory.id) {
                                setChosenSubcategory(null);
                            } else {
                                setChosenSubcategory(subcategory);
                            }
                        }}>
                            <TableCellCategory>{subcategory.name}</TableCellCategory>
                            <TableCellCategory>{subcategory.enabled ? 'ACTIVE' : 'INACTIVE'}</TableCellCategory>
                        </TableRowCategory>)}
                    </TableBody>
                </Table>
            </div>
        </div>
        {isAuthorized([USER_TYPES.ADMINISTRATOR, USER_TYPES.SUPERVISOR, USER_TYPES.TECHNICIAN], user.roles as Role[]) ? <div className="categories-control">
            <div className="selected">Selected: {getSelected()}</div>

            <TextField
                label={chosenCategory ? 'Subcategory' : 'Category'}
                value={newCategory}
                onChange={event => setNewCategory(event.target.value)}
            />

            <Button
                variant="contained"
                color="default"
                className="add-button"
                onClick={onAddCategoryClicked}
            >Add {chosenCategory ? 'subcategory' : 'category'}</Button>

            <div className="separator"></div>

            <Button
                disabled={!chosenCategory}
                variant="contained"
                color="default"
                className="add-button"
                onClick={onEditCategoryClicked}
            >Edit {chosenSubcategory ? 'subcategory' : 'category'}</Button>

            <Button
                disabled={!chosenCategory || hasSubcategories(chosenCategory)}
                variant="contained"
                color="default"
                className="add-button"
                onClick={onDeleteCategory}
            >Delete category</Button>
        </div> : null}
        {isModalOpen ? <ChosenCategoryModal
            open={isModalOpen}
            category={chosenCategory as Category}
            onCategoryEdit={(category: Category) => setChosenCategory(category)}
            subcategory={chosenSubcategory as Subcategory}
            onSubcategoryEdit={(subcategory: Subcategory) => setChosenSubcategory(subcategory)}
            onSave={onChange}
            onClose={() => setIsModalOpen(false)}></ChosenCategoryModal> : null}
    </div>;
}
