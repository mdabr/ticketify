import { useState, useEffect } from 'react';
import './UserDetails.scss';
import { useDispatch, useSelector } from 'react-redux';
import { hideUserDetails, showDanger } from 'store/actions';
import styled from 'styled-components';
import closeIcon from 'assets/close.svg';
import Personal from './Personal';
import Users from './Users';
import AddUser from './AddUser';
import Categories from './Categories';
import { fetchUsers, fetchCompanies } from 'utils/FormService';
import Spinner from 'react-loader-spinner';
import Companies from './Companies';
import CompanyInfo from './CompanyInfo';
import AddCompany from './AddCompany';
import { USER_TYPES } from 'utils/constants';
import { getRole } from 'utils/helpers';
import { List, ListItem } from 'utils/uiComponents';
import { CloseButtonProps } from 'types/components/NewTicket';
import { Role } from 'types/utils/helpers';
import { Company, User } from 'types/store/user';
import { StoreState } from 'store';

const CloseButton = styled.div<CloseButtonProps>`
    background-image: url(${props => props.image});
`;

export default function UserDetails(): JSX.Element {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);
    const token = useSelector((state: StoreState) => state.token.token);
    const [users, setUsers] = useState([] as User[]);
    const [companies, setCompanies] = useState([]);
    const [isFetchingUsers, setIsFetchingUsers] = useState(true);
    const [isFetchingCompanies, setIsFetchingCompanies] = useState(true);

    const views: Record<string, JSX.Element> = {
        'Personal': <Personal user={user as User}></Personal>,
        'Company Info': <CompanyInfo companyInfo={user.companyInfo as Company}></CompanyInfo>,
        'Users': isFetchingUsers ? <div className="loader"><Spinner type="Rings" color="#fff" height={300} width={300}></Spinner></div> : <Users users={users} onChange={() => setIsFetchingUsers(true)} />,
        'Add user': <AddUser onChange={() => {
            setIsFetchingUsers(true);
            setActiveTab('Users');
        }} companies={companies} />,
        'Categories': <Categories></Categories>,
        'Companies': <Companies onChange={() => {
            setIsFetchingCompanies(true);
            setActiveTab('Companies');
        }} companies={companies}></Companies>,
        'Add Company': <AddCompany onChange={() => {
            setIsFetchingCompanies(true);
            setActiveTab('Companies');
        }}></AddCompany>
    };

    const authorities = {
        [USER_TYPES.ADMINISTRATOR]: Object.keys(views),
        [USER_TYPES.TECHNICIAN]: ['Personal', 'Company Info', 'Users', 'Categories'],
        [USER_TYPES.SUPERVISOR]: ['Personal', 'Company Info', 'Users', 'Categories'],
        [USER_TYPES.EMPLOYEE]: ['Personal', 'Company Info', 'Categories']
    };
    const tabs = authorities[getRole(user.roles as Role[])];
    const [activeTab, setActiveTab] = useState(tabs[0]);

    function closeUserDetails() {
        dispatch(hideUserDetails());
    }

    function getActiveView() {
        return views[activeTab];
    }

    useEffect(() => {
        if (isFetchingUsers) {
            fetchUsers(token)
                .then(response => response.json())
                .then((users: User[]) => Array.isArray(users) ? setUsers(users) : [])
                .catch(() => dispatch(showDanger({ header: 'Error!', message: 'Couldn\'t retrieve users data!' })))
                .then(() => setIsFetchingUsers(false));
        }
    });

    useEffect(() => {
        if (isFetchingCompanies) {
            fetchCompanies(token)
                .then(response => response.json())
                .then(companies => setCompanies(companies))
                .catch(() => dispatch(showDanger({ header: 'Error!', message: 'Couldn\'t retrieve companies\' data!' })))
                .then(() => setIsFetchingCompanies(false));
        }
    });

    return <>
        <div className="user-details-backdrop" onClick={closeUserDetails}></div>
        <div className="user-details-container">
            <List className="user-details-menu">
                {tabs.map((tab, index) => <ListItem key={index} selected={activeTab === tab} onClick={() => setActiveTab(tab)}>{tab}</ListItem>)}
            </List>
            {getActiveView()}
            <CloseButton image={closeIcon} onClick={closeUserDetails} className="close-button"></CloseButton>
        </div>
    </>;
}
