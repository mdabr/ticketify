import { useState } from 'react';
import { Table, TableHead, TableRow, TableBody } from '@material-ui/core';
import ChosenUserModal from './ChosenUserModal';
import { getRole, path } from 'utils/helpers';
import {
    TableBodyRow,
    TableHeadCellCategory,
    TableCellCategory
} from 'utils/uiComponents';
import { UsersProps } from 'types/components/UserDetails';
import { User } from 'types/store/user';
import { Role } from 'types/utils/helpers';

const tabs = ['Name', 'Email', 'Company', 'Role', 'Status'];

export default function Users({ users, onChange }: UsersProps): JSX.Element {
    const [chosenUser, setChosenUser] = useState(undefined as unknown as User);

    return <div className="users-container">
        <Table>
            <TableHead>
                <TableRow>
                    {tabs.map((tab, index) => <TableHeadCellCategory key={index}>{tab}</TableHeadCellCategory>)}
                </TableRow>
            </TableHead>
            <TableBody>
                {users.map((user, index) => <TableBodyRow key={index} onClick={() => {
                    setChosenUser(user);
                }}>
                    <TableCellCategory>{user.firstName} {user.lastName}</TableCellCategory>
                    <TableCellCategory>{user.email}</TableCellCategory>
                    <TableCellCategory>{path(['companyInfo', 'name'], user)}</TableCellCategory>
                    <TableCellCategory>{getRole(user.roles as Role[])}</TableCellCategory>
                    <TableCellCategory>{user.enabled ? 'ACTIVE' : 'INACTIVE'}</TableCellCategory>
                </TableBodyRow>)}
            </TableBody>
        </Table>
        {chosenUser && <ChosenUserModal user={chosenUser} onClose={() => setChosenUser(undefined as unknown as User)} onChange={onChange} />}
    </div>;
}
