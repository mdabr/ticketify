import { PersonalProps } from 'types/components/UserDetails';
import { Role } from 'types/utils/helpers';
import { getRole, path } from 'utils/helpers';

export default function Personal({ user }: PersonalProps): JSX.Element {
    return <div className="personal-container">
        <div className="firstname">First name: <b>{user.firstName || 'N/A'}</b></div>
        <div className="lastname">Last name: <b>{user.lastName || 'N/A'}</b></div>
        <div className="company">Company: <b>{path(['companyInfo', 'name'], user) || 'N/A'}</b></div>
        <div className="role">Role: <b>{getRole(user.roles as Role[]) || 'N/A'}</b></div>
        <div className="role">Email: <b>{user.email || 'N/A'}</b></div>
    </div>;
}
