import { useReducer } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { capitalizeFirst } from 'utils/helpers';
import { useDispatch, useSelector } from 'react-redux';
import { TextField, InputLabel, ListItemText, Button, Select } from 'utils/uiComponents';
import { StoreState } from 'store';
import { AddUserAction, AddUserProps, AddUserState } from 'types/components/UserDetails';

const addUserInitialState: AddUserState = {
    email: '',
    firstName: '',
    lastName: '',
    company: '',
    role: '',
    password: ''
};

function addUserReducer(state: AddUserState, action: AddUserAction): AddUserState {
    switch (action.type) {
        case 'setEmail': return { ...state, email: action.data as string };
        case 'setFirstName': return { ...state, firstName: action.data as string };
        case 'setLastName': return { ...state, lastName: action.data as string };
        case 'setCompany': return { ...state, company: action.data as string };
        case 'setPassword': return { ...state, password: action.data as string };
        case 'resetUser': return addUserInitialState;
        default: throw new Error();
    }
}

export default function AddUser({ onChange, companies }: AddUserProps): JSX.Element {
    const dispatch = useDispatch();
    const token = useSelector((state: StoreState) => state.token.token);
    const [state, dispatchLocal] = useReducer(addUserReducer, addUserInitialState);
    const { email, firstName, lastName, company, password } = state;

    function onAddUserClicked() {
        dispatch({
            type: 'ADD_USER',
            email,
            firstName,
            lastName,
            company,
            password,
            token,
            onSuccess: () => {
                onChange();
                dispatchLocal({ type: 'resetUser' });
            }
        });
    }

    return <div className="add-user-container">
        <div className="left">
            <TextField
                label="Email"
                autoComplete="new-email"
                value={email}
                onChange={event => dispatchLocal({ type: 'setEmail', data: event.target.value })}
            />
            <TextField
                label="First name"
                autoComplete="new-firstname"
                value={firstName}
                onChange={event => dispatchLocal({ type: 'setFirstName', data: event.target.value })}
            />
            <TextField
                label="Last name"
                autoComplete="new-lastname"
                value={lastName}
                onChange={event => dispatchLocal({ type: 'setLastName', data: event.target.value })}
            />
        </div>

        <div className="right">
            <div>
                <InputLabel htmlFor="company-select">Company</InputLabel>
                <Select
                    value={company}
                    onChange={event => dispatchLocal({ type: 'setCompany', data: event.target.value as string })}
                    inputProps={{
                        name: 'company-select',
                        id: 'company-select'
                    }}
                >
                    {companies.map((company, index) => <MenuItem key={index} value={company as any}>
                        <ListItemText primary={capitalizeFirst(company.name)} />
                    </MenuItem>)}
                </Select>
            </div>

            <TextField
                label="Password"
                value={password}
                type="password"
                autoComplete="new-password"
                onChange={event => dispatchLocal({ type: 'setPassword', data: event.target.value })}
            />
        </div>

        <Button
            variant="contained"
            color="default"
            className="add-button"
            onClick={onAddUserClicked}
        >Add user</Button>
    </div>;
}
