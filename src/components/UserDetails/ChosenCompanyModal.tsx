import { useState } from 'react';
import { Modal } from '@material-ui/core';
import styled from 'styled-components';
import closeIcon from 'assets/close.svg';
import { isNil } from 'ramda';
import { useDispatch, useSelector } from 'react-redux';
import { TextField, Button } from 'utils/uiComponents';
import { CloseButtonProps } from 'types/components/NewTicket';
import { ChosenCompanyModalProps } from 'types/components/UserDetails';
import { StoreState } from 'store';

const CloseButton = styled.div<CloseButtonProps>`
    background-image: url(${props => props.image});
`;

export default function ChosenCompanyModal({ company, onClose, onChange }: ChosenCompanyModalProps): JSX.Element {
    const dispatch = useDispatch();
    const token = useSelector((state: StoreState) => state.token.token);
    const [city, setCity] = useState(company.city);
    const [country, setCountry] = useState(company.country);
    const [localIdCode, setLocalIdCode] = useState(company.localIdCode);
    const [name, setName] = useState(company.name);
    const [postalCode, setPostalCode] = useState(company.postalCode);
    const [premisesAddress, setPremisesAddress] = useState(company.premisesAddress);
    const [status, setStatus] = useState(company.enabled);

    const onSaveClicked = () => {
        if (city === company.city &&
            country === company.country &&
            localIdCode === company.localIdCode &&
            postalCode === company.postalCode &&
            name === company.name &&
            premisesAddress === company.premisesAddress &&
            status === company.enabled) {
            return;
        }

        dispatch({
            type: 'EDIT_COMPANY',
            company: Object.assign(company, { city, country, localIdCode, name, postalCode, premisesAddress, enabled: status }),
            token,
            onSuccess: () => {
                onChange();
                onClose();
            }
        });
    };

    return <Modal open={!isNil(company)} onClose={onClose}>
        <div className="company-modal-container">
            <div className="inputs">
                <div className="left">
                    <TextField
                        label="Name"
                        value={name}
                        onChange={event => setName(event.target.value)}
                    />
                    <TextField
                        label="City"
                        value={city}
                        onChange={event => setCity(event.target.value)}
                    />
                    <TextField
                        label="Country"
                        value={country}
                        onChange={event => setCountry(event.target.value)}
                    />
                </div>
                <div className="right">
                    <TextField
                        label="Local ID code"
                        value={localIdCode}
                        onChange={event => setLocalIdCode(event.target.value)}
                    />
                    <TextField
                        label="Premises address"
                        value={premisesAddress}
                        onChange={event => setPremisesAddress(event.target.value)}
                    />
                    <TextField
                        label="Postal code"
                        value={postalCode}
                        onChange={event => setPostalCode(event.target.value)}
                    />
                </div>
            </div>
            <div className="buttons">
                <Button
                    variant="contained"
                    color="default"
                    onClick={() => setStatus(!status)}
                >
                    <b>{status ? 'Block' : 'Unblock'}</b>
                </Button>

                <Button
                    variant="contained"
                    color="default"
                    onClick={onSaveClicked}
                >
                    <b>Save</b>
                </Button>
            </div>
            <CloseButton image={closeIcon} onClick={onClose} className="close-button"></CloseButton>
        </div>
    </Modal>;
}
