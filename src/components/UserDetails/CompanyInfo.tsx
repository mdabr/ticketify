import { CompanyInfoProps } from 'types/components/UserDetails';

export default function CompanyInfo({ companyInfo }: CompanyInfoProps): JSX.Element {
    const { city, country, enabled, localIdCode, name, postalCode, premisesAddress } = companyInfo;

    return <div className="company-info-container">
        <div><b>City: </b>{city}</div>
        <div><b>Country: </b>{country}</div>
        <div><b>Status: </b>{enabled ? 'ACTIVE' : 'INACTIVE'}</div>
        <div><b>Local ID: </b>{localIdCode}</div>
        <div><b>Name: </b>{name}</div>
        <div><b>Postal code: </b>{postalCode}</div>
        <div><b>Address: </b>{premisesAddress}</div>
    </div>;
}
