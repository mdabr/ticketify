import './Search.scss';
import { PRIORITIES, STATUS } from 'utils/constants';
import { capitalizeFirst, zeroHour, fullHour, isIdentifiableUnique } from 'utils/helpers';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider
} from '@material-ui/pickers';
import MenuItem from '@material-ui/core/MenuItem';
import { ThemeProvider } from '@material-ui/styles';
import { useSearchReducer } from './SearchReducer';
import { useDispatch, useSelector } from 'react-redux';
import {
    TextField,
    InputLabel,
    ListItemText,
    Button,
    Select,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    calendarTheme,
    Checkbox,
    FilterIcon,
    KeyboardDatePicker
} from 'utils/uiComponents';
import { filterTickets } from 'store/actions';
import { isNil } from 'ramda';
import { Category, Subcategory } from 'types/store/data';
import { Company, User } from 'types/store/user';
import { StoreState } from 'store';
import { tickets as ticketComparators } from 'utils/comparators';
import ConfigurableMultiSelect from 'components/MultiSelect/ConfigurableMultiSelect';
import { useState } from 'react';

const PrioritiesMultiSelect = ConfigurableMultiSelect({ name: 'priorities-select' });
const StatusMultiSelect = ConfigurableMultiSelect({ name: 'status-select' });

export default function Search(): JSX.Element {
    const dispatch = useDispatch();
    const CATEGORIES = useSelector((state: StoreState) => state.data.categories);
    const TICKETS = useSelector((state: StoreState) => state.data.tickets);

    const initState: Record<string, Date> = {
        startedFrom: TICKETS.length > 0 ? zeroHour(TICKETS.sort(ticketComparators.startedAsc)[0].createdAt) : new Date(),
        startedTo: TICKETS.length > 0 ? fullHour(TICKETS.sort(ticketComparators.startedDesc)[0].createdAt) : new Date(),
        modifiedFrom: TICKETS.length > 0 ? zeroHour(TICKETS.sort(ticketComparators.modifiedAsc)[0].lastModifiedDate) : new Date(),
        modifiedTo: TICKETS.length > 0 ? fullHour(TICKETS.sort(ticketComparators.modifiedDesc)[0].lastModifiedDate) : new Date()
    };
    const [state, dispatchLocal] = useSearchReducer(initState);
    const { id, title, companies, categories, subcategories, assignees, creators, startedFrom, startedTo, modifiedFrom, modifiedTo } = state;
    const [ticketAssignees] = useState<User[]>(TICKETS.map(ticket => ticket.technicianInfo).filter(tech => !isNil(tech)) as User[]);
    const [ticketCreators] = useState(TICKETS.map(ticket => ticket.authorInfo).filter(tech => !isNil(tech)));
    const [ticketCompanies] = useState(TICKETS.map(ticket => ticket.authorInfo.companyInfo as Company).filter(isIdentifiableUnique));

    function onSubmit() {
        dispatch(filterTickets(state));
    }

    function idsToCompanies(ids: number[]) {
        console.log('idsToCompanies', ids);
        return ticketCompanies.filter(company => ids.find(id => id === company.id));
    }

    function idsToCategories(ids: number[]) {
        console.log('idsToCategories', ids);
        return CATEGORIES.filter(category => ids.find(id => id === category.id));
    }

    function idsToAssignees(ids: string[]) {
        return TICKETS.map(ticket => ticket.technicianInfo)
            .filter(technician => technician)
            .filter((user, index, array) => !array.slice(0, index).find(u2 => (u2 as User).id === (user as User).id))
            .filter(user => ids.find(id => id === (user as User).id));
    }

    function idsToCreators(ids: string[]) {
        return TICKETS.map(ticket => ticket.authorInfo)
            .filter((user, index, array) => !array.slice(0, index).find(u2 => u2.id === user.id))
            .filter(user => ids.find(id => id === user.id));
    }

    function showSubcategories() {
        return <>
            <InputLabel htmlFor="subcategories-select">Subcategories</InputLabel>
            <Select
                value={subcategories}
                onChange={event => dispatchLocal({ type: 'setSubcategories', data: event.target.value as Subcategory[] })}
                inputProps={{
                    name: 'subcategories-select',
                    id: 'subcategories-select'
                }}
                renderValue={selected => (selected as Subcategory[]).map(sub => sub.name)
                    .map(capitalizeFirst)
                    .join(', ')}
                multiple
            >
                {categories.flatMap(category => category.subcategories)
                    .map(({ name, id }, index) => <MenuItem key={index} value={name}>
                        <Checkbox checked={subcategories.find(subcategory => subcategory.id === id) !== undefined} />
                        <ListItemText primary={capitalizeFirst(name)} />
                    </MenuItem>)}
            </Select>
        </>;
    }

    return <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <ThemeProvider theme={calendarTheme}>
            <div className="search-container" onKeyDown={event => event.keyCode === 13 && onSubmit()}>
                <fieldset>
                    <legend>Filter</legend>

                    <div className="scrollable-container">
                        <TextField
                            label="ID"
                            value={id}
                            onChange={event => dispatchLocal({ type: 'setId', data: event.target.value })}
                        />

                        <TextField
                            label="Title"
                            value={title}
                            onChange={event => dispatchLocal({ type: 'setTitle', data: event.target.value })}
                        />

                        <InputLabel htmlFor="priorities-select">Priorities</InputLabel>
                        <PrioritiesMultiSelect
                            options={Object.keys(PRIORITIES)}
                            onChange={newPriorities => dispatchLocal({ type: 'setPriorities', data: newPriorities })}
                        />

                        <InputLabel htmlFor="status-select">Status</InputLabel>
                        <StatusMultiSelect
                            options={Object.keys(STATUS)}
                            onChange={newStatus => dispatchLocal({ type: 'setStatus', data: newStatus })}
                        />

                        <InputLabel htmlFor="companies-select">Companies</InputLabel>
                        <Select
                            value={companies}
                            onChange={event => dispatchLocal({ type: 'setCompanies', data: idsToCompanies(event.target.value as number[]) })}
                            inputProps={{
                                name: 'companies-select',
                                id: 'companies-select'
                            }}
                            renderValue={selected => (selected as Company[]).map(company => company.name).join(', ')}
                            multiple
                        >
                            {ticketCompanies.map((company, index) => <MenuItem key={index} value={company.id}>
                                <Checkbox checked={companies.find(c1 => c1.id === company.id) !== undefined} />
                                <ListItemText primary={company.name} />
                            </MenuItem>)}
                        </Select>

                        <InputLabel htmlFor="categories-select">Categories</InputLabel>
                        <Select
                            value={categories}
                            onChange={event => {
                                const newCategories = idsToCategories(event.target.value as number[]);
                                const newSubcategories = newCategories.flatMap(category => category.subcategories);

                                dispatchLocal({ type: 'setCategories', data: newCategories });
                                dispatchLocal({
                                    type: 'setSubcategories',
                                    data: subcategories.filter(
                                        sub => newSubcategories.find(subcategory => subcategory.id === sub.id)
                                    )
                                });
                            }}
                            inputProps={{
                                name: 'categories-select',
                                id: 'categories-select'
                            }}
                            renderValue={selected => (selected as Category[])
                                .map(sel => sel.name)
                                .map(capitalizeFirst)
                                .join(', ')}
                            multiple
                        >
                            {CATEGORIES.map((category, index) => <MenuItem key={index} value={category.id}>
                                <Checkbox checked={categories.map(cat => cat.name).indexOf(category.name) > -1} />
                                <ListItemText primary={capitalizeFirst(category.name || '')} />
                            </MenuItem>)}
                        </Select>

                        {categories.length ? showSubcategories() : null}

                        <InputLabel htmlFor="assignees-select">Assignees</InputLabel>
                        <Select
                            value={assignees}
                            onChange={event => dispatchLocal({ type: 'setAssignees', data: idsToAssignees(event.target.value as string[]) })}
                            inputProps={{
                                name: 'assignees-select',
                                id: 'assignees-select'
                            }}
                            renderValue={selected => (selected as User[]).map(assignee => `${assignee.firstName} ${assignee.lastName}`).join(', ')}
                            multiple
                        >
                            {ticketAssignees.map((assignee, index) => <MenuItem key={index} value={assignee.id as string}>
                                <Checkbox checked={assignees.find(a2 => a2.id === assignee.id) !== undefined} />
                                <ListItemText primary={`${assignee.firstName} ${assignee.lastName}`} />
                            </MenuItem>)}
                        </Select>

                        <InputLabel htmlFor="creators-select">Creators</InputLabel>
                        <Select
                            value={creators}
                            onChange={event => dispatchLocal({ type: 'setCreators', data: idsToCreators(event.target.value as string[]) })}
                            inputProps={{
                                name: 'creators-select',
                                id: 'creators-select'
                            }}
                            renderValue={selected => (selected as User[]).map(creator => `${creator.firstName} ${creator.lastName}`).join(', ')}
                            multiple
                        >
                            {ticketCreators.map((creator, index) => <MenuItem key={index} value={creator.id as string}>
                                <Checkbox checked={creators.find(c2 => c2.id === creator.id) !== undefined} />
                                <ListItemText primary={`${creator.firstName} ${creator.lastName}`} />
                            </MenuItem>)}
                        </Select>

                        <Accordion>
                            <AccordionSummary>
                                <InputLabel htmlFor="started-picker">Started between</InputLabel>
                            </AccordionSummary>

                            <AccordionDetails>
                                <div>
                                    <KeyboardDatePicker
                                        value={startedFrom}
                                        onChange={value => dispatchLocal({ type: 'setStartedFrom', data: zeroHour(new Date(value as Date)) })}
                                    />

                                    <KeyboardDatePicker
                                        value={startedTo}
                                        onChange={value => dispatchLocal({ type: 'setStartedTo', data: fullHour(new Date(value as Date)) })}
                                    />
                                </div>
                            </AccordionDetails>
                        </Accordion>

                        <Accordion>
                            <AccordionSummary>
                                <InputLabel htmlFor="modified-picker">Modified between</InputLabel>
                            </AccordionSummary>

                            <AccordionDetails>
                                <div>
                                    <KeyboardDatePicker
                                        value={modifiedFrom}
                                        onChange={value => dispatchLocal({ type: 'setModifiedFrom', data: zeroHour(new Date(value as Date)) })}
                                    />

                                    <KeyboardDatePicker
                                        value={modifiedTo}
                                        onChange={value => dispatchLocal({ type: 'setModifiedTo', data: fullHour(new Date(value as Date)) })}
                                    />
                                </div>
                            </AccordionDetails>
                        </Accordion>
                    </div>

                    <Button
                        className="filter-button"
                        variant="contained"
                        color="default"
                        onClick={onSubmit}
                    >
                        <FilterIcon/>
                        <b>Filter</b>
                    </Button>
                </fieldset>
            </div>
        </ThemeProvider>
    </MuiPickersUtilsProvider>;
}
