import { isNil } from 'ramda';
import { Dispatch, ReducerAction, ReducerState, useReducer } from 'react';
import { SearchAction, SearchState } from 'types/components/Search';
import { Category, Subcategory } from 'types/store/data';
import { Company, User } from 'types/store/user';
import { fullHour, zeroHour } from 'utils/helpers';

const searchState: SearchState = {
    id: '',
    title: '',
    priorities: [],
    status: [],
    companies: [],
    categories: [],
    subcategories: [],
    assignees: [],
    creators: [],
    startedFrom: new Date(),
    startedTo: new Date(),
    modifiedFrom: new Date(),
    modifiedTo: new Date()
};

const updateStartedFrom = (state: SearchState, startedFrom: Date) => ({
    ...state,
    startedFrom,
    startedTo: fullHour(new Date(Math.max(state.startedTo.getTime(), startedFrom.getTime())))
});

const updateStartedTo = (state: SearchState, startedTo: Date) => ({
    ...state,
    startedTo,
    startedFrom: zeroHour(new Date(Math.min(state.startedFrom.getTime(), startedTo.getTime())))
});

const updateModifiedFrom = (state: SearchState, modifiedFrom: Date) => ({
    ...state,
    modifiedFrom,
    modifiedTo: fullHour(new Date(Math.max(state.modifiedTo.getTime(), modifiedFrom.getTime())))
});

const updateModifiedTo = (state: SearchState, modifiedTo: Date) => ({
    ...state,
    modifiedTo,
    modifiedFrom: zeroHour(new Date(Math.min(state.modifiedFrom.getTime(), modifiedTo.getTime())))
});

function searchReducer(state: SearchState, action: SearchAction): SearchState {
    switch (action.type) {
        case 'setId': return { ...state, id: action.data as string };
        case 'setTitle': return { ...state, title: action.data as string };
        case 'setPriorities': return { ...state, priorities: action.data as string[] };
        case 'setStatus': return { ...state, status: action.data as string[] };
        case 'setCompanies': return { ...state, companies: action.data as Company[] };
        case 'setCategories': return { ...state, categories: action.data as Category[] };
        case 'setSubcategories': return { ...state, subcategories: action.data as Subcategory[] };
        case 'setAssignees': return { ...state, assignees: action.data as User[] };
        case 'setCreators': return { ...state, creators: action.data as User[] };
        case 'setStartedFrom': return updateStartedFrom(state, action.data as Date);
        case 'setStartedTo': return updateStartedTo(state, action.data as Date);
        case 'setModifiedFrom': return updateModifiedFrom(state, action.data as Date);
        case 'setModifiedTo': return updateModifiedTo(state, action.data as Date);
        default: throw new Error();
    }
}

function useSearchReducer(initialState?: Record<string | number, any>): [ReducerState<(state: SearchState, action: SearchAction) => SearchState>, Dispatch<ReducerAction<(state: SearchState, action: SearchAction) => SearchState>>] {
    const state = initialState
        ? Object.keys(initialState)
            .reduce((acc, next) => isNil((acc as Record<string, any>)[next])
                ? acc
                : Object.assign(acc, initialState[next]), searchState)
        : searchState;

    return useReducer(searchReducer, state);
}

export {
    searchReducer,
    searchState,
    useSearchReducer
};
