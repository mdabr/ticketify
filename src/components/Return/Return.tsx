import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ReturnContainerProps, ReturnProps } from 'types/components/Return';

const ReturnContainer = styled.div<ReturnContainerProps>`
    background-image: url(${props => props.image});
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    width: ${props => props.width}vmin;
    height: ${props => props.height}vmin;
`;

export default function Return({ image, to, width = 2, height = 2 }: ReturnProps): JSX.Element {
    return <Link to={to}>
        <ReturnContainer className="go-back" image={image} width={width} height={height} />
    </Link>;
}
