import { ToastsListProps } from 'types/components/ToastsList';
import Toast from './Toast/Toast';

export default function ToastsList({ toasts }: ToastsListProps): JSX.Element {
    return <>
        {toasts.map((toast, index) => <Toast key={index} {...toast} />)}
    </>;
}
