import { useEffect, useCallback } from 'react';
import { TOAST_LEVELS } from 'utils/constants';
import { removeToast } from 'store/actions';
import './Toast.scss';
import { useDispatch } from 'react-redux';
import { ToastProps } from 'types/components/Toast';

const toastToClass: Record<string, string> = {
    [TOAST_LEVELS.SUCCESS]: 'success',
    [TOAST_LEVELS.DANGER]: 'danger',
    [TOAST_LEVELS.WARNING]: 'warning',
    [TOAST_LEVELS.INFO]: 'info',
    [TOAST_LEVELS.NONE]: 'none'
};

export default function Toast({ id, header, message, level, time }: ToastProps): JSX.Element {
    const dispatch = useDispatch();
    const memoizedCallback = useCallback(
        () => {
            setTimeout(() => {
                dispatch(removeToast(id));
            }, time);
        },
        [message, id, time, dispatch]
    );

    useEffect(memoizedCallback);

    return <div className={`toast ${toastToClass[level]}`}>
        <h4>{header}</h4>
        <span>{message}</span>
    </div>;
}
