import { useSelector } from 'react-redux';
import './Infobox.scss';
import { StoreState } from 'store';
import ToastsList from './ToastsList/ToastsList';

export default function Infobox(): JSX.Element {
    const toasts = useSelector((state: StoreState) => state.toasts);

    return <div className="toasts-container">
        <ToastsList toasts={toasts} />
    </div>;
}
