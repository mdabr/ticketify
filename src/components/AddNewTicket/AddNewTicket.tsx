import styled from 'styled-components';
import TicketIcon from 'assets/new-ticket.svg';
import { useDispatch } from 'react-redux';
import { showNewTicket } from 'store/actions';
import { NewTicketIconProps } from 'types/components/AddNewTicket';

const NewTicketIcon = styled.div<NewTicketIconProps>`
    background-image: url(${props => props.image})
`;

export default function AddNewTicket(): JSX.Element {
    const dispatch = useDispatch();
    const onClick = () => dispatch(showNewTicket());

    return <NewTicketIcon className="add-new-ticket" image={TicketIcon} onClick={onClick} />;
}
