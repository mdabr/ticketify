import './UserInfo.scss';
import UserAvatar from 'assets/user-avatar.svg';
import LogOut from 'assets/log-out.svg';
import styled from 'styled-components';
import { CustomIconProps, UserInfoProps } from 'types/components/UserInfo';

const Icon = styled.div<CustomIconProps>`
    background-image: url(${({ image }) => image});
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    height: 90%;
    width: 15%;
    margin-right: 5px;
    margin-left: 2px;

    &:hover {
        border-bottom: 1px solid #666;
    }
`;

export default function UserInfo({ user, logOut, showUserDetails }: UserInfoProps): JSX.Element {
    return <div className="user-info-container">
        <div className="labeled-icon">
            <div className="label">{user.firstName}</div>
            <Icon onClick={showUserDetails} image={UserAvatar}></Icon>
            <Icon onClick={logOut} image={LogOut}></Icon>
        </div>
    </div>;
}
