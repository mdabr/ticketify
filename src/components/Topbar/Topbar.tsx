import './Topbar.scss';
import styled from 'styled-components';
import { UserInfo } from 'components';
import { useDispatch, useSelector } from 'react-redux';
import { StoreState } from 'store';
import { resetUser, showUserDetails } from 'store/actions';
import { User } from 'types/store/user';

const AppTitle = styled.h1`
    font-size: 6vmin;
    text-shadow: 0px 1px 4px #000;
    margin: 0;
`;

export default function Topbar(): JSX.Element {
    const dispatch = useDispatch();
    const user = useSelector((state: StoreState) => state.user);

    return <div className="topbar">
        <AppTitle>Ticketify</AppTitle>
        <UserInfo
            user={user as User}
            logOut={() => dispatch(resetUser())}
            showUserDetails={() => dispatch(showUserDetails())}
        />
    </div>;
}
