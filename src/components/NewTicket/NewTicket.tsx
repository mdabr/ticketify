import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNewTicket, editTicket, hideNewTicket } from 'store/actions';
import styled from 'styled-components';
import closeIcon from 'assets/close.svg';
import { List, ListItem } from 'utils/uiComponents';
import NewTicketMain from './NewTicketMain';
import NewTicketDescription from './NewTicketDescription';
import './NewTicket.scss';
import { PRIORITIES } from 'utils/constants';
import { findCategory } from 'utils/ticket';
import { path } from 'utils/helpers';
import { CloseButtonProps } from 'types/components/NewTicket';
import { Category } from 'types/store/data';
import { StoreState } from 'store';
import { User } from 'types/store/user';

const CloseButton = styled.div<CloseButtonProps>`
    background-image: url(${props => props.image});
`;

export default function NewTicket(): JSX.Element {
    const dispatch = useDispatch();
    const categories = useSelector((state: StoreState) => state.data.categories.filter(category => path(['subcategories', 'length'], category) > 0));
    const user = useSelector((state: StoreState) => state.user);
    const ticket = useSelector((state: StoreState) => state.newticket.editedTicket);

    const [title, setTitle] = useState(ticket ? ticket.title : '');
    const [priority, setPriority] = useState(ticket ? ticket.priority : PRIORITIES.MEDIUM);
    const [category, setCategory] = useState(ticket ? ticket.categoryInfo : categories[0]);
    const [subcategory, setSubcategory] = useState(ticket ? ticket.subcategoryInfo : path(['0', 'subcategories', '0'], categories) || null);
    const [description, setDescription] = useState(ticket ? ticket.description : '');
    const views: Record<string, JSX.Element> = {
        Main: <NewTicketMain stateHandlers={{
            title,
            setTitle,
            priority,
            setPriority,
            category: findCategory(category, subcategory, categories) as Category,
            setCategory,
            subcategory,
            setSubcategory
        }} categories={categories} onSubmit={onSubmit} editing={Boolean(ticket)} />,
        Description: <NewTicketDescription description={description} setDescription={setDescription} />
    };
    const tabs = Object.keys(views);
    const [activeTab, setActiveTab] = useState(tabs[0]);

    function getActiveView() {
        return views[activeTab];
    }

    function closeNewTicket() {
        dispatch(hideNewTicket());
    }

    function onSubmit() {
        if (ticket) {
            dispatch(editTicket(
                { ...ticket, title, priority, categoryInfo: category, subcategoryInfo: subcategory, description }
            ));
        } else {
            dispatch(addNewTicket(
                { title, priority, categoryInfo: category, subcategoryInfo: subcategory, description, authorInfo: user as User, createdAt: new Date(), companyInfo: { id: path(['companyInfo', 'id'], user) } }
            ));
        }
    }

    function tabMapper(tab: string, index: number) {
        return <ListItem key={index} selected={activeTab === tab} onClick={() => setActiveTab(tab)}>{tab}</ListItem>;
    }

    return <>
        <div className="new-ticket-backdrop" onClick={closeNewTicket}></div>
        <div className="new-ticket-container">
            <List className="new-ticket-menu">
                {tabs.map(tabMapper)}
            </List>
            {getActiveView()}
            <CloseButton image={closeIcon} onClick={closeNewTicket} className="close-button"></CloseButton>
        </div>
    </>;
}
