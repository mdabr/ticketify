import MenuItem from '@material-ui/core/MenuItem';
import { PRIORITIES } from 'utils/constants';
import { underscoreToSpace, capitalizeFirst } from 'utils/helpers';
import { TextField, InputLabel, ListItemText, Button, Select } from 'utils/uiComponents';
import { NewTicketMainProps } from 'types/components/NewTicket';
import { Category, Subcategory } from 'types/store/data';

export default function NewTicketMain({ stateHandlers, categories, onSubmit, editing }: NewTicketMainProps): JSX.Element {
    const {
        title,
        setTitle,
        priority,
        setPriority,
        category,
        setCategory,
        subcategory,
        setSubcategory
    } = stateHandlers;

    function mapIdToCategory(id: number) {
        return categories.find(category => category.id === id);
    }

    function mapIdToSubcategory(id: number) {
        return categories.flatMap(category => category.subcategories).find(subcategory => subcategory.id === id);
    }

    return <div className="new-ticket-main">
        <div className="inputs">
            <div className="left">
                <TextField
                    label={'Title'}
                    value={title}
                    onChange={event => setTitle(event.target.value)}
                />

                <div>
                    <InputLabel htmlFor="priority-select">Priority</InputLabel>
                    <Select
                        value={priority}
                        onChange={event => setPriority(event.target.value as string)}
                        inputProps={{
                            name: 'priority-select',
                            id: 'priority-select'
                        }}
                        renderValue={value => underscoreToSpace(capitalizeFirst(PRIORITIES[value as string]))}
                    >
                        {Object.keys(PRIORITIES).map((priority, index) => <MenuItem key={index} value={priority}>
                            <ListItemText primary={underscoreToSpace(capitalizeFirst(priority))} />
                        </MenuItem>)}
                    </Select>
                </div>
            </div>

            <div className="right">
                <div>
                    <InputLabel htmlFor="category-select">Category</InputLabel>
                    <Select
                        value={category}
                        onChange={event => setCategory(mapIdToCategory(parseInt(event.target.value as string, 10)) as Category)}
                        inputProps={{
                            name: 'category-select',
                            id: 'category-select'
                        }}
                        renderValue={value => underscoreToSpace(capitalizeFirst((value as Category).name))}
                    >
                        {categories.map((category, index) => <MenuItem
                            key={index}
                            value={category.id}
                            onClick={() => setSubcategory(category.subcategories[0])}
                        >
                            <ListItemText primary={underscoreToSpace(capitalizeFirst(category.name))} />
                        </MenuItem>)}
                    </Select>
                </div>

                <div>
                    <InputLabel htmlFor="subcategory-select">Subcategory</InputLabel>
                    <Select
                        value={subcategory}
                        onChange={event => setSubcategory(mapIdToSubcategory(parseInt(event.target.value as string, 10)) as Subcategory)}
                        inputProps={{
                            name: 'subcategory-select',
                            id: 'subcategory-select'
                        }}
                        renderValue={value => underscoreToSpace(capitalizeFirst((value as Subcategory).name))}
                    >
                        {(category ? category.subcategories : []).map((subcategory, index) => <MenuItem key={index} value={subcategory.id}>
                            <ListItemText primary={underscoreToSpace(capitalizeFirst(subcategory.name))} />
                        </MenuItem>)}
                    </Select>
                </div>
            </div>
        </div>

        <Button
            variant="contained"
            color="default"
            onClick={onSubmit}
        >{editing ? 'Save' : 'Submit'}</Button>
    </div>;
}
