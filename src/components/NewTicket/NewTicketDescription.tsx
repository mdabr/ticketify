import React from 'react';
import { NewTicketDescriptionProps } from 'types/components/NewTicket';
import { DescTextField } from 'utils/uiComponents';

export default function NewTicketDescription({ description, setDescription }: NewTicketDescriptionProps): JSX.Element {
    function onChange(event: React.ChangeEvent<{ value: string }>) {
        const MAX_LENGTH = 200;
        const { value } = event.target;

        if (value.length > MAX_LENGTH) {
            event.target.value = event.target.value.slice(0, MAX_LENGTH);
        }

        setDescription(value.length > MAX_LENGTH ? value.slice(0, MAX_LENGTH) : value);
    }

    return <div className="new-ticket-description">
        <DescTextField
            placeholder="Description"
            value={description}
            multiline={true}
            rowsMax={20}
            onChange={onChange}
        />
    </div>;
}
