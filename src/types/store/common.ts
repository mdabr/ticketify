import * as FormService from 'utils/FormService';

export interface Dependencies {
    FormService: typeof FormService
}
