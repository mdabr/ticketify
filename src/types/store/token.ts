import { UpdateTokenAction } from './actions';

export type Token = {
    expires: Date
    token: string
}

export type TokenState = {
    expires: Date
    token: string
}

export type TokenActions = UpdateTokenAction
