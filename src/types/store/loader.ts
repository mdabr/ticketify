import { HideLoaderAction, ShowLoaderAction } from './actions';

export type LoaderState = {
    loaderVisible: boolean
}

export type LoaderActions = ShowLoaderAction
                        | HideLoaderAction;
