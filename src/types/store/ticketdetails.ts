import { HideTicketDetailsAction, SetTicketDetailsAction } from './actions';
import { Ticket } from './data';
import { User } from './user';

export type TicketDetailsState = {
    ticketDetailsVisible: boolean
    ticket: Ticket | null
    assignees: User[]
}

export type TicketDetailsActions = SetTicketDetailsAction
                                | HideTicketDetailsAction
