import { HideUserDetailsAction, ShowUserDetailsAction } from './actions';

export type UserDetailsState = {
    userDetailsVisible: boolean
}

export type UserDetailsActions = ShowUserDetailsAction
                                | HideUserDetailsAction
