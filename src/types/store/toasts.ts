import { AddToastAction, RemoveToastAction } from './actions';

export type ToastsState = Toast[]

export interface Toast {
    id: string
    message: string
    header: string
    level: string
    time: number
}

export type ToastInfo = {
    header: string
    message: string
    level: string
    time?: number
}

export type ToastsActions = AddToastAction
                        | RemoveToastAction
