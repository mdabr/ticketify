import { Company, User } from 'types/store/user';
import { Identifiable } from '../common';
import { FilterTicketsAction, SetCategoriesAction, SetTicketsAction } from './actions';

export interface Ticket extends Identifiable {
    createdAt: string | Date
    lastModifiedDate: string | Date
    priority: string
    status: string
    title: string
    authorInfo: User
    categoryInfo: Category
    subcategoryInfo: Subcategory
    technicianInfo: User | null
    companyInfo: Identifiable
    description: string
    comments: Comment[]
}

export type TicketInfo = {
    title: string
    priority: string
    categoryInfo: Category
    subcategoryInfo: Subcategory
    description: string
    authorInfo: User
    createdAt: Date
    companyInfo: Identifiable
}

export interface Category extends Identifiable {
    name: string
    subcategories: Subcategory[]
    enabled: boolean
}

export interface Subcategory extends Identifiable {
    name: string
    enabled: boolean
}

export interface Specifiers {
    id: string
    creators: User[]
    categories: Category[]
    startedFrom: string | Date
    startedTo: string | Date
    modifiedFrom: string | Date
    modifiedTo: string | Date
    priorities: string[]
    status: string[]
    subcategories: Subcategory[]
    assignees: User[]
    title: string
    companies: Company[]
}

export type TicketPredicate = (ticket: Ticket) => boolean

export type DataState = {
    tickets: Ticket[]
    categories: Category[]
    filteredTickets: Ticket[]
}

export type Comment = {
    id: number
    authorInfo: User
    comment: string
}

export type DataActions = SetTicketsAction
                        | SetCategoriesAction
                        | FilterTicketsAction
