import { Role } from 'types/utils/helpers';
import { Identifiable } from '../common';
import { ChangeUserAction, ResetUserAction } from './actions';

export interface Company extends Identifiable {
    city: string
    country: string
    enabled: boolean
    localIdCode: string
    name: string
    postalCode: string
    premisesAddress: string
}

export interface User {
    id: string
    firstName: string
    lastName: string
    roles: Role[]
    companyInfo: Company
    email: string
    enabled: boolean
}

export type UserState = {
    id: string | null
    firstName: string | null
    lastName: string | null
    roles: Role[]
    companyInfo: Company | null
    email: string | null
    enabled: boolean
}

export type UserActions = ChangeUserAction
                        | ResetUserAction
