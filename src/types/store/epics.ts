import { Dependencies } from 'types/store/common';
import { Epic } from 'redux-observable';
import { StoreState } from 'store';
import { AcceptSolutionAction, AddCategoryAction, AddCompanyAction, AddNewTicketAction, AddSubcategoryAction, AddUserAction, ChangeAssigneeAction, CloseTicketAction, DeleteCategoryAction, EditCategoryAction, EditCompanyAction, EditSubcategoryAction, EditTicketAction, EditUserAction, FetchCategoriesAction, FetchTicketDetailsAction, FinishTicketAction, RefreshTicketsAction, RejectSolutionAction, SendMessageAction, StartProgressAction } from './actions';

export type SendMessageEpic = Epic<SendMessageAction, any, StoreState, Dependencies>
export type RefreshTicketsEpic = Epic<RefreshTicketsAction, any, StoreState, Dependencies>
export type ChangeAssigneeEpic = Epic<ChangeAssigneeAction, any, StoreState, Dependencies>
export type StartProgressEpic = Epic<StartProgressAction, any, StoreState, Dependencies>
export type FinishTicketEpic = Epic<FinishTicketAction, any, StoreState, Dependencies>
export type CloseTicketEpic = Epic<CloseTicketAction, any, StoreState, Dependencies>
export type AcceptSolutionEpic = Epic<AcceptSolutionAction, any, StoreState, Dependencies>
export type RejectSolutionEpic = Epic<RejectSolutionAction, any, StoreState, Dependencies>
export type FetchTicketDetailsEpic = Epic<FetchTicketDetailsAction, any, StoreState, Dependencies>
export type AddNewTicketEpic = Epic<AddNewTicketAction, any, StoreState, Dependencies>
export type EditTicketEpic = Epic<EditTicketAction, any, StoreState, Dependencies>
export type AddCompanyEpic = Epic<AddCompanyAction, any, StoreState, Dependencies>
export type AddUserEpic = Epic<AddUserAction, any, StoreState, Dependencies>
export type FetchCategoriesEpic = Epic<FetchCategoriesAction, any, StoreState, Dependencies>
export type AddSubcategoryEpic = Epic<AddSubcategoryAction, any, StoreState, Dependencies>
export type AddCategoryEpic = Epic<AddCategoryAction, any, StoreState, Dependencies>
export type DeleteCategoryEpic = Epic<DeleteCategoryAction, any, StoreState, Dependencies>
export type EditSubcategoryEpic = Epic<EditSubcategoryAction, any, StoreState, Dependencies>
export type EditCategoryEpic = Epic<EditCategoryAction, any, StoreState, Dependencies>
export type EditCompanyEpic = Epic<EditCompanyAction, any, StoreState, Dependencies>
export type EditUserEpic = Epic<EditUserAction, any, StoreState, Dependencies>
