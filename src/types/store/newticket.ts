import { HideNewTicketAction, SetEditedTicketAction, ShowNewTicketAction } from './actions';
import { Ticket } from './data';

export type NewTicketState = {
    newTicketVisible: boolean,
    editedTicket: Ticket | null
}

export type NewTicketActions = ShowNewTicketAction
                            | HideNewTicketAction
                            | SetEditedTicketAction
