import { Action } from 'redux';
import { Identifiable } from 'types/common';
import { CompanyInfo } from 'types/utils/company';
import { UserInfo } from 'types/utils/user';
import { Category, Specifiers, Subcategory, Ticket, TicketInfo } from './data';
import { ToastInfo } from './toasts';
import { Token } from './token';
import { Company, User } from './user';

export interface ChangeUserAction extends Action {
    type: 'CHANGE_USER'
    payload: User
}

export interface ResetUserAction extends Action {
    type: 'RESET_USER'
}

export interface RemoveToastAction extends Action {
    type: 'REMOVE_TOAST'
    payload: {
        id: string
    }
}

export interface ShowLoaderAction extends Action {
    type: 'SHOW_LOADER'
}

export interface HideLoaderAction extends Action {
    type: 'HIDE_LOADER'
}

export interface ShowUserDetailsAction extends Action {
    type: 'SHOW_USER_DETAILS'
}

export interface HideUserDetailsAction extends Action {
    type: 'HIDE_USER_DETAILS'
}

export interface HideTicketDetailsAction extends Action {
    type: 'HIDE_TICKET_DETAILS'
}

export interface ShowNewTicketAction extends Action {
    type: 'SHOW_NEW_TICKET'
}

export interface HideNewTicketAction extends Action {
    type: 'HIDE_NEW_TICKET'
}

export interface SetEditedTicketAction extends Action {
    type: 'SET_EDITED_TICKET'
    payload: Ticket | null
}

export interface SetTicketsAction extends Action {
    type: 'SET_TICKETS'
    payload: Ticket[]
}

export interface SetCategoriesAction extends Action {
    type: 'SET_CATEGORIES'
    payload: Category[]
}

export interface FilterTicketsAction extends Action {
    type: 'FILTER_TICKETS'
    payload: Specifiers
}

export interface UpdateTokenAction extends Action {
    type: 'UPDATE_TOKEN'
    payload: Token
}

export interface EditTicketAction extends Action {
    type: 'EDIT_TICKET'
    payload: Ticket
}

export interface AddNewTicketAction extends Action {
    type: 'ADD_NEW_TICKET'
    payload: TicketInfo
}

export interface ShowTicketDetailsAction extends Action {
    type: 'SHOW_TICKET_DETAILS'
    payload: {
        ticketId: number
    }
}

export interface RefreshTicketsAction extends Action {
    type: 'REFRESH_TICKETS'
}

export interface SendMessageAction extends Action {
    type: 'SEND_MESSAGE'
    payload: {
        message: string
        ticketId: number
    }
}

export interface ChangeAssigneeAction extends Action {
    type: 'CHANGE_ASSIGNEE'
    payload: {
        ticket: Ticket
        assignee: User
    }
}

export interface StartProgressAction extends Action {
    type: 'START_PROGRESS'
    payload: Ticket
}

export interface FinishTicketAction extends Action {
    type: 'FINISH_TICKET'
    payload: Ticket
}

export interface CloseTicketAction extends Action {
    type: 'CLOSE_TICKET'
    payload: Ticket
}

export interface AcceptSolutionAction extends Action {
    type: 'ACCEPT_SOLUTION'
    payload: Ticket
}

export interface RejectSolutionAction extends Action {
    type: 'REJECT_SOLUTION'
    payload: Ticket
}

export interface FetchTicketDetailsAction extends Action {
    type: 'FETCH_TICKET_DETAILS'
    payload: {
        ticketId: number
        withLoader?: boolean
    }
}

export interface SetTicketDetailsAction extends Action {
    type: 'SET_TICKET_DETAILS'
    payload: {
        ticket: Ticket | null,
        assignees: User[]
    }
}

export interface LogInAction extends Action {
    type: 'LOG_IN'
    payload: {
        email: string
        password: string
    }
}

export interface RegisterCompanyAction extends Action {
    type: 'REGISTER_COMPANY'
    payload: {
        companyName: string
        contactEmail: string
    }
}

export interface ResetPasswordAction extends Action {
    type: 'RESET_PASSWORD'
    payload: {
        email: string
    }
}

export interface AddToastAction extends Action {
    type: 'ADD_TOAST'
    payload: ToastInfo
}

export interface EditTicketData extends Action {
    type: 'EDIT_TICKET'
    payload: {
        title: string
        priority: string
        category: Category
        subcategory: Subcategory
        description: string
        creator: User
        startDate: Date,
        company: Identifiable
        user: User
    }
}

export interface AddCompanyAction extends Action {
    type: 'ADD_COMPANY'
    payload: CompanyInfo
}

export interface AddUserAction extends Action {
    type: 'ADD_USER'
    payload: UserInfo
}

export interface FetchCategoriesAction extends Action {
    type: 'FETCH_CATEGORIES'
    payload: {
        companyId: string
    }
}

export interface AddSubcategoryAction extends Action {
    type: 'ADD_SUBCATEGORY'
    payload: {
        name: string
        categoryId: string
    }
}

export interface AddCategoryAction extends Action {
    type: 'ADD_CATEGORY'
    payload: {
        name: string
        companyId: string
    }
}

export interface DeleteCategorySaga extends Action {
    type: 'DELETE_CATEGORY'
    payload: {
        categoryId: string
    }
}

export interface EditSubcategoryAction extends Action {
    type: 'EDIT_SUBCATEGORY'
    payload: {
        subcategory: Subcategory,
        categoryId: string | number
    }
}

export interface EditCategoryAction extends Action {
    type: 'EDIT_CATEGORY'
    payload: {
        id: string
        name: string
        enabled: boolean
    }
}

export interface EditCompanyAction extends Action {
    type: 'EDIT_COMPANY'
    payload: Company
}

export interface EditUserAction extends Action {
    type: 'EDIT_USER'
    payload: User
}

export interface DeleteCategoryAction extends Action {
    type: 'DELETE_CATEGORY'
    payload: {
        categoryId: string
    }
}
