export type CompanyInfo = {
    name: string
    country: string
    city: string
    postalCode: string
    premisesAddress: string
    localIdCode: string
    enabled: boolean
}
