export type UserInfo = {
    email: string
    firstName: string
    lastName: string
    company: string
    password: string
}
