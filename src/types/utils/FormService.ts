import { Subcategory, Ticket, TicketInfo } from 'types/store/data';
import { Company, User } from 'types/store/user';
import { CompanyInfo } from './company';
import { UserInfo } from './user';

export type SubmitLoginBody = {
    email: string
    password: string
}

export type SubmitRegisterBody = {
    companyName: string
    contactEmail: string
}

export type ResetPasswordBody = {
    email: string
}

export type AddUserBody = UserInfo

export type EditUserBody = User

export type AddCategoryBody = {
    name: string,
    enabled: boolean,
    company: {
        id: number | string
    }
}

export type EditCategoryBody = {
    id: string
    name: string
    enabled: boolean
}

export type AddSubcategoryBody = {
    name: string
    enabled: boolean
}

export type EditSubcategoryBody = Subcategory

export type AddCompanyBody = CompanyInfo

export type EditCompanyBody = Company

export type AddNewTicketBody = TicketInfo

export type EditTicketBody = Ticket
