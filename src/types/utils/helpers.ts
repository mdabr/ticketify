import { USER_TYPE } from 'utils/constants';

export type Role = {
    role: USER_TYPE
}
