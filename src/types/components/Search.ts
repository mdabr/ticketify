import { Action } from 'redux';
import { Category, Subcategory, Ticket } from 'types/store/data';
import { Company, User } from 'types/store/user';

export type SearchProps = {
    categories: Category[]
    tickets: Ticket[]
}

export type SearchState = {
    id: string,
    title: string,
    priorities: string[],
    status: string[],
    companies: Company[],
    categories: Category[],
    subcategories: Subcategory[],
    assignees: User[],
    creators: User[],
    startedFrom: Date,
    startedTo: Date,
    modifiedFrom: Date,
    modifiedTo: Date
}

export interface SearchAction extends Action {
    data: any
}
