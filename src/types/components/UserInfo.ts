import { User } from 'types/store/user';

export type CustomIconProps = {
    image: string
}

export type UserInfoProps = {
    user: User
    logOut: () => void
    showUserDetails: () => void
}
