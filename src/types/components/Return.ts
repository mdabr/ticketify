export type ReturnContainerProps = {
    image: string
    width: number
    height: number
}

export type ReturnProps = {
    image: string
    width?: number
    height?: number
    to: string
}
