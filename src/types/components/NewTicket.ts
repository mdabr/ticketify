import { Category, Subcategory, Ticket } from 'types/store/data';
import { Token } from 'types/store/token';
import { User } from 'types/store/user';

export type CloseButtonProps = {
    image: string
}

export type NewTicketMappedProps = {
    data: {
        categories: Category[]
    }
    user: User
    token: Token
    newticket: {
        editedTicket: Ticket
    }
}

export type NewTicketProps = {
    categories: Category[]
    user: User
    token: string
    ticket: Ticket | null
}

export type NewTicketDescriptionProps = {
    description: string
    setDescription: (value: string) => void
}

export type NewTicketMainProps = {
    stateHandlers: {
        title: string
        setTitle: (value: string) => void
        priority: string
        setPriority: (value: string) => void
        category: Category | null
        setCategory: (value: Category) => void
        subcategory: string
        setSubcategory: (value: Subcategory) => void
    },
    categories: Category[]
    onSubmit: () => void
    editing: boolean
}
