import { Category, Ticket } from 'types/store/data';

export type TicketListProps = {
    tickets: Ticket[]
    categories: Category[]
    token: string
}
