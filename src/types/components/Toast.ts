export type ToastProps = {
    id: string
    header: string
    message: string
    level: string
    time: number
}
