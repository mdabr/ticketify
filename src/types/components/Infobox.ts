import { Toast } from 'types/store/toasts';

export type InfoboxProps = {
    toasts: Toast[]
}
