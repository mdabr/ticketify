import { User } from 'types/store/user';

export type ViewHolderProps = {
    user: User
}
