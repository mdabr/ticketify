import { Action } from 'redux';
import { Category, Subcategory } from 'types/store/data';
import { Company, User } from 'types/store/user';

export type UserDetailsProps = {
    user: User
    token: string
}

export type PersonalProps = {
    user: User
}

export type CompanyInfoProps = {
    companyInfo: Company
}

export type UsersProps = {
    users: User[]
    onChange: () => void
}

export type AddUserProps = {
    onChange: () => void
    companies: Company[]
}

export type AddUserState = {
    email: string
    firstName: string
    lastName: string
    company: string
    role: string
    password: string
}

export interface AddUserAction extends Action {
    data?: string
}

export type CategoriesProps = {
    categories: Category[]
    token: string
    user: User
}

export type CompaniesProps = {
    companies: Company[]
    onChange: () => void
}

export type AddCompanyProps = {
    onChange: () => void
}

export type ChosenUserModalProps = {
    user?: User
    onClose: () => void
    onChange: () => void
}

export type ChosenCategoryModalProps = {
    open: boolean
    category: Category
    subcategory: Subcategory
    onClose: () => void
    onSave: () => void
    onCategoryEdit: (category: Category) => void
    onSubcategoryEdit: (subcategory: Subcategory) => void
}

export type ChosenCompanyModalProps = {
    company: Company
    onClose: () => void
    onChange: () => void
}
