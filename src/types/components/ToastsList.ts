import { Toast } from 'types/store/toasts';

export type ToastsListProps = {
    toasts: Toast[]
};
