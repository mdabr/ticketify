import { Action } from 'redux';
import { Category, Subcategory, Ticket } from 'types/store/data';
import { User } from 'types/store/user';

export type EditButtonProps = {
    image: string
}

export type TicketDetailsState = Ticket & {
    category: Category
    subcategory: Subcategory
}

export interface TicketDetailsAction extends Action {
    data?: string | Category | Subcategory | TicketDetailsState
}

export type TicketDetailsProps = {
    ticket: Ticket
    assignees: User[]
}

export type InfoProps = {
    ticket: Ticket
    assignees: User[]
}

export type MessagesProps = {
    ticket: Ticket
}

export type DescriptionProps = {
    description: string
}

export type AssignMeProps = {
    ticket: Ticket
}

export type StartProgressProps = {
    ticket: Ticket
}

export type AssignTechnicianProps = {
    ticket: Ticket
    assignees: User[]
}

export type ResolveTicketProps = {
    ticket: Ticket
}

export type VerifySolutionProps = {
    ticket: Ticket
}
