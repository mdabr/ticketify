export type MainPageProps = {
    userDetailsVisible: boolean
    ticketDetailsVisible: boolean
    newTicketVisible: boolean
}
