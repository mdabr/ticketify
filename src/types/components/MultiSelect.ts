import { InputProps } from '@material-ui/core';

export type MultiSelectProps = {
    onChange?: (elements: string[]) => void
    inputProps?: InputProps['inputProps']
    options: string[]
}

export type MultiSelectConfig = {
    name?: string
}
