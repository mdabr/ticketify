import { User } from 'types/store/user';

export type TopbarProps = {
    user: User
}
