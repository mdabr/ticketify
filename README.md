## Prerequisites

Install (node.js)[https://nodejs.org/en/] (tested for version 10.15.3)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Currently not supported.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>
