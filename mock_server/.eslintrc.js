module.exports = {
    'env': {
        'commonjs': true,
        'es6': true,
        'node': true
    },
    'rules': {
        'indent': [
            'error',
            4
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        'no-console': 'off',
        'valid-jsdoc': 'off'
    }
};
