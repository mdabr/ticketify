const LoginService = require('@/services/LoginService');

describe('LoginService', () => {
    test('generateAuthorizationToken', () => {
        it('generates authorization token', () => {
            const authInfo = {
                login: 'login',
                password: 'password'
            };
            const token = LoginService.generateToken(authInfo);

            expect(typeof token).toBe('string');
        });

        it('throws an error when credentials are incorrect', () => {
            const authInfo = null;

            expect(() => LoginService.generateToken(authInfo)).toThrow(LoginService.WRONG_CREDENTIALS);
        });
    });
});
