const UsersService = require('@/services/UsersService');

describe('UsersService', () => {
    beforeEach(() => {
        UsersService.removeAllListeners();
    });

    describe('getUsers', () => {
        it('return list of users based on filters', async () => {
            const filter = {
                some: 'filter'
            };
            const users = await UsersService.getUsers(filter);

            expect(Array.isArray(users)).toBe(true);
        });
    });

    describe('addUser', () => {
        it('registers a new user', async () => {
            const user = {
                firstName: 'John',
                lastName: 'Smith'
            };
            const companyId = 'abcd-efgh-ijkl-mnop';
            const userAddedListener = jest.fn();

            UsersService.on(UsersService.USER_ADDED, userAddedListener);
            await UsersService.addUser(user, companyId);

            expect(userAddedListener).toBeCalledTimes(1);
        });

        it('throws an error when company with provided ID was not found', async () => {
            const user = {
                firstName: 'John',
                lastName: 'Smith'
            };
            const companyId = null;
            const userAddedListener = jest.fn();

            UsersService.on(UsersService.USER_ADDED, userAddedListener);

            await expect(
                UsersService.addUser(user, companyId)
            ).rejects.toThrow(UsersService.COMPANY_NOT_FOUND);
            expect(userAddedListener).toBeCalledTimes(0);
        });

        it('throws an error when user information are invalid', async () => {
            const user = null;
            const companyId = 1;
            const userAddedListener = jest.fn();

            UsersService.on(UsersService.USER_ADDED, userAddedListener);

            await expect(
                UsersService.addUser(user, companyId)
            ).rejects.toThrow(UsersService.INVALID_USER);
            expect(userAddedListener).toBeCalledTimes(0);
        });
    });

    describe('updateUser', () => {
        it('updates user\'s information', async () => {
            const userId = 1;
            const newUser = {
                prop: 'toUpdate'
            };
            const userUpdatedListener = jest.fn();

            UsersService.on(UsersService.USER_UPDATED, userUpdatedListener);
            await UsersService.updateUser(userId, newUser);

            expect(userUpdatedListener).toBeCalledTimes(1);
        });

        it('throws an error when user with provided ID was not found', async () => {
            const userId = null;
            const newUser = {
                prop: 'toUpdate'
            };
            const userUpdatedListener = jest.fn();

            UsersService.on(UsersService.USER_UPDATED, userUpdatedListener);

            await expect(
                UsersService.updateUser(userId, newUser)
            ).rejects.toThrow(UsersService.USER_NOT_FOUND);
            expect(userUpdatedListener).toBeCalledTimes(0);
        });

        it('throws an error when user information are invalid', async () => {
            const userId = 1;
            const newUser = null;
            const userUpdatedListener = jest.fn();

            UsersService.on(UsersService.USER_UPDATED, userUpdatedListener);

            await expect(
                UsersService.updateUser(userId, newUser)
            ).rejects.toThrow(UsersService.INVALID_USER);
            expect(userUpdatedListener).toBeCalledTimes(0);
        });
    });

    describe('getUser', () => {
        it('returns user with specified ID', async () => {
            const userId = 1;
            const user = await UsersService.getUser(userId);

            expect(user).toBe(expect.anything());
        });

        it('returns null when user was not found', async () => {
            const userId = null;
            const user = await UsersService.getUser(userId);

            expect(user).toBeNull();
        });
    });
});
