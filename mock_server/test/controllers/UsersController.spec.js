const supertest = require('supertest');

const app = require('@/app');

const request = supertest(app);

describe('UsersController', () => {
    const authorization = ['Authorization', 'abc123'];

    describe('sendUsers', () => {
        it('sends list of users', async () => {
            const response = await request
                .get('/users')
                .set(...authorization);

            expect(response.status).toBe(200);
            expect(Array.isArray(response.body)).toBe(true);
        });

        it('returns an error for unauthorized user', async () => {
            const response = await request
                .get('/users');

            expect(response.status).toBe(401);
        });
    });

    describe('addUser', () => {
        it('accepts request for adding user', async () => {
            const body = {
                user: {
                    firstName: 'John',
                    lastName: 'Smith'
                },
                companyId: 1
            };
            const response = await request
                .post('/users')
                .set(...authorization)
                .send(body);

            expect(response.status).toBe(200);
        });

        it('returns an error for unauthorized user', async () => {
            const body = {
                user: {
                    firstName: 'John',
                    lastName: 'Smith'
                },
                companyId: 1
            };
            const response = await request
                .post('/users')
                .send(body);

            expect(response.status).toBe(401);
        });

        it('returns an error for missing user data', async () => {
            const body = {
                user: null,
                companyId: 1
            };
            const response = await request
                .post('/users')
                .set(...authorization)
                .send(body);

            expect(response.status).toBe(422);
        });

        it('returns an error for missing companyId', async () => {
            const body = {
                user: {
                    firstName: 'John',
                    lastName: 'Smith'
                },
                companyId: null
            };
            const response = await request
                .post('/users')
                .set(...authorization)
                .send(body);

            expect(response.status).toBe(422);
        });
    });

    describe('updateUser', () => {
        it('updates user information', async () => {
            const id = 1;
            const newUser = {
                lastName: 'Locke'
            };
            const response = await request
                .put(`/users/${id}`)
                .set(...authorization)
                .send(newUser);

            expect(response.status).toBe(200);
        });

        it('returns an error for unauthorized user', async () => {
            const id = 1;
            const newUser = {
                lastName: 'Locke'
            };
            const response = await request
                .put(`/users/${id}`)
                .send(newUser);

            expect(response.status).toBe(401);
        });

        it('returns an error for wrong id parameter', async () => {
            const id = null;
            const newUser = {
                lastName: 'Locke'
            };
            const response = await request
                .put(`/users/${id}`)
                .set(...authorization)
                .send(newUser);

            expect(response.status).toBe(422);
        });

        it('returns an error for missing user data', async () => {
            const id = 1;
            const newUser = null;
            const response = await request
                .put(`/users/${id}`)
                .set(...authorization)
                .send(newUser);

            expect(response.status).toBe(422);
        });
    });

    describe('sendUser', () => {
        it('sends requested user data', async () => {
            const id = 1;
            const response = await request
                .get(`/users/${id}`)
                .set(...authorization);

            expect(response.status).toBe(200);
            expect(typeof response.body).toBe('object');
        });

        it('returns an error for unauthorized user', async () => {
            const id = 1;
            const response = await request
                .get(`/users/${id}`);

            expect(response.status).toBe(401);
        });

        it('returns an error for wrong id parameter', async () => {
            const id = null;
            const response = await request
                .get(`/users/${id}`)
                .set(...authorization);

            expect(response.status).toBe(422);
        });
    });

    describe('sendUsersWithRole', () => {
        it('sends users with specified role', async () => {
            const role = 'SOME_ROLE';
            const response = await request
                .get(`/role/${role}`)
                .set(...authorization);

            expect(response.status).toBe(200);
            expect(Array.isArray(response.body)).toBe(true);
        });

        it('returns an error for unauthorized user', async () => {
            const role = 'SOME_ROLE';
            const response = await request
                .get(`/role/${role}`);

            expect(response.status).toBe(401);
        });

        it('returns an error for missing role parameter', async () => {
            const role = null;
            const response = await request
                .get(`/role/${role}`)
                .set(...authorization);

            expect(response.status).toBe(422);
        });
    });
});
