const supertest = require('supertest');

const app = require('@/app');

const request = supertest(app);

describe('LoginController', () => {
    describe('sendAuthorizationToken', () => {
        it('returns authorization token for valid login and password', async () => {
            const body = {
                login: 'login',
                password: 'password'
            };
            const response = await request.post('/login').send(body);

            expect(response.status).toEqual(200);
            expect(response.body).toHaveProperty('token');
            expect(response.body).toHaveProperty('expires');
        });

        it('sends error for invalid login and password', async () => {
            const body = null;
            const response = await request.post('/login').send(body);

            expect(response.status).toEqual(401);
        });
    });
});
