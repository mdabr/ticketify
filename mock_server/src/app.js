const express = require('express');
const app = express();
const cors = require('cors');
const os = require('os');

const port = 8080;

app.use(cors());
app.use(express.json());
app.use(require('./utils/throttle')(500));
app.use(require('./routes'));

app.listen(port, () => {
    console.log('HTTP service opened on following addresses: ');

    const interfaces = os.networkInterfaces();

    Object.keys(interfaces).forEach(interfaceName => {
        const interfaceAdresses = interfaces[interfaceName];

        interfaceAdresses.forEach(interfaceAdress => {
            if (interfaceAdress.family === 'IPv4' && !interfaceAdress.internal) {
                console.log(`${interfaceAdress.address}:${port}`);
            }
        });
    });
});

module.exports = app;
