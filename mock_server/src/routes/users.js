const router = require('express').Router();

const UsersController = require('../controllers/UsersController');

router.get('/', UsersController.sendUsers);
router.post('/', UsersController.addUser);

router.put('/:id', UsersController.updateUser);
router.get('/:id', UsersController.sendUser);

router.get('/role/:role', UsersController.sendUsersWithRole);

module.exports = router;
