const router = require('express').Router();

router.use('/login', require('./login'));
router.use('/users', require('./users'));
router.use('/tickets', require('./tickets'));
router.use('/categories', require('./categories'));
router.use('/companies', require('./companies'));

module.exports = router;
