const router = require('express').Router();

const TicketsController = require('../controllers/TicketsController');

router.get('/', TicketsController.sendTickets);
router.post('/', TicketsController.createTicket);
router.put('/', TicketsController.updateTicket);

router.get('/:id', TicketsController.sendTicket);

router.post('/:id/comments', TicketsController.addComment);

router.post('/newCompany', TicketsController.addCompanyRegistrationTicket);

router.post('/resetPassword', TicketsController.addPasswordResetTicket);

module.exports = router;
