const router = require('express').Router();

const CompaniesController = require('../controllers/CompaniesController');

router.get('/', CompaniesController.sendCompanies);
router.post('/', CompaniesController.addCompany);
router.put('/', CompaniesController.updateCompany);

module.exports = router;
