const router = require('express').Router();

const CategoriesController = require('../controllers/CategoriesController');

router.get('/', CategoriesController.sendCategories);
router.post('/', CategoriesController.addCategory);
router.put('/', CategoriesController.updateCategory);

router.delete('/:id', CategoriesController.removeCategory);

router.post('/:id/subcategories', CategoriesController.addSubcategory);
router.put('/:id/subcategories', CategoriesController.updateSubcategory);

module.exports = router;
