module.exports = {
    *getIDGenerator(start) {
        let nextId = start === undefined ? 1 : start;

        while (true) {
            yield nextId;

            nextId += 1;
        }
    }
};
