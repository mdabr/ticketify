module.exports = timeout => (request, response, next) => setTimeout(() => next(), timeout);
