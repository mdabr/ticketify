/**
 * @typedef {Object} Authorization
 * @property {String} token
 * @property {Date} expires
 */

/**
 * @typedef {Object} UserFilter
 * @property {Role} [role]
 * @property {String} [company]
 */
