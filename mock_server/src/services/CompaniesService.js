const EventEmitter = require('events');
const { getIDGenerator } = require('../utils/id');

const companiesIDGenerator = getIDGenerator(2);

class CompaniesService extends EventEmitter {
    constructor() {
        super();

        this.companies = [{
            id: 1,
            city: 'Warsaw',
            country: 'Poland',
            enabled: true,
            localIdCode: '42069',
            name: 'RicCorp',
            postalCode: '01-234',
            premisesAddress: 'Some Polish Street'
        }];
    }

    getCompanies() {
        return this.companies;
    }

    addCompany(company) {
        const companyToAdd = {
            id: companiesIDGenerator.next().value,
            city: company.city,
            country: company.country,
            enabled: Boolean(company.enabled),
            localIdCode: company.localIdCode,
            name: company.name,
            postalCode: company.postalCode,
            premisesAddress: company.premisesAddress
        };

        this.companies.push(companyToAdd);

        return companyToAdd;
    }

    updateCompany(company) {
        const companyIndex = this.companies.findIndex(com => com.id === company.id);
        const companyUpdater = {
            city: company.city,
            country: company.country,
            enabled: Boolean(company.enabled),
            localIdCode: company.localIdCode,
            name: company.name,
            postalCode: company.postalCode,
            premisesAddress: company.premisesAddress,
            ...this.companies[companyIndex]
        };

        this.companies.splice(companyIndex, 1, companyUpdater);

        return companyUpdater;
    }

    getCompany(companyId) {
        return this.companies.find(company => company.id === companyId) || null;
    }
}

module.exports = new CompaniesService();
