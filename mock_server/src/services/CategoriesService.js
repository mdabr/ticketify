const EventEmitter = require('events');

class CategoriesService extends EventEmitter {
    constructor() {
        super();

        this.categories = [{
            id: 1,
            name: 'Mobile device',
            enabled: true,
            subcategories: [{
                id: 1,
                name: 'Phone',
                enabled: true
            }]
        }];
    }

    getCategories() {
        return this.categories;
    }

    createCategory(categoryInformation) {
        this.categories.push({
            subcategories: [],
            ...categoryInformation
        });
    }

    updateCategory(categoryId, categoryInformation) {
        const categoryIndex = this.categories.findIndex(category => category.id === categoryId);

        this.categories.splice(categoryIndex, 1, { ...this.categories[categoryIndex], ...categoryInformation });
    }

    removeCategory(categoryId) {
        const categoryIndex = this.categories.findIndex(category => category.id === categoryId);

        this.categories.splice(categoryIndex, 1);
    }

    addSubcategory(categoryId, subcategoryInfo) {
        const categoryIndex = this.categories.findIndex(category => category.id === categoryId);

        this.tickets.splice(categoryIndex, 1, { ...this.categories[categoryIndex], subcategories: this.categories[categoryIndex].subcategories.concat({ ...subcategoryInfo }) });
    }

    updateSubcategory(categoryId, subcategoryInfo) {
        const categoryIndex = this.categories.findIndex(category => category.id === categoryId);
        const subcategoryIndex = this.categories[categoryIndex].subcategories.findIndex(subcategory => subcategory.id === subcategoryInfo.id);

        this.categories[categoryIndex].subcategories.splice(subcategoryIndex, 1, { ...this.categories[categoryIndex].subcategories[subcategoryIndex], ...subcategoryInfo });
    }

    getCategory(categoryId) {
        return this.categories.find(category => category.id === categoryId) || null;
    }

    getSubcategory(subcategoryId) {
        return this.categories.flatMap(category => category.subcategories).find(subcategory => subcategory.id === subcategoryId);
    }
}

module.exports = new CategoriesService();
