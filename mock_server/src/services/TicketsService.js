const EventEmitter = require('events');
const { getIDGenerator } = require('../utils/id');
const CategoriesService = require('./CategoriesService');
const CompaniesService = require('./CompaniesService');
const LoginService = require('./LoginService');
const UsersService = require('./UsersService');

const ticketIDGenerator = getIDGenerator(2);

class TicketsService extends EventEmitter {
    constructor() {
        super();

        this.tickets = [{
            id: 1,
            createdAt: new Date(),
            lastModifiedDate: new Date(),
            priority: 'LOW',
            status: 'UNASSIGNED',
            title: 'Fix mah phone',
            authorInfo: 'myself',
            categoryInfo: 1,
            subcategoryInfo: 1,
            technicianInfo: null,
            companyInfo: 1,
            description: 'Phone is ded',
            comments: []
        }];
    }

    getTickets() {
        return this.tickets.map(ticket => ({
            ...ticket,
            authorInfo: UsersService.getUser(ticket.authorInfo),
            categoryInfo: CategoriesService.getCategory(ticket.categoryInfo),
            subcategoryInfo: CategoriesService.getSubcategory(ticket.subcategoryInfo),
            technicianInfo: UsersService.getUser(ticket.technicianInfo),
            companyInfo: CompaniesService.getCompany(ticket.companyInfo),
            comments: ticket.comments.map(comment => ({
                authorInfo: UsersService.getUser(comment.authorInfo),
                comment: comment.comment
            }))
        }));
    }

    createTicket(ticketInformation) {
        this.tickets.push({
            ...ticketInformation,
            id: ticketIDGenerator.next().value
        });
    }

    updateTicket(ticketInformation) {
        const ticketIndex = this.tickets.findIndex(ticket => ticket.id === ticketInformation.id);
        const updatedTicket = {
            comments: [],
            ...this.tickets[ticketIndex],
            status: ticketInformation.status,
            authorInfo: ticketInformation.authorInfo.id,
            categoryInfo: ticketInformation.categoryInfo.id,
            subcategoryInfo: ticketInformation.subcategoryInfo.id,
            companyInfo: ticketInformation.companyInfo.id,
            technicianInfo: ticketInformation.technicianInfo.id
        };
        console.log(1, ticketInformation);
        console.log(2, updatedTicket);

        this.tickets.splice(ticketIndex, 1, updatedTicket);
    }

    getTicket(ticketId) {
        const ticket = this.tickets.find(ticket => ticket.id === ticketId);

        if (ticket) {
            return {
                ...ticket,
                authorInfo: UsersService.getUser(ticket.authorInfo),
                categoryInfo: CategoriesService.getCategory(ticket.categoryInfo),
                subcategoryInfo: CategoriesService.getSubcategory(ticket.subcategoryInfo),
                technicianInfo: UsersService.getUser(ticket.technicianInfo),
                companyInfo: CompaniesService.getCompany(ticket.companyInfo),
                comments: ticket.comments.map(comment => ({
                    authorInfo: UsersService.getUser(comment.authorInfo),
                    comment: comment.comment
                }))
            };
        }

        throw new Error('Ticket with provided ID was not found');
    }

    addComment(ticketId, comment, token) {
        const ticketIndex = this.tickets.findIndex(ticket => ticket.id === ticketId);
        const newComment = {
            authorInfo: LoginService.getUserByToken(token),
            comment: comment.comment
        };

        this.tickets.splice(ticketIndex, 1, { ...this.tickets[ticketIndex], comments: this.tickets[ticketIndex].comments.concat(newComment) });
    }
}

module.exports = new TicketsService();
