const EventEmitter = require('events');
const UsersService = require('./UsersService');

class LoginService extends EventEmitter {
    constructor() {
        super();

        // Events
        this.USER_VERIFIED = 'USER_VERIFIED';

        // Errors
        this.WRONG_CREDENTIALS = 'Invalid username or password';

        this.usersTokens = {};
    }

    /**
     * Generates authorization token
     * @param {Object} authInfo authorization info
     * @param {String} authInfo.email
     * @param {String} authInfo.password
     * @emits USER_VERIFIED when token was generated
     * @throws {AuthorizationError} when credentials are incorrect
     * @returns {String} authorization token
     */
    generateToken(authInfo) {
        const user = UsersService.getUserByLogin(authInfo.email, authInfo.password);

        if (!user) {
            throw new Error(this.WRONG_CREDENTIALS);
        }

        const token = `${new Date().getTime()}`;

        this.usersTokens[token] = user.id;

        return token;
    }

    getUserByToken(token) {
        console.log(this.usersTokens, token);
        return this.usersTokens[token.split(' ')[1]];
    }
}

module.exports = new LoginService();
