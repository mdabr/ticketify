/* eslint-disable */ // TODO: remove after implementation
const EventEmitter = require('events');
const { getIDGenerator } = require('../utils/id');
const CompaniesService = require('./CompaniesService');

require('../utils/typedefs');

const usersIDGenerator = getIDGenerator();

class UsersService extends EventEmitter {
    constructor() {
        super();

        // Events
        this.USER_ADDED = 'USER_ADDED';
        this.USER_UPDATED = 'USER_UPDATED';

        // Errors
        this.COMPANY_NOT_FOUND = 'Company with provided ID was not found';
        this.INVALID_USER = 'Provided user information are invalid';
        this.USER_NOT_FOUND = 'User with provided ID was not found';

        this.users = [{
            id: 'myself',
            firstName: 'Mateusz',
            lastName: 'Dabrowski',
            companyInfo: 1,
            roles: [{
                role: 'ADMINISTRATOR'
            }],
            email: 'm.dabrowski@riccorp.com',
            password: 'test123'
        }, {
            id: '1',
            firstName: 'Mateusz',
            lastName: 'Dabrowski',
            companyInfo: 1,
            roles: [{
                role: 'TECHNICIAN'
            }],
            email: 'm.dabrowski@riccorp.com',
            password: 'test123'
        }];
    }

    /**
     * Returns list of users based on filters
     * @param {UserFilter} filter rules for filtering users list
     * @returns {User[]} filtered list of users
     */
    getUsers(filter = {}) {
        return this.users.filter(user =>
            (!filter.company || filter.company === user.companyInfo.id)
            && (!filter.role || user.roles.find(({ role }) => role === filter.role))
        ).map(user => ({
            ...user,
            companyInfo: CompaniesService.getCompany(user.companyInfo)
        }));
    }

    /**
     * Registers a new user
     * @param {User} user information for registering user
     * @param {String} companyId ID of company for which user will be registered
     * @emits USER_ADDED when user was added
     * @throws {Error} when user information are invalid
     * @throws {Error} when company with provided ID was not found
     */
    addUser(user, companyId) {
        this.users.push({
            ...user,
            companyInfo: companyId,
            id: usersIDGenerator.next().value
        });
    }

    /**
     * Updates user's information
     * @param {String} userId ID of user to be updated
     * @param {User} newUser information to update user
     * @emits USER_UPDATED when user was updated
     * @throws {Error} when user with provided ID was not found
     * @throws {Error} when user information are invalid
     */
    updateUser(userId, newUser) {
        const userIndex = this.users.findIndex(user => user.id === userId);

        if (userIndex < 0) {
            throw new Error(this.USER_NOT_FOUND);
        }

        this.users.splice(userIndex, 1, { ...this.users[userIndex], ...newUser });
    }

    /**
     * Returns user with specified ID
     * @param {String} userId ID of user to be returned
     * @returns {User|null} user with specified ID
     */
    getUser(userId) {
        const user = this.users.find(u => u.id === userId);

        if (user) {
            return {
                ...user,
                companyInfo: CompaniesService.getCompany(user.companyInfo)
            };
        }

        return null;
    }

    getUserByLogin(login, password) {
        return this.users.find(user => user.email === login && user.password === password) || null;
    }
}

module.exports = new UsersService();
