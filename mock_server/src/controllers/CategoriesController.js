const CategoriesService = require('../services/CategoriesService');

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function sendCategories(request, response) {
    try {
        const categories = CategoriesService.getCategories();

        response.status(200).send(categories);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function addCategory(request, response) {
    try {
        const categoryInfo = request.body;
        const category = CategoriesService.createCategory(categoryInfo);

        response.status(200).send(category);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function updateCategory(request, response) {
    try {
        const { id } = request.params;
        const categoryInfo = request.body;
        const updatedCategory = CategoriesService.updateCategory(id, categoryInfo);

        response.status(200).send(updatedCategory);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function removeCategory(request, response) {
    try {
        const { id } = request.params;
        const removedCategory = CategoriesService.removeCategory(id);

        response.status(200).send(removedCategory);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function addSubcategory(request, response) {
    try {
        const { id } = request.params;
        const subcategoryInfo = request.body;
        const addedSubcategory = CategoriesService.addSubcategory(id, subcategoryInfo);

        response.status(200).send(addedSubcategory);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function updateSubcategory(request, response) {
    try {
        const { id } = request.params;
        const subcategoryInfo = request.body;
        const updatedSubcategory = CategoriesService.updateSubcategory(id, subcategoryInfo);

        response.status(200).send(updatedSubcategory);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

module.exports = {
    sendCategories,
    addCategory,
    updateCategory,
    removeCategory,
    addSubcategory,
    updateSubcategory
};
