const CompaniesService = require('../services/CompaniesService');

/**
 * Sends companies
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function sendCompanies(request, response) {
    try {
        const companies = CompaniesService.getCompanies();

        response.status(200).send(companies);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 * Sends companies
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function addCompany(request, response) {
    try {
        const companyToAdd = request.body;

        const addedCompany = CompaniesService.addCompany(companyToAdd);

        response.status(200).send(addedCompany);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 * Sends companies
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function updateCompany(request, response) {
    try {
        const companyUpdateInfo = request.body;

        const updatedCompany = CompaniesService.updateCompany(companyUpdateInfo);

        response.status(200).send(updatedCompany);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

module.exports = {
    sendCompanies,
    addCompany,
    updateCompany
};
