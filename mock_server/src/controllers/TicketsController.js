const TicketsService = require('../services/TicketsService');

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function sendTickets(request, response) {
    try {
        const tickets = TicketsService.getTickets();

        response.status(200).send(tickets);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function createTicket(request, response) {
    try {
        const ticketInformation = request.body;

        const createdTicket = TicketsService.createTicket(ticketInformation);

        response.status(200).send(createdTicket);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function updateTicket(request, response) {
    try {
        const ticketInformation = request.body;

        const updatedTicket = TicketsService.updateTicket(ticketInformation);

        response.status(200).send(updatedTicket);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function sendTicket(request, response) {
    try {
        const { id } = request.params;

        const ticket = TicketsService.getTicket(parseInt(id, 10));

        response.status(200).send(ticket);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function addComment(request, response) {
    try {
        const { id } = request.params;
        const comment = request.body;
        const token = request.headers.authorization;

        TicketsService.addComment(parseInt(id, 10), comment, token);

        response.status(200).end();
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function addCompanyRegistrationTicket(request, response) {
    try {
        const ticketInformation = request.body;

        const addedTicket = TicketsService.createTicket(ticketInformation);

        response.status(200).send(addedTicket);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 *
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function addPasswordResetTicket(request, response) {
    try {
        const ticketInformation = request.body;

        const addedTicket = TicketsService.createTicket(ticketInformation);

        response.status(200).send(addedTicket);
    } catch (error) {
        console.error(error);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

module.exports = {
    sendTickets,
    createTicket,
    updateTicket,
    sendTicket,
    addComment,
    addCompanyRegistrationTicket,
    addPasswordResetTicket
};
