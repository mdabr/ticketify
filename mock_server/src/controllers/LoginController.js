const LoginService = require('../services/LoginService');

/**
 * Sends authorization token for valid login and password
 * @param {import('express').Request} request
 * @param {import('express').Response} response
 */
function sendAuthorizationToken(request, response) {
    try {
        const token = LoginService.generateToken(request.body);
        const expires = new Date();
        expires.setHours(expires.getHours() + 10);

        response.status(200).send({ expires, token });
    } catch (error) {
        console.error(error);

        const errorHandler = {
            [LoginService.WRONG_CREDENTIALS]: () => response.status(401).end(),
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

module.exports = {
    sendAuthorizationToken
};
