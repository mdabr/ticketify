/* eslint-disable */ // TODO: remove after implementation

const UsersService = require('../services/UsersService');

/**
 * Sends users list based on user authorization
 * @param {import('express').Request} request 
 * @param {import('express').Response} response 
 */
function sendUsers(request, response) {
    try {
        const users = UsersService.getUsers();

        response.status(200).send(users);
    } catch (error) {
        console.error(error.message);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 * Adds a new user
 * @param {import('express').Request} request 
 * @param {import('express').Response} response 
 */
function addUser(request, response) {
    try {
        const userInfo = request.body;

        UsersService.getUser(userInfo);

        response.status(200).end();
    } catch (error) {
        console.error(error.message);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 * Updates user information
 * @param {import('express').Request} request 
 * @param {import('express').Response} response 
 */
function updateUser(request, response) {
    try {
        const { id } = request.params;
        const user = UsersService.getUser(id);

        response.status(200).send(user);
    } catch (error) {
        console.error(error.message);

        const errorHandler = {
            [UsersService.USER_NOT_FOUND]: () => response.status(404).end(),
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 * Sends user information
 * @param {import('express').Request} request 
 * @param {import('express').Response} response 
 */
function sendUser(request, response) {
    try {
        const { id } = request.params;
        const user = UsersService.getUser(id);

        response.status(200).send(user);
    } catch (error) {
        console.error(error.message);

        const errorHandler = {
            [UsersService.USER_NOT_FOUND]: () => response.status(404).end(),
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

/**
 * Sends users list with specific role
 * @param {import('express').Request} request 
 * @param {import('express').Response} response 
 */
function sendUsersWithRole(request, response) {
    try {
        const { role } = request.params;
        const users = UsersService.getUsers({ role });

        response.status(200).send(users);
    } catch (error) {
        console.error(error.message);

        const errorHandler = {
            default: () => response.status(500).end()
        };

        (errorHandler[error.message] || errorHandler.default)();
    }
}

module.exports = {
    sendUsers,
    addUser,
    updateUser,
    sendUser,
    sendUsersWithRole
};
